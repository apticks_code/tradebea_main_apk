package com.tradebeauser.interfaces;

public interface CloseCallBack {
    void close();
}
