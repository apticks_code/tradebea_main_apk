package com.tradebeauser.interfaces;

public interface TransactionDetailsFilterCallBack {
    void transactionDetailsFilter(String startDate, String endDate, String lastDays);
}
