package com.tradebeauser.interfaces;

public interface LocationUpdateCallBack {
    void OnLatLongReceived(double latitude, double longitude);
}
