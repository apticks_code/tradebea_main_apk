package com.tradebeauser.interfaces;

public interface SelectedQuantityCallBack {
    void OnQuantitySelected(int quantity);
}
