package com.tradebeauser.interfaces;

import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;

public interface SelectedAddressCallBack {
    void selectedAddress(AddressDetails addressDetails);
}
