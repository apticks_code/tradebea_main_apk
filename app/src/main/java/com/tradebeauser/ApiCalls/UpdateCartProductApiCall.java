package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.JsonObject;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.jsonBuilderParser.JsonBuilderParser;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;

import org.json.JSONObject;

public class UpdateCartProductApiCall {
    private static HttpReqResCallBack callBack;

    public static void serviceCallToUpdateCartProduct(Context context, Fragment fragment, RecyclerView.Adapter adapter, String ID, String status, String quantity, String token) {
        String url = Constants.UPDATE_CART_PRODUCT_STATUS_URL;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",ID);
        jsonObject.addProperty("status",status);
        jsonObject.addProperty("qty",quantity);
        JSONObject postObject = JsonBuilderParser.jsonBuilder(jsonObject);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS);
                        }
                    }
                });
    }
}
