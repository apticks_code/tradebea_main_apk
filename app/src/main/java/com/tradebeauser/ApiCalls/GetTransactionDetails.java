package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.utils.Constants;

import java.util.HashMap;
import java.util.Map;

public class GetTransactionDetails {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetTransactionDetails(Context context, Fragment fragment, final RecyclerView.Adapter adapter, String token, String startDate, String endDate, String lastDays, String status) {
        String url = Constants.BASE_URL + "payment/api/payment/wallet_payments?";
        if (!lastDays.isEmpty()) {
            url = url + "" + "last_days" + "=" + lastDays;
        }
        if (!startDate.isEmpty()) {
            url = url + "" + "&start_date" + "=" + startDate;
        }
        if (!endDate.isEmpty()) {
            url = url + "" + "&end_date" + "=" + endDate;
        }
        if (!status.isEmpty()) {
            url = url + "" + "&status" + "=" + status;
        }

        StringRequest postRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(response, 200, Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(response, 200, Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(response, 200, Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("X_AUTH_TOKEN", token);
                headers.put("APP_id", Constants.APP_ID);
                return headers;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        requestQueue.add(postRequest);
    }
}
