package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.JsonObject;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.jsonBuilderParser.JsonBuilderParser;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;

import org.json.JSONObject;

public class OrderHistoryApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForOrderHistory(Context context, final Fragment fragment, final RecyclerView.Adapter adapter, String token, String startDate, String endDate, String lastDays, String lastYear) {

        String url = Constants.ORDER_HISTORY_URL;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("start_date", startDate);
        jsonObject.addProperty("end_date", endDate);
        jsonObject.addProperty("last_days", lastDays);
        jsonObject.addProperty("last_years", lastYear);
        jsonObject.addProperty("status", "");
        JSONObject postObject = JsonBuilderParser.jsonBuilder(jsonObject);
        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject)
                .setPriority(Priority.HIGH)
                .addHeaders("X_AUTH_TOKEN", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ORDER_HISTORY);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ORDER_HISTORY);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ORDER_HISTORY);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ORDER_HISTORY);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ORDER_HISTORY);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ORDER_HISTORY);
                        }
                    }
                });
    }
}
