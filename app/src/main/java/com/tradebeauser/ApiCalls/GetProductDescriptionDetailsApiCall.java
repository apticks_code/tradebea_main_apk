package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;

import org.json.JSONObject;

public class GetProductDescriptionDetailsApiCall {
    private static HttpReqResCallBack callBack;

    public static void serviceCallToProductDescription(Context context, Fragment fragment, RecyclerView.Adapter adapter, String productID, String latitude, String longitude) {
        String url = Constants.PRODUCTS_LIST_URL;

        AndroidNetworking.get(url)
                .addQueryParameter("product_id",productID)// posting json
                .addQueryParameter("latitude",latitude)
                .addQueryParameter("longitude",longitude)
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS);
                        }
                    }
                });
    }
}
