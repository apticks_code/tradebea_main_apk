package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.jsonBuilderParser.JsonBuilderParser;
import com.tradebeauser.models.requestModels.countDetailsRequest.CountDetailsRequest;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;

import org.json.JSONObject;

public class CountDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForCountDetails(final Context context, final Fragment fragment, final RecyclerView.Adapter adapter, String token, int activeLocationID, String latitude, String longitude) {
        String url = Constants.COUNT_URL;

        CountDetailsRequest countDetailsRequest = new CountDetailsRequest();
        if (activeLocationID != -1) {
            countDetailsRequest.shippingAddressId = activeLocationID;
        } else if (!latitude.isEmpty()) {
            countDetailsRequest.currentLocationLatitude = Double.parseDouble(latitude);
            countDetailsRequest.currentLocationLongitude = Double.parseDouble(longitude);
        }
        JSONObject postObject = JsonBuilderParser.jsonBuilder(countDetailsRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("X_AUTH_TOKEN", token)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_COUNT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_COUNT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_COUNT_DETAILS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_COUNT_DETAILS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_COUNT_DETAILS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_COUNT_DETAILS);
                        }
                    }
                });
    }
}
