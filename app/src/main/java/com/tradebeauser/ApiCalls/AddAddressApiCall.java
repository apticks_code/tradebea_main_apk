package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.jsonBuilderParser.JsonBuilderParser;
import com.tradebeauser.models.requestModels.AddressDetailsRequest.AddressDetailsRequest;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;

import org.json.JSONObject;

public class AddAddressApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToAddNewAddresses(final Context context, final Fragment fragment, final RecyclerView.Adapter adapter, String token, String geoAddress, String completeAddress, String landmark, String pinCode, String tag, String latitude, String longitude) {
        String url = Constants.ADD_ADDRESSES_LIST_URL;
        AddressDetailsRequest addressDetailsRequest = new AddressDetailsRequest();
        addressDetailsRequest.tag = tag;
        addressDetailsRequest.geoLocation = geoAddress;
        addressDetailsRequest.address = completeAddress;
        addressDetailsRequest.pincode = pinCode;
        addressDetailsRequest.landmark = landmark;
        addressDetailsRequest.latitude = latitude;
        addressDetailsRequest.longitude = longitude;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(addressDetailsRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("X_AUTH_TOKEN", token)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ADD_ADDRESS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ADD_ADDRESS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_ADD_ADDRESS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ADD_ADDRESS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ADD_ADDRESS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_ADD_ADDRESS);
                        }
                    }
                });
    }
}
