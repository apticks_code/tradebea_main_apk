package com.tradebeauser.ApiCalls;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeauser.R;
import com.tradebeauser.activities.EditProfileDetailsActivity;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.jsonBuilderParser.JsonBuilderParser;
import com.tradebeauser.models.requestModels.updateProfileDetailsRequest.UpdateProfileDetailsRequest;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;

import org.json.JSONObject;

public class UpdateProfileDetails {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForUpdateProfileDetails(Context context, String firstName, String lastName, String emailId, String mobile, String selectedGender, String profileImageBase64, String token) {
        String url = Constants.UPDATE_PROFILE_DETAILS_URL;
        UpdateProfileDetailsRequest updateProfileDetailsRequest = new UpdateProfileDetailsRequest();
        updateProfileDetailsRequest.firstName = firstName;
        updateProfileDetailsRequest.lastName = lastName;
        updateProfileDetailsRequest.gender = selectedGender;
        updateProfileDetailsRequest.email = emailId;
        updateProfileDetailsRequest.mobile = mobile;
        updateProfileDetailsRequest.profileImage = profileImageBase64;
        JSONObject postObject = JsonBuilderParser.jsonBuilder(updateProfileDetailsRequest);

        AndroidNetworking.post(url)
                .addJSONObjectBody(postObject) // posting json
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .addHeaders("X_AUTH_TOKEN", token)
                .addHeaders("Content-Type", "application/json")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_UPDATE_PROFILE);
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_UPDATE_PROFILE);
                    }
                });
    }
}
