package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.StringUtils;
import com.tradebeauser.utils.UserData;

import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ProductsSearchWithFiltersApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetProductsListWithFilters(Context context, Fragment fragment, RecyclerView.Adapter adapter, String searchText, String latitude, String longitude, String limit, String offset, String sortOrder) {
        String url = Constants.PRODUCTS_LIST_URL + "/" + limit + "/" + offset + "/";
        String subCategory = "";
        String subSubCategory = "";
        String brand = "";
        String optionalFilters = "";
        List<String> listOfOptionalFilters = new LinkedList<>();
        LinkedHashMap<String, List<String>> mapOfFinalFilterLabelKeyAndSelectedOptions = UserData.getInstance().getMapOfFinalFilterLabelKeyAndSelectedOptions();
        if(mapOfFinalFilterLabelKeyAndSelectedOptions!=null) {
            List<String> listOfFilterTypes = new LinkedList<>(mapOfFinalFilterLabelKeyAndSelectedOptions.keySet());
           for(int index = 0 ; index < listOfFilterTypes.size() ; index++){
               String filterType = listOfFilterTypes.get(index);
               List<String> listOfValues = mapOfFinalFilterLabelKeyAndSelectedOptions.get(filterType);
               if(listOfValues!=null) {
                   if (filterType.equalsIgnoreCase("sub_categories")) {
                       subCategory = StringUtils.join(listOfValues, ",");
                   }else if(filterType.equalsIgnoreCase("sub_sub_categories")){
                       subSubCategory =StringUtils.join(listOfValues, ",");
                   }else if(filterType.equalsIgnoreCase("brands")){
                       brand =StringUtils.join(listOfValues, ",");
                   }else{
                       listOfOptionalFilters.addAll(listOfValues);
                   }
               }
           }
        }
        optionalFilters = StringUtils.join(listOfOptionalFilters, ",");
        AndroidNetworking.get(url)
                .addQueryParameter("q",searchText)// posting json
                .addQueryParameter("latitude",latitude)
                .addQueryParameter("longitude",longitude)
                .addQueryParameter("price_order",sortOrder)
                .addQueryParameter("sub_cat_id",subCategory)
                .addQueryParameter("sub_sub_cat_id",subSubCategory)
                .addQueryParameter("brand_id",brand)
                .addQueryParameter("options",optionalFilters)
                .setPriority(Priority.HIGH)
                .addHeaders("APP_id", Constants.APP_ID)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethods.handleErrorResponse(context, anError);
                        if (fragment != null) {
                            callBack = (HttpReqResCallBack) fragment;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS);
                        } else if (adapter != null) {
                            callBack = (HttpReqResCallBack) adapter;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS);
                        } else {
                            callBack = (HttpReqResCallBack) context;
                            callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS);
                        }
                    }
                });
    }
}
