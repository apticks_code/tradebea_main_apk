package com.tradebeauser.ApiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.utils.Constants;

import java.util.HashMap;
import java.util.Map;

public class GetRazorPayKeyDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallToGetRazorPayKeyDetails(Context context, Fragment fragment, RecyclerView.Adapter adapter, String token) {
        String url = Constants.PAYMENT_GATEWAY_CREDENTIALS;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (fragment != null) {
                    callBack = (HttpReqResCallBack) fragment;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS);
                } else if (adapter != null) {
                    callBack = (HttpReqResCallBack) adapter;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS);
                } else {
                    callBack = (HttpReqResCallBack) context;
                    callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        requestQueue.add(postRequest);
    }
}
