package com.tradebeauser.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.GetShopCategoriesApiCall;
import com.tradebeauser.ApiCalls.RelatedProductsSearchApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.ShopMenuAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.LocationInfo;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.Data;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.ShopByCategoryDetailsResponse;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.UserData;

import java.util.LinkedList;
import java.util.List;

public class ShopByCategoryFragment extends BaseFragment implements HttpReqResCallBack {

    private RecyclerView rvMenu;

    private List<Data> listOfMenuDetails;
    private String latitude = "";
    private String longitude = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_shop_by_category_fragment, container, false);
        PrepareLocationDetails();
        initializeUi(view);
        initializeListeners();
        PrepareDetails();
        return view;
    }

    private void PrepareLocationDetails() {
        AddressDetails addressDetails = UserData.getInstance().getActiveLocation();
        if (addressDetails != null) {
            LocationInfo locationInfo = addressDetails.getLocation();
            latitude = String.valueOf(addressDetails.getLocation().getLatitude());
            longitude = String.valueOf(addressDetails.getLocation().getLongitude());
        }
    }
    private void initializeUi(View view) {
        rvMenu = view.findViewById(R.id.rvMenu);

        listOfMenuDetails = new LinkedList<>();

    }

    private void initializeListeners() {

    }

    private void PrepareDetails() {
        showProgressBar(getActivity());
        GetShopCategoriesApiCall.serviceCallToGetShopCategories(getActivity(), this, null);
    }

    private void initializeAdapter() {
        if(listOfMenuDetails!=null && listOfMenuDetails.size()!=0) {
            ShopMenuAdapter ShopMenuAdapter = new ShopMenuAdapter(getActivity(), listOfMenuDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvMenu.setLayoutManager(layoutManager);
            rvMenu.setItemAnimator(new DefaultItemAnimator());
            rvMenu.setAdapter(ShopMenuAdapter);
        }
    }


    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_SHOP_BY_CATEGORIES:
                if (jsonResponse != null) {
                    try {
                        ShopByCategoryDetailsResponse menuDetailsResponse = new Gson().fromJson(jsonResponse, ShopByCategoryDetailsResponse.class);
                        if (menuDetailsResponse != null) {
                            boolean status = menuDetailsResponse.getStatus();
                            String message = menuDetailsResponse.getMessage();
                            if (status) {
                                listOfMenuDetails = menuDetailsResponse.getListOfMenuDetails();
                                initializeAdapter();

                            } else {

                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
        }
    }
}
