package com.tradebeauser.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.OrderHistoryApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.MyOrdersFragmentAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.myOrdersResponse.MyOrdersResponse;
import com.tradebeauser.models.responseModels.myOrdersResponse.OrderData;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;

import java.util.LinkedList;

public class MyOrdersFragment extends BaseFragment implements HttpReqResCallBack {

    private TextView tvError;
    private EditText etSearch;
    private RecyclerView rvMyOrderDetails;
    private SwipeRefreshLayout swipeRefreshLayout;

    private LinkedList<OrderData> listOfOrderData;

    private String startDate = "";
    private String endDate = "";
    private String lastDays = "";
    private String lastYear = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_my_orders_fragment, container, false);
        initializeUi(view);
        prepareDetails();
        refreshContent();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        rvMyOrderDetails = view.findViewById(R.id.rvMyOrderDetails);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
    }

    private void prepareDetails() {
        if (getActivity() != null) {
            showProgressBar(getActivity());
            String token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
            OrderHistoryApiCall.serviceCallForOrderHistory(getActivity(), this, null, token, startDate, endDate, lastDays, lastYear);
        }
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_ORDER_HISTORY) {
            if (jsonResponse != null) {
                MyOrdersResponse myOrdersResponse = new Gson().fromJson(jsonResponse, MyOrdersResponse.class);
                if (myOrdersResponse != null) {
                    boolean status = myOrdersResponse.getStatus();
                    String message = myOrdersResponse.getMessage();
                    if (status) {
                        listOfOrderData = myOrdersResponse.getListOfOrderData();
                        if (listOfOrderData != null) {
                            if (listOfOrderData.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        MyOrdersFragmentAdapter myOrdersFragmentAdapter = new MyOrdersFragmentAdapter(getActivity(), listOfOrderData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMyOrderDetails.setLayoutManager(layoutManager);
        rvMyOrderDetails.setItemAnimator(new DefaultItemAnimator());
        rvMyOrderDetails.setAdapter(myOrdersFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvMyOrderDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvMyOrderDetails.setVisibility(View.GONE);
    }
}
