package com.tradebeauser.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.CountDetailsApiCall;
import com.tradebeauser.ApiCalls.DeviceTokenApiCall;
import com.tradebeauser.ApiCalls.GetBrandsApiCall;
import com.tradebeauser.ApiCalls.GetMenuApiCall;
import com.tradebeauser.ApiCalls.GetSliderApiCall;
import com.tradebeauser.ApiCalls.ProductsSearchApiCall;
import com.tradebeauser.ApiCalls.TopDealProductsApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ChooseLocationActivity;
import com.tradebeauser.activities.HomeActivity;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.adapters.BrandDetailsAdapter;
import com.tradebeauser.adapters.MenuDetailsAdapter;
import com.tradebeauser.adapters.HomePageBannerImagesAdapter;
import com.tradebeauser.adapters.TopSearchProductsDetailsAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.interfaces.LocationUpdateCallBack;
import com.tradebeauser.interfaces.SelectedAddressCallBack;
import com.tradebeauser.models.responseModels.brandsDetailsResponse.BrandDetails;
import com.tradebeauser.models.responseModels.brandsDetailsResponse.BrandDetailsResponse;
import com.tradebeauser.models.responseModels.countDetailsResponse.CountDetailsData;
import com.tradebeauser.models.responseModels.countDetailsResponse.CountDetailsResponse;
import com.tradebeauser.models.responseModels.deviceTokenResponse.DeviceTokenResponse;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetails;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetailsResponse;
import com.tradebeauser.models.responseModels.productDetailsResponse.ProductDetailsResponse;
import com.tradebeauser.models.responseModels.productDetailsResponse.ResultDetails;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.LocationInfo;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetails;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetailsResponse;
import com.tradebeauser.utils.AppPermissions;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.GetCurrentLocation;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;

public class HomeFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack, LocationUpdateCallBack, SelectedAddressCallBack {

    private LatLng latLng;
    private EditText etSearch;
    private TextView tvLocation;
    private ViewPager viewPager;
    private RecyclerView rvMenu, rvExploreTopBrands, rvExploreBestDeals;
    private LinearLayout llMenu, llBanners, llDots, llExploreTopBrands, llExploreBestDeals, llDataContainer, llError;
    private ShimmerFrameLayout menuShimmerViewContainer, exploreTopBrandsShimmerViewContainer, exploreBestDetailsShimmerViewContainer;

    private List<ResultDetails> listOfProductDetails;
    private LinkedList<SliderDetails> listOfBannerDetails;
    private LinkedList<MenuDetails> listOfMenuItemDetails;
    private LinkedList<BrandDetails> listOfBrandItemDetails;
    private List<ResultDetails> listOfTopSearchProductDetails;

    private String token = "";
    private String latitude = "";
    private String longitude = "";

    private int count = 0;
    private int activeLocationID = -1;
    private long bannerTime = 2000;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_home_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        preparePushNotificationDetails();
        return view;
    }

    private void initializeUi(View view) {
        llMenu = view.findViewById(R.id.llMenu);
        llDots = view.findViewById(R.id.llDots);
        rvMenu = view.findViewById(R.id.rvMenu);
        llError = view.findViewById(R.id.llError);
        etSearch = view.findViewById(R.id.etSearch);
        viewPager = view.findViewById(R.id.viewPager);
        llBanners = view.findViewById(R.id.llBanners);
        tvLocation = view.findViewById(R.id.tvLocation);
        llDataContainer = view.findViewById(R.id.llDataContainer);
        llExploreTopBrands = view.findViewById(R.id.llExploreTopBrands);
        rvExploreTopBrands = view.findViewById(R.id.rvExploreTopBrands);
        llExploreBestDeals = view.findViewById(R.id.llExploreBestDeals);
        rvExploreBestDeals = view.findViewById(R.id.rvExploreBestDeals);
        menuShimmerViewContainer = view.findViewById(R.id.menuShimmerViewContainer);
        exploreTopBrandsShimmerViewContainer = view.findViewById(R.id.exploreTopBrandsShimmerViewContainer);
        exploreBestDetailsShimmerViewContainer = view.findViewById(R.id.exploreBestDetailsShimmerViewContainer);
    }

    private void initializeListeners() {
        tvLocation.setOnClickListener(this);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        UserData.getInstance().setContext(null);
        UserData.getInstance().setFragment(this);
    }

    private void prepareDetails() {
        prepareLocationDetails();
    }

    private void preparePushNotificationDetails() {
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        String deviceToken = PreferenceConnector.readString(getActivity(), getString(R.string.device_token), "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!deviceToken.isEmpty()) {
                    DeviceTokenApiCall.serviceCallForDeviceToken(getActivity(), HomeFragment.this, null, deviceToken, token);
                }
            }
        }).start();
    }

    private void performSearch() {
        if (getActivity() != null) {
            showProgressBar(getActivity());
            ProductsSearchApiCall.serviceCallToGetProductsList(getActivity(), this, null, etSearch.getText().toString(), latitude, longitude, "10", "0", "");
        }
    }

    private void prepareLocationDetails() {
        AddressDetails addressDetails = UserData.getInstance().getActiveLocation();
        if (addressDetails != null) {
            activeLocationID = addressDetails.getId();
            String tag = addressDetails.getTag();
            String geoLocation = addressDetails.getLocation().getGeoLocation();
            latitude = String.valueOf(addressDetails.getLocation().getLatitude());
            longitude = String.valueOf(addressDetails.getLocation().getLongitude());
            if (tag != null && geoLocation != null) {
                tvLocation.setText(String.format("%s - %s", tag, geoLocation));
                prepareCartCountDetails();
            } else {
                checkLocationAccessPermission();
            }
        } else {
            checkLocationAccessPermission();
        }
    }

    private void checkLocationAccessPermission() {
        if (getActivity() != null) {
            if (AppPermissions.checkPermissionForAccessLocation(getActivity())) {
                showProgressBar(getActivity());
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                new GetCurrentLocation(getActivity(), this, locationManager);
            } else {
                AppPermissions.requestPermissionForLocation(getActivity());
            }
        }
    }

    @Override
    public void OnLatLongReceived(double latitude, double longitude) {
        latLng = new LatLng(latitude, longitude);
        this.latitude = String.valueOf(latLng.latitude);
        this.longitude = String.valueOf(latLng.longitude);
        prepareCartCountDetails();
        getCurrentLocation();
        closeProgressbar();
    }

    @SuppressLint("SetTextI18n")
    private void getCurrentLocation() {
        String errorMessage = "";
        latitude = String.valueOf(latLng.latitude);
        longitude = String.valueOf(latLng.longitude);
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (Exception exception) {
            errorMessage = getString(R.string.unable_to_fetch_address_details);
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        }

        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        } else {
            Address address = addresses.get(0);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                builder.append(address.getAddressLine(i));
            }
            tvLocation.setText("Deliver to " + builder.toString());
            AddressDetails addressDetails = new AddressDetails();
            LocationInfo locationInfo = new LocationInfo();
            locationInfo.setGeoLocation(builder.toString());
            locationInfo.setLatitude(latitude);
            locationInfo.setLongitude(longitude);
            addressDetails.setLocation(locationInfo);
            UserData.getInstance().setActiveLocation(addressDetails);
        }
    }

    private void prepareMenuDetails() {
        menuShimmerViewContainer.startShimmer();
        new Thread(new Runnable() {
            @Override
            public void run() {
                GetMenuApiCall.serviceCallToGetMenu(getActivity(), HomeFragment.this, null);
            }
        }).start();
    }

    private void prepareExploreBestDetails() {
        exploreBestDetailsShimmerViewContainer.startShimmer();
        new Thread(new Runnable() {
            @Override
            public void run() {
                TopDealProductsApiCall.serviceCallToGetTopDealProductsList(getActivity(), HomeFragment.this, null, latitude, longitude, "10", "0", "DESC");
            }
        }).start();
    }

    private void prepareBannerDetails() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                GetSliderApiCall.serviceCallToGetSlider(getActivity(), HomeFragment.this, null);
            }
        }).start();
    }

    private void prepareTopBrandDetails() {
        exploreTopBrandsShimmerViewContainer.startShimmer();
        new Thread(new Runnable() {
            @Override
            public void run() {
                GetBrandsApiCall.serviceCallToGetBrands(getActivity(), HomeFragment.this, null);
            }
        }).start();
    }

    private void prepareCartCountDetails() {
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
        showProgressBar(getActivity());
        CountDetailsApiCall.serviceCallForCountDetails(getActivity(), HomeFragment.this, null, token, activeLocationID, latitude, longitude);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvLocation:
                goToAddOrChangeLocation();
                break;
            default:
                break;
        }
    }

    private void goToAddOrChangeLocation() {
        Intent intent = new Intent(getActivity(), ChooseLocationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.active_location_id), activeLocationID);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH:
                if (getActivity() != null) {
                    if (jsonResponse != null) {
                        try {
                            ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                            if (productDetailsResponse != null) {
                                boolean status = productDetailsResponse.getStatus();
                                String message = productDetailsResponse.getMessage();
                                if (status) {
                                    listOfProductDetails = productDetailsResponse.getData().getListOfResults();
                                    if (listOfProductDetails != null && listOfProductDetails.size() != 0) {
                                        JSONObject jsonRootObject = new JSONObject(jsonResponse);
                                        JSONObject data = jsonRootObject.getJSONObject("data");
                                        JSONObject filters = data.getJSONObject("filters");
                                        UserData.getInstance().setFilterDetails(filters);
                                        goToProductsListActivity();
                                    } else {
                                        Toast.makeText(getActivity(), Objects.requireNonNull(getActivity()).getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), Objects.requireNonNull(getActivity()).getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), Objects.requireNonNull(getActivity()).getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                            Toast.makeText(getActivity(), Objects.requireNonNull(getActivity()).getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    }
                    closeProgressbar();
                }
                break;
            case Constants.SERVICE_CALL_TO_GET_MENU:
                if (getActivity() != null) {
                    if (jsonResponse != null) {
                        try {
                            MenuDetailsResponse menuDetailsResponse = new Gson().fromJson(jsonResponse, MenuDetailsResponse.class);
                            if (menuDetailsResponse != null) {
                                boolean status = menuDetailsResponse.getStatus();
                                String message = menuDetailsResponse.getMessage();
                                if (status) {
                                    listOfMenuItemDetails = menuDetailsResponse.getListOfMenuItemDetails();
                                    if (listOfMenuItemDetails != null) {
                                        if (listOfMenuItemDetails.size() != 0) {
                                            menuIsFull();
                                            initializeMenuAdapter();
                                        } else {
                                            menuIsEmpty();
                                        }
                                    } else {
                                        menuIsEmpty();
                                    }
                                } else {
                                    menuIsEmpty();
                                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception exception) {
                            menuIsEmpty();
                            exception.printStackTrace();
                        }
                    }
                    menuShimmerViewContainer.stopShimmer();
                    menuShimmerViewContainer.setVisibility(View.GONE);
                }
                break;
            case Constants.SERVICE_CALL_FOR_TOP_DEAL_PRODUCTS:
                if (getActivity() != null) {
                    if (jsonResponse != null) {
                        try {
                            ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                            if (productDetailsResponse != null) {
                                boolean status = productDetailsResponse.getStatus();
                                String message = productDetailsResponse.getMessage();
                                if (status) {
                                    listOfTopSearchProductDetails = productDetailsResponse.getData().getListOfResults();
                                    if (listOfTopSearchProductDetails != null) {
                                        if (listOfTopSearchProductDetails.size() != 0) {
                                            exploreBestDetailsIsFull();
                                            initializeTopDealsAdapter();
                                        } else {
                                            exploreBestDetailsIsEmpty();
                                        }
                                    } else {
                                        exploreBestDetailsIsEmpty();
                                    }
                                } else {
                                    exploreBestDetailsIsEmpty();
                                }
                            } else {
                                exploreBestDetailsIsEmpty();
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                            exploreBestDetailsIsEmpty();
                        }
                    }
                    exploreBestDetailsShimmerViewContainer.stopShimmer();
                    exploreBestDetailsShimmerViewContainer.setVisibility(View.GONE);
                }
                break;
            case Constants.SERVICE_CALL_TO_GET_SLIDERS:
                listOfBannerDetails = new LinkedList<>();
                if (getActivity() != null) {
                    if (jsonResponse != null) {
                        try {
                            SliderDetailsResponse sliderDetailsResponse = new Gson().fromJson(jsonResponse, SliderDetailsResponse.class);
                            if (sliderDetailsResponse != null) {
                                boolean status = sliderDetailsResponse.getStatus();
                                String message = sliderDetailsResponse.getMessage();
                                if (status) {
                                    listOfBannerDetails = sliderDetailsResponse.getData().getSliders();
                                    if (listOfBannerDetails != null) {
                                        if (listOfBannerDetails.size() != 0) {
                                            llBanners.setVisibility(View.VISIBLE);
                                            prepareViewPagerDetails();
                                        } else {
                                            llBanners.setVisibility(View.GONE);
                                        }
                                    } else {
                                        llBanners.setVisibility(View.GONE);
                                    }
                                } else {
                                    llBanners.setVisibility(View.GONE);
                                }
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                }
                break;
            case Constants.SERVICE_CALL_TO_GET_BRANDS:
                if (getActivity() != null) {
                    if (jsonResponse != null) {
                        try {
                            BrandDetailsResponse brandDetailsResponse = new Gson().fromJson(jsonResponse, BrandDetailsResponse.class);
                            if (brandDetailsResponse != null) {
                                boolean status = brandDetailsResponse.getStatus();
                                String message = brandDetailsResponse.getMessage();
                                if (status) {
                                    listOfBrandItemDetails = brandDetailsResponse.getListOfBrandItemDetails();
                                    if (listOfBrandItemDetails != null) {
                                        if (listOfBrandItemDetails.size() != 0) {
                                            brandsIsFull();
                                            initializeBrandAdapter();
                                        } else {
                                            brandsIsEmpty();
                                        }
                                    } else {
                                        brandsIsEmpty();
                                    }
                                } else {
                                    brandsIsEmpty();
                                }
                            } else {
                                brandsIsEmpty();
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                    exploreTopBrandsShimmerViewContainer.stopShimmer();
                    exploreTopBrandsShimmerViewContainer.setVisibility(View.GONE);
                }
                break;
            case Constants.SERVICE_CALL_FOR_COUNT_DETAILS:
                if (jsonResponse != null) {
                    CountDetailsResponse countDetailsResponse = new Gson().fromJson(jsonResponse, CountDetailsResponse.class);
                    if (countDetailsResponse != null) {
                        boolean status = countDetailsResponse.getStatus();
                        if (status) {
                            CountDetailsData countDetailsData = countDetailsResponse.getCountDetailsData();
                            if (countDetailsData != null) {
                                int cartCount = countDetailsData.getCartCount();
                                int notificationCount = countDetailsData.getNotificationsCount();
                                int isServiceAvailable = countDetailsData.getIsServiceAvailable();
                                if (isServiceAvailable == 1) {
                                    serviceAvailable();
                                    prepareMenuDetails();
                                    prepareExploreBestDetails();
                                    prepareBannerDetails();
                                    prepareTopBrandDetails();
                                } else {
                                    serviceNotAvailable();
                                }
                            }
                        } else {
                            serviceNotAvailable();
                        }
                    }
                } else {
                    serviceNotAvailable();
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_DEVICE_TOKEN:
                if (jsonResponse != null) {
                    DeviceTokenResponse deviceTokenResponse = new Gson().fromJson(jsonResponse, DeviceTokenResponse.class);
                    if (deviceTokenResponse != null) {
                        boolean status = deviceTokenResponse.getStatus();
                        if (status) {
                            String message = deviceTokenResponse.getMessage();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    private void serviceAvailable() {
        llError.setVisibility(View.GONE);
        llDataContainer.setVisibility(View.VISIBLE);
    }

    private void serviceNotAvailable() {
        llError.setVisibility(View.VISIBLE);
        llDataContainer.setVisibility(View.GONE);
    }

    private void prepareViewPagerDetails() {
        HomePageBannerImagesAdapter homePageBannerImagesAdapter = new HomePageBannerImagesAdapter(getActivity(), listOfBannerDetails);
        viewPager.setAdapter(homePageBannerImagesAdapter);
        for (int index = 0; index < homePageBannerImagesAdapter.getCount(); index++) {
            ImageButton ibDot = new ImageButton(getActivity());
            ibDot.setTag(index);
            ibDot.setImageResource(R.drawable.dot_selector);
            ibDot.setBackgroundResource(0);
            ibDot.setPadding(0, 0, 5, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(50, 50);
            ibDot.setLayoutParams(params);
            if (index == 0)
                ibDot.setSelected(true);
            llDots.addView(ibDot);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int index = 0; index < homePageBannerImagesAdapter.getCount(); index++) {
                    if (index != position) {
                        (llDots.findViewWithTag(index)).setSelected(false);
                    }
                }
                (llDots.findViewWithTag(position)).setSelected(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask(), bannerTime, bannerTime);
    }

    private class TimerTask extends java.util.TimerTask {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (listOfBannerDetails.size() - 1 != count) {
                            count = count + 1;
                            viewPager.setCurrentItem(count);
                        } else {
                            viewPager.setCurrentItem(0);
                            count = 0;
                        }
                    }
                });
            }
        }
    }

    private void goToProductsListActivity() {
        Intent productsIntent = new Intent(getActivity(), ProductsListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.searched_text), etSearch.getText().toString());
        bundle.putSerializable(getString(R.string.products), (Serializable) listOfProductDetails);
        productsIntent.putExtras(bundle);
        startActivity(productsIntent);
    }

    private void initializeMenuAdapter() {
        MenuDetailsAdapter menuDetailsAdapter = new MenuDetailsAdapter(getActivity(), listOfMenuItemDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvMenu.setLayoutManager(layoutManager);
        rvMenu.setItemAnimator(new DefaultItemAnimator());
        rvMenu.setAdapter(menuDetailsAdapter);
    }

    private void initializeTopDealsAdapter() {
        TopSearchProductsDetailsAdapter topSearchProductsDetailsAdapter = new TopSearchProductsDetailsAdapter(getActivity(), listOfTopSearchProductDetails, latitude, longitude);
        rvExploreBestDeals.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rvExploreBestDeals.setItemAnimator(new DefaultItemAnimator());
        rvExploreBestDeals.setAdapter(topSearchProductsDetailsAdapter);
    }

    private void initializeBrandAdapter() {
        BrandDetailsAdapter brandDetailsAdapter = new BrandDetailsAdapter(getActivity(), listOfBrandItemDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvExploreTopBrands.setLayoutManager(layoutManager);
        rvExploreTopBrands.setItemAnimator(new DefaultItemAnimator());
        rvExploreTopBrands.setAdapter(brandDetailsAdapter);
    }

    private void menuIsFull() {
        rvMenu.setVisibility(View.VISIBLE);
        llMenu.setVisibility(View.VISIBLE);
    }

    private void menuIsEmpty() {
        rvMenu.setVisibility(View.GONE);
        llMenu.setVisibility(View.GONE);
    }

    private void exploreBestDetailsIsFull() {
        rvExploreBestDeals.setVisibility(View.VISIBLE);
        llExploreBestDeals.setVisibility(View.VISIBLE);
    }

    private void exploreBestDetailsIsEmpty() {
        rvExploreBestDeals.setVisibility(View.GONE);
        llExploreBestDeals.setVisibility(View.GONE);
    }

    private void brandsIsFull() {
        rvExploreTopBrands.setVisibility(View.VISIBLE);
        llExploreTopBrands.setVisibility(View.VISIBLE);
    }

    private void brandsIsEmpty() {
        rvExploreTopBrands.setVisibility(View.GONE);
        llExploreTopBrands.setVisibility(View.GONE);
    }

    @Override
    public void selectedAddress(AddressDetails addressDetails) {
        prepareDetails();
    }
}
