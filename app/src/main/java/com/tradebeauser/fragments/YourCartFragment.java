package com.tradebeauser.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tradebeauser.ApiCalls.CheckoutProductsApiCall;
import com.tradebeauser.ApiCalls.GetCartProductsApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductCheckoutActivity;
import com.tradebeauser.adapters.CartProductsAdapter;
import com.tradebeauser.adapters.SavedProductsAdapter;
import com.tradebeauser.interfaces.CloseCallBack;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.CartProductsResponse.CartProductDetailsResponse;
import com.tradebeauser.models.responseModels.CartProductsResponse.Data;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.CheckoutProductDetailsResponse;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import java.util.LinkedList;
import java.util.List;

public class YourCartFragment extends BaseFragment implements View.OnClickListener, HttpReqResCallBack, CloseCallBack {

    private JsonArray productsJsonArray;
    private JsonObject checkoutRequestObject;
    private LinearLayout llCartItems, llSavedItems;
    private RecyclerView rvCartProducts, rvSavedProducts;
    private TextView tvCheckoutTop, tvCheckoutBottom, tvSubTotal, tvSaveForLater;
    private com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data checkoutData;

    private List<Data> listOfCartProductDetails;
    private List<Data> listOfTotalProductDetails;
    private List<Data> listOfSavedProductDetails;

    private String token = "";
    private String appliedPromoCode = "";

    private double totalPrice = 0.0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_your_cart_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareCartProducts();
        return view;
    }

    private void initializeUi(View view) {
        tvSubTotal = view.findViewById(R.id.tvSubTotal);
        llCartItems = view.findViewById(R.id.llCartItems);
        llSavedItems = view.findViewById(R.id.llSavedItems);
        tvSaveForLater = view.findViewById(R.id.tvSaveForLater);
        tvCheckoutTop = view.findViewById(R.id.tvCheckoutTop);
        rvCartProducts = view.findViewById(R.id.rvCartProducts);
        rvSavedProducts = view.findViewById(R.id.rvSavedProducts);
        tvCheckoutBottom = view.findViewById(R.id.tvCheckoutBottom);

        UserData.getInstance().setCheckoutRequestObject(null);
        token = PreferenceConnector.readString(getActivity(), getString(R.string.user_token), "");
    }

    private void initializeListeners() {
        tvCheckoutTop.setOnClickListener(this);
        tvCheckoutBottom.setOnClickListener(this);
    }

    private void prepareCartProducts() {
        totalPrice = 0.0;
        listOfCartProductDetails = new LinkedList<>();
        listOfTotalProductDetails = new LinkedList<>();
        listOfSavedProductDetails = new LinkedList<>();
        productsJsonArray = new JsonArray();
        showProgressBar(getActivity());
        GetCartProductsApiCall.serviceCallToGetCartProducts(getActivity(), this, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.tvCheckoutTop:
            case R.id.tvCheckoutBottom:
                prepareCheckout();
                break;
            default:
                break;
        }
    }

    private void prepareCheckout() {
        checkoutRequestObject = new JsonObject();
        checkoutRequestObject.addProperty("address_id", "");
        checkoutRequestObject.addProperty("applied_promo_code", appliedPromoCode);
        checkoutRequestObject.add("products", productsJsonArray);
        UserData.getInstance().setCheckoutRequestObject(checkoutRequestObject);
        showProgressBar(getActivity());
        CheckoutProductsApiCall.serviceCallToCheckoutProducts(getActivity(), this, null, token, checkoutRequestObject);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_GET_CART_PRODUCTS:
                if (jsonResponse != null) {
                    try {
                        CartProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, CartProductDetailsResponse.class);
                        if (productDetailsResponse != null) {
                            boolean status = productDetailsResponse.getStatus();
                            String message = productDetailsResponse.getMessage();
                            if (status) {
                                listOfTotalProductDetails = productDetailsResponse.getData();
                                if (listOfTotalProductDetails != null && listOfTotalProductDetails.size() != 0) {
                                    for (int index = 0; index < listOfTotalProductDetails.size(); index++) {
                                        String productStatus = listOfTotalProductDetails.get(index).getStatus();
                                        if (productStatus != null) {
                                            if (productStatus.equalsIgnoreCase("1")) {
                                                listOfCartProductDetails.add(listOfTotalProductDetails.get(index));
                                                int quantity = Integer.parseInt(listOfTotalProductDetails.get(index).getQuantity());
                                                String variantID = listOfTotalProductDetails.get(index).getVariantId();
                                                String productID = listOfTotalProductDetails.get(index).getProductId();
                                                String price = listOfTotalProductDetails.get(index).getListOfVariantDetails().get(0).getMrp();
                                                String discount = listOfTotalProductDetails.get(index).getListOfVariantDetails().get(0).getDiscount();
                                                Double salePrice = null;
                                                if (price != null && !price.isEmpty() && discount != null && !discount.isEmpty()) {
                                                    salePrice = CommonMethods.getSalePrice(discount, price);
                                                    salePrice = quantity * salePrice;
                                                    totalPrice = totalPrice + salePrice;

                                                }
                                                JsonObject jsonObject = new JsonObject();
                                                jsonObject.addProperty("product_id", productID);
                                                jsonObject.addProperty("variant_id", variantID);
                                                jsonObject.addProperty("qty", quantity);
                                                productsJsonArray.add(jsonObject);
                                            } else if (productStatus.equalsIgnoreCase("2")) {
                                                listOfSavedProductDetails.add(listOfTotalProductDetails.get(index));
                                            }
                                        }
                                    }
                                    tvSubTotal.setText("Total" + "( " + listOfCartProductDetails.size() + " items )" + " " + ":" + " " + getString(R.string.rs) + totalPrice);
                                    tvSaveForLater.setText(String.format("Saved for later (%s items)", listOfSavedProductDetails.size()));

                                    if (listOfCartProductDetails != null) {
                                        if (listOfCartProductDetails.size() != 0) {
                                            listCartProductsFull();
                                            initializeCartProductsAdapter();
                                        } else {
                                            listCartProductsEmpty();
                                        }
                                    } else {
                                        listCartProductsEmpty();
                                    }

                                    if (listOfSavedProductDetails != null) {
                                        if (listOfSavedProductDetails.size() != 0) {
                                            listSavedProductsFull();
                                            initializeSavedProductsAdapter();
                                        } else {
                                            listSavedProductsEmpty();
                                        }
                                    } else {
                                        listSavedProductsEmpty();
                                    }

                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_CHECKOUT_PRODUCTS:
                if (jsonResponse != null) {
                    try {
                        CheckoutProductDetailsResponse checkoutProductDetailsResponse = new Gson().fromJson(jsonResponse, CheckoutProductDetailsResponse.class);
                        if (checkoutProductDetailsResponse != null) {
                            boolean status = checkoutProductDetailsResponse.getStatus();
                            String message = checkoutProductDetailsResponse.getMessage();
                            if (status) {
                                checkoutData = checkoutProductDetailsResponse.getData();
                                UserData.getInstance().setCheckoutData(checkoutData);
                                gotToCheckout();
                            } else {
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void gotToCheckout() {
        if (getActivity() != null) {
            Intent productCheckoutIntent = new Intent(getActivity(), ProductCheckoutActivity.class);
            startActivity(productCheckoutIntent);
        }
    }

    private void initializeCartProductsAdapter() {
        CartProductsAdapter productsListAdapter = new CartProductsAdapter(getActivity(), listOfCartProductDetails, token);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCartProducts.setLayoutManager(layoutManager);
        rvCartProducts.setItemAnimator(new DefaultItemAnimator());
        rvCartProducts.setAdapter(productsListAdapter);
    }

    private void initializeSavedProductsAdapter() {
        SavedProductsAdapter productsListAdapter = new SavedProductsAdapter(getActivity(), listOfSavedProductDetails, token);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvSavedProducts.setLayoutManager(layoutManager);
        rvSavedProducts.setItemAnimator(new DefaultItemAnimator());
        rvSavedProducts.setAdapter(productsListAdapter);
    }

    @Override
    public void close() {
        prepareCartProducts();
    }

    private void listCartProductsFull() {
        llCartItems.setVisibility(View.VISIBLE);
    }

    private void listCartProductsEmpty() {
        llCartItems.setVisibility(View.GONE);
    }

    private void listSavedProductsFull() {
        llSavedItems.setVisibility(View.VISIBLE);
    }

    private void listSavedProductsEmpty() {
        llSavedItems.setVisibility(View.GONE);
    }
}
