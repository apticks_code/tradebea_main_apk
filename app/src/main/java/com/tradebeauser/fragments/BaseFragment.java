package com.tradebeauser.fragments;

import android.app.Dialog;
import android.content.Context;
import androidx.fragment.app.Fragment;

import com.tradebeauser.utils.DialogUtils;

public class BaseFragment extends Fragment {

    private Dialog progressDialog;

    public void showProgressBar(Context context) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
