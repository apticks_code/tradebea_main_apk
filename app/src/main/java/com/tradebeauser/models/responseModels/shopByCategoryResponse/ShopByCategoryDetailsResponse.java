package com.tradebeauser.models.responseModels.shopByCategoryResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopByCategoryDetailsResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;

    public List<Data> getListOfMenuDetails() {
        return listOfMenuDetails;
    }

    public void setListOfMenuDetails(List<Data> listOfMenuDetails) {
        this.listOfMenuDetails = listOfMenuDetails;
    }

    @SerializedName("data")
    @Expose
    private List<Data> listOfMenuDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
