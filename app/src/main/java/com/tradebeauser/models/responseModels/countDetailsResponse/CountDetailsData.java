package com.tradebeauser.models.responseModels.countDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountDetailsData {

    @SerializedName("cart_count")
    @Expose
    private Integer cartCount;
    @SerializedName("notifications_count")
    @Expose
    private Integer notificationsCount;
    @SerializedName("default_address")
    @Expose
    private DefaultAddress defaultAddress;
    @SerializedName("is_service_available")
    @Expose
    private Integer isServiceAvailable;


    public Integer getCartCount() {
        return cartCount;
    }

    public void setCartCount(Integer cartCount) {
        this.cartCount = cartCount;
    }

    public Integer getNotificationsCount() {
        return notificationsCount;
    }

    public void setNotificationsCount(Integer notificationsCount) {
        this.notificationsCount = notificationsCount;
    }

    public Integer getIsServiceAvailable() {
        return isServiceAvailable;
    }

    public void setIsServiceAvailable(Integer isServiceAvailable) {
        this.isServiceAvailable = isServiceAvailable;
    }

    public DefaultAddress getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(DefaultAddress defaultAddress) {
        this.defaultAddress = defaultAddress;
    }
}
