package com.tradebeauser.models.responseModels.CartProductsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    private String id;
    private String status;
    @SerializedName("created_user_id")
    @Expose
    private String createdUserId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("variant_id")
    @Expose
    private String variantId;
    @SerializedName("qty")
    @Expose
    private String quantity;
    @SerializedName("product")
    @Expose
    private Product productDetails;
    @SerializedName("variant")
    @Expose
    private List<VariantDetails> listOfVariantDetails;
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Product getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(Product productDetails) {
        this.productDetails = productDetails;
    }

    public List<VariantDetails> getListOfVariantDetails() {
        return listOfVariantDetails;
    }

    public void setListOfVariantDetails(List<VariantDetails> listOfVariantDetails) {
        this.listOfVariantDetails = listOfVariantDetails;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
