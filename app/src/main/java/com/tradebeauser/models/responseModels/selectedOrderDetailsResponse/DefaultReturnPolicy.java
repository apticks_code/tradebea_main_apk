package com.tradebeauser.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class DefaultReturnPolicy {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("return_before_in_hrs")
    @Expose
    private Integer returnBeforeInHrs;
    @SerializedName("return_title")
    @Expose
    private String returnTitle;
    @SerializedName("return_policies")
    @Expose
    private String returnPolicies;
    @SerializedName("default_return_reasons")
    @Expose
    private LinkedList<DefaultReturnReason> listOfDefaultReturnReasons = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReturnBeforeInHrs() {
        return returnBeforeInHrs;
    }

    public void setReturnBeforeInHrs(Integer returnBeforeInHrs) {
        this.returnBeforeInHrs = returnBeforeInHrs;
    }

    public String getReturnTitle() {
        return returnTitle;
    }

    public void setReturnTitle(String returnTitle) {
        this.returnTitle = returnTitle;
    }

    public String getReturnPolicies() {
        return returnPolicies;
    }

    public void setReturnPolicies(String returnPolicies) {
        this.returnPolicies = returnPolicies;
    }

    public LinkedList<DefaultReturnReason> getListOfDefaultReturnReasons() {
        return listOfDefaultReturnReasons;
    }

    public void setListOfDefaultReturnReasons(LinkedList<DefaultReturnReason> listOfDefaultReturnReasons) {
        this.listOfDefaultReturnReasons = listOfDefaultReturnReasons;
    }
}
