package com.tradebeauser.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetail {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("seller_variant_id")
    @Expose
    private Object sellerVariantId;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("delivery_fee")
    @Expose
    private Double deliveryFee;
    @SerializedName("sub_total")
    @Expose
    private Double subTotal;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("promo_discount")
    @Expose
    private Double promoDiscount;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("varinat")
    @Expose
    private Varinat varinat;
    @SerializedName("seller_varinat")
    @Expose
    private SellerVarinat sellerVarinat;
    @SerializedName("rules")
    @Expose
    private Rules rules;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public Object getSellerVariantId() {
        return sellerVariantId;
    }

    public void setSellerVariantId(Object sellerVariantId) {
        this.sellerVariantId = sellerVariantId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(Double promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Varinat getVarinat() {
        return varinat;
    }

    public void setVarinat(Varinat varinat) {
        this.varinat = varinat;
    }

    public SellerVarinat getSellerVarinat() {
        return sellerVarinat;
    }

    public void setSellerVarinat(SellerVarinat sellerVarinat) {
        this.sellerVarinat = sellerVarinat;
    }

    public Rules getRules() {
        return rules;
    }

    public void setRules(Rules rules) {
        this.rules = rules;
    }
}
