package com.tradebeauser.models.responseModels.productDescriptionResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VariantDetails {

    @SerializedName("variant_code")
    @Expose
    private String variantCode;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("qty")
    @Expose
    private String quantity;
    private String id;
    private String mrp;
    private String discount;
    @SerializedName("variant_values")
    @Expose
    private List<VariantValues> listOfVariantValues;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVariantCode() {
        return variantCode;
    }

    public void setVariantCode(String variantCode) {
        this.variantCode = variantCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public List<VariantValues> getListOfVariantValues() {
        return listOfVariantValues;
    }

    public void setListOfVariantValues(List<VariantValues> listOfVariantValues) {
        this.listOfVariantValues = listOfVariantValues;
    }
}
