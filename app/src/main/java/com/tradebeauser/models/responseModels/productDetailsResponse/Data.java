package com.tradebeauser.models.responseModels.productDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetails;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class Data  implements Serializable {
    @SerializedName("result")
    @Expose
    private List<ResultDetails> listOfResults = null;
    @SerializedName("count")
    @Expose
    private String count;

    @SerializedName("filters")
    @Expose
    private JSONObject filters;

    public List<ResultDetails> getListOfResults() {
        return listOfResults;
    }

    public void setListOfResults(List<ResultDetails> listOfResults) {
        this.listOfResults = listOfResults;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public JSONObject getFilters() {
        return filters;
    }

    public void setFilters(JSONObject filters) {
        this.filters = filters;
    }
}
