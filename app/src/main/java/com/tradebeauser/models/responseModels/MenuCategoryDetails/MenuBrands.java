package com.tradebeauser.models.responseModels.MenuCategoryDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuBrands {
    @SerializedName("id")
    @Expose
    private String ID;
    private String name;
    private String image;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
