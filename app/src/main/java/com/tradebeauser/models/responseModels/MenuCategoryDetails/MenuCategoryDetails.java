package com.tradebeauser.models.responseModels.MenuCategoryDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MenuCategoryDetails {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ext")
    @Expose
    private String extension;



    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("categories")
    @Expose
    private List<CategoriesInfoDetails> listOfCategoriesDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<CategoriesInfoDetails> getListOfCategoriesDetails() {
        return listOfCategoriesDetails;
    }

    public void setListOfCategoriesDetails(List<CategoriesInfoDetails> listOfCategoriesDetails) {
        this.listOfCategoriesDetails = listOfCategoriesDetails;
    }
}
