package com.tradebeauser.models.responseModels.MenuCategoryDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetails;

import org.json.JSONArray;

import java.util.List;

public class Data {
    @SerializedName("menu_details")
    @Expose
    private MenuCategoryDetails menuDetails;
    @SerializedName("menu_banners")
    @Expose
    private List<String> listOfMenuBanners;
    @SerializedName("menu_brands")
    @Expose
    private List<MenuBrands> listOfMenuBrands;

    public MenuCategoryDetails getMenuDetails() {
        return menuDetails;
    }

    public void setMenuDetails(MenuCategoryDetails menuDetails) {
        this.menuDetails = menuDetails;
    }

    public List<String> getListOfMenuBanners() {
        return listOfMenuBanners;
    }

    public void setListOfMenuBanners(List<String> listOfMenuBanners) {
        this.listOfMenuBanners = listOfMenuBanners;
    }

    public List<MenuBrands> getListOfMenuBrands() {
        return listOfMenuBrands;
    }

    public void setListOfMenuBrands(List<MenuBrands> listOfMenuBrands) {
        this.listOfMenuBrands = listOfMenuBrands;
    }
}
