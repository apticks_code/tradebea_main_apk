package com.tradebeauser.models.responseModels.productDescriptionResponse;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.brandsDetailsResponse.BrandDetails;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetails;

import java.util.List;

public class Data {
    private String id;
    private String name;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("desc")
    @Expose
    private String description;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("cat_id")
    @Expose
    private String categoryId;
    @SerializedName("sub_cat_id")
    @Expose
    private String subCategoryId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    private String subSubCategoryId;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("menu")
    @Expose
    private MenuDetails menuDetails;
    @SerializedName("category")
    @Expose
    private CategoryDetails categoryDetails;
    @SerializedName("sub_category")
    @Expose
    private SubCategoryDetails subCategoryDetails;
    @SerializedName("sub_sub_category")
    @Expose
    private SubSubCategoryDetails subSubCategoryDetails;
    @SerializedName("brand")
    @Expose
    private BrandDetails brandDetails;
    @SerializedName("product_images")
    @Expose
    private List<ProductImageDetails> listOfProductImageDetails;
    @SerializedName("variants")
    @Expose
    private List<VariantDetails> listOfVariantDetails;
    @SerializedName("options")
    @Expose
    private List<OptionDetails> listOfOptionDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubSubCategoryId() {
        return subSubCategoryId;
    }

    public void setSubSubCategoryId(String subSubCategoryId) {
        this.subSubCategoryId = subSubCategoryId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public MenuDetails getMenuDetails() {
        return menuDetails;
    }

    public void setMenuDetails(MenuDetails menuDetails) {
        this.menuDetails = menuDetails;
    }

    public CategoryDetails getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(CategoryDetails categoryDetails) {
        this.categoryDetails = categoryDetails;
    }

    public SubCategoryDetails getSubCategoryDetails() {
        return subCategoryDetails;
    }

    public void setSubCategoryDetails(SubCategoryDetails subCategoryDetails) {
        this.subCategoryDetails = subCategoryDetails;
    }

    public SubSubCategoryDetails getSubSubCategoryDetails() {
        return subSubCategoryDetails;
    }

    public void setSubSubCategoryDetails(SubSubCategoryDetails subSubCategoryDetails) {
        this.subSubCategoryDetails = subSubCategoryDetails;
    }

    public BrandDetails getBrandDetails() {
        return brandDetails;
    }

    public void setBrandDetails(BrandDetails brandDetails) {
        this.brandDetails = brandDetails;
    }

    public List<ProductImageDetails> getListOfProductImageDetails() {
        return listOfProductImageDetails;
    }

    public void setListOfProductImageDetails(List<ProductImageDetails> listOfProductImageDetails) {
        this.listOfProductImageDetails = listOfProductImageDetails;
    }

    public List<VariantDetails> getListOfVariantDetails() {
        return listOfVariantDetails;
    }

    public void setListOfVariantDetails(List<VariantDetails> listOfVariantDetails) {
        this.listOfVariantDetails = listOfVariantDetails;
    }

    public List<OptionDetails> getListOfOptionDetails() {
        return listOfOptionDetails;
    }

    public void setListOfOptionDetails(List<OptionDetails> listOfOptionDetails) {
        this.listOfOptionDetails = listOfOptionDetails;
    }
}
