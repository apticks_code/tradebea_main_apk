package com.tradebeauser.models.responseModels.topSearchProductsResponse;

public class TopSearchProductDetails {
    private String id;
    private String name;
    private String discount;
    private int image;

    public TopSearchProductDetails(int image, String discount) {
        this.image = image;
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
