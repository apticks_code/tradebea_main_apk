package com.tradebeauser.models.responseModels.sliderDetailsResponse;

public class SliderDetails {
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
