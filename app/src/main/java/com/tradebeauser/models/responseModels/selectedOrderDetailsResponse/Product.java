package com.tradebeauser.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("product_images")
    @Expose
    private LinkedList<ProductImage> listOfProductImages;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<ProductImage> getListOfProductImages() {
        return listOfProductImages;
    }

    public void setListOfProductImages(LinkedList<ProductImage> listOfProductImages) {
        this.listOfProductImages = listOfProductImages;
    }
}
