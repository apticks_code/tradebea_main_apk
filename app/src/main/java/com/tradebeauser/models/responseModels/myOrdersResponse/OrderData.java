package com.tradebeauser.models.responseModels.myOrdersResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.CustomerAppStatus;

import java.util.LinkedList;

public class OrderData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("track_id")
    @Expose
    private String trackId;
    @SerializedName("order_delivery_otp")
    @Expose
    private String orderDeliveryOTP;
    @SerializedName("preparation_time")
    @Expose
    private Object preparationTime;
    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("promo_id")
    @Expose
    private Object promoId;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("order_status")
    @Expose
    private Integer orderStatus;
    @SerializedName("seller_updated_at")
    @Expose
    private Object sellerUpdatedAt;
    @SerializedName("delivery_job_id")
    @Expose
    private Object deliveryJobId;
    @SerializedName("job_id")
    @Expose
    private Object jobId;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("delivery_partner_user_id")
    @Expose
    private Object deliveryPartnerUserId;
    @SerializedName("order_details")
    @Expose
    private LinkedList<OrderDetails> listOfOrderDetails;
    @SerializedName("customer_app_statuses")
    @Expose
    private LinkedList<CustomerAppStatus> listOfCustomerAppStatuses = null;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getOrderDeliveryOTP() {
        return orderDeliveryOTP;
    }

    public void setOrderDeliveryOTP(String orderDeliveryOTP) {
        this.orderDeliveryOTP = orderDeliveryOTP;
    }

    public Object getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Object preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Object getPromoId() {
        return promoId;
    }

    public void setPromoId(Object promoId) {
        this.promoId = promoId;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Object getSellerUpdatedAt() {
        return sellerUpdatedAt;
    }

    public void setSellerUpdatedAt(Object sellerUpdatedAt) {
        this.sellerUpdatedAt = sellerUpdatedAt;
    }

    public Object getDeliveryJobId() {
        return deliveryJobId;
    }

    public void setDeliveryJobId(Object deliveryJobId) {
        this.deliveryJobId = deliveryJobId;
    }

    public Object getJobId() {
        return jobId;
    }

    public void setJobId(Object jobId) {
        this.jobId = jobId;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getDeliveryPartnerUserId() {
        return deliveryPartnerUserId;
    }

    public void setDeliveryPartnerUserId(Object deliveryPartnerUserId) {
        this.deliveryPartnerUserId = deliveryPartnerUserId;
    }

    public LinkedList<OrderDetails> getListOfOrderDetails() {
        return listOfOrderDetails;
    }

    public void setListOfOrderDetails(LinkedList<OrderDetails> listOfOrderDetails) {
        this.listOfOrderDetails = listOfOrderDetails;
    }

    public LinkedList<CustomerAppStatus> getListOfCustomerAppStatuses() {
        return listOfCustomerAppStatuses;
    }

    public void setListOfCustomerAppStatuses(LinkedList<CustomerAppStatus> listOfCustomerAppStatuses) {
        this.listOfCustomerAppStatuses = listOfCustomerAppStatuses;
    }
}
