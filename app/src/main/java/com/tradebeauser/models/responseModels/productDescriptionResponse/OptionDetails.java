package com.tradebeauser.models.responseModels.productDescriptionResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.brandsDetailsResponse.BrandDetails;

import java.util.List;

public class OptionDetails {
    @SerializedName("label")
    @Expose
    private LabelDetails labelDetails;
    @SerializedName("values")
    @Expose
    private List<Values> listOfValuesDetails;

    public LabelDetails getLabelDetails() {
        return labelDetails;
    }

    public void setLabelDetails(LabelDetails labelDetails) {
        this.labelDetails = labelDetails;
    }

    public List<Values> getListOfValuesDetails() {
        return listOfValuesDetails;
    }

    public void setListOfValuesDetails(List<Values> listOfValuesDetails) {
        this.listOfValuesDetails = listOfValuesDetails;
    }
}
