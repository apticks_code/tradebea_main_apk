package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppliedPromoDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("menu_id")
    @Expose
    private Integer menuId;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("sub_cat_id")
    @Expose
    private Integer subCatId;
    @SerializedName("sub_sub_cat_id")
    @Expose
    private Integer subSubCatId;
    @SerializedName("promo_title")
    @Expose
    private String promoTitle;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("valid_to")
    @Expose
    private String validTo;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(Integer subCatId) {
        this.subCatId = subCatId;
    }

    public Integer getSubSubCatId() {
        return subSubCatId;
    }

    public void setSubSubCatId(Integer subSubCatId) {
        this.subSubCatId = subSubCatId;
    }

    public String getPromoTitle() {
        return promoTitle;
    }

    public void setPromoTitle(String promoTitle) {
        this.promoTitle = promoTitle;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
