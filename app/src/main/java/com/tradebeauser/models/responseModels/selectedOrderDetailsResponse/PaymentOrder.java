package com.tradebeauser.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentOrder {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("used_wallet_amount")
    @Expose
    private Integer usedWalletAmount;
    @SerializedName("status")
    @Expose
    private Integer status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(Integer usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
