package com.tradebeauser.models.responseModels.myOrdersResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class OrderDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("seller_variant_id")
    @Expose
    private Object sellerVariantId;
    @SerializedName("variant_id")
    @Expose
    private Integer variantId;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("product")
    @Expose
    private ProductDetails productDetails;
    @SerializedName("variant")
    @Expose
    private VariantDetails variantDetails;
    @SerializedName("images")
    @Expose
    private LinkedList<ProductImages> listOfProductImages;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Object getSellerVariantId() {
        return sellerVariantId;
    }

    public void setSellerVariantId(Object sellerVariantId) {
        this.sellerVariantId = sellerVariantId;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public VariantDetails getVariantDetails() {
        return variantDetails;
    }

    public void setVariantDetails(VariantDetails variantDetails) {
        this.variantDetails = variantDetails;
    }

    public LinkedList<ProductImages> getListOfProductImages() {
        return listOfProductImages;
    }

    public void setListOfProductImages(LinkedList<ProductImages> listOfProductImages) {
        this.listOfProductImages = listOfProductImages;
    }
}
