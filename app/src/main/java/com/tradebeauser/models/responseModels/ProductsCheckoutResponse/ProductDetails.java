package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class ProductDetails {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("variant_id")
    @Expose
    private String variantId;
    @SerializedName("qty")
    @Expose
    private int quantity;
    @SerializedName("product_details")
    @Expose
    private ProductInfo productInfo;
    @SerializedName("extra_charges")
    @Expose
    private ExtraCharges extraCharges;
    @SerializedName("applied_promo_details")
    @Expose
    private AppliedPromoDetails appliedPromoDetails;
    @SerializedName("seller_user_id")
    @Expose
    private String sellerUserId;
    @SerializedName("product_seller_variant_id")
    @Expose
    private String productSellerVariantId;
    @SerializedName("variants")
    @Expose
    private LinkedList<VariantDetails> listOfVariantDetails;
    @SerializedName("status")
    @Expose
    private ProductStatus productStatus;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public ProductInfo getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public ExtraCharges getExtraCharges() {
        return extraCharges;
    }

    public void setExtraCharges(ExtraCharges extraCharges) {
        this.extraCharges = extraCharges;
    }

    public String getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(String sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getProductSellerVariantId() {
        return productSellerVariantId;
    }

    public void setProductSellerVariantId(String productSellerVariantId) {
        this.productSellerVariantId = productSellerVariantId;
    }

    public LinkedList<VariantDetails> getListOfVariantDetails() {
        return listOfVariantDetails;
    }

    public void setListOfVariantDetails(LinkedList<VariantDetails> listOfVariantDetails) {
        this.listOfVariantDetails = listOfVariantDetails;
    }

    public ProductStatus getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(ProductStatus productStatus) {
        this.productStatus = productStatus;
    }

    public AppliedPromoDetails getAppliedPromoDetails() {
        return appliedPromoDetails;
    }

    public void setAppliedPromoDetails(AppliedPromoDetails appliedPromoDetails) {
        this.appliedPromoDetails = appliedPromoDetails;
    }
}
