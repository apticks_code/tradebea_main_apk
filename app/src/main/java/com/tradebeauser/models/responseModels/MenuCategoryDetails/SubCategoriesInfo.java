package com.tradebeauser.models.responseModels.MenuCategoryDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.SubSubCategoriesDetails;

import java.util.List;

public class SubCategoriesInfo {
    @SerializedName("cat_id")
    @Expose
    private String categoryId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ext")
    @Expose
    private String extension;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("sub_sub_categories")
    @Expose
    private List<SubSubCategoriesInfo> listOfSubSubCategoriesDetails;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<SubSubCategoriesInfo> getListOfSubSubCategoriesDetails() {
        return listOfSubSubCategoriesDetails;
    }

    public void setListOfSubSubCategoriesDetails(List<SubSubCategoriesInfo> listOfSubSubCategoriesDetails) {
        this.listOfSubSubCategoriesDetails = listOfSubSubCategoriesDetails;
    }
}
