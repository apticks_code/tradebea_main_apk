package com.tradebeauser.models.responseModels.CartProductsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.productDescriptionResponse.OptionGroupInfo;
import com.tradebeauser.models.responseModels.productDescriptionResponse.OptionInfo;
import com.tradebeauser.models.responseModels.productDescriptionResponse.OptionItemInfo;

public class VariantValues {
    @SerializedName("option_item_id")
    @Expose
    private String optionItemId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("option_id")
    @Expose
    private String optionId;
    @SerializedName("option_group_id")
    @Expose
    private String optionGroupId;
    private String mrp;
    private String discount;
    @SerializedName("option")
    @Expose
    private OptionInfo option;
    @SerializedName("option_group")
    @Expose
    private OptionGroupInfo optionGroup;
    @SerializedName("option_item")
    @Expose
    private OptionItemInfo optionItem;

    public String getOptionItemId() {
        return optionItemId;
    }

    public void setOptionItemId(String optionItemId) {
        this.optionItemId = optionItemId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionGroupId() {
        return optionGroupId;
    }

    public void setOptionGroupId(String optionGroupId) {
        this.optionGroupId = optionGroupId;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public OptionInfo getOption() {
        return option;
    }

    public void setOption(OptionInfo option) {
        this.option = option;
    }

    public OptionGroupInfo getOptionGroup() {
        return optionGroup;
    }

    public void setOptionGroup(OptionGroupInfo optionGroup) {
        this.optionGroup = optionGroup;
    }

    public OptionItemInfo getOptionItem() {
        return optionItem;
    }

    public void setOptionItem(OptionItemInfo optionItem) {
        this.optionItem = optionItem;
    }
}
