package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExtraCharges {
    @SerializedName("delivery_fee")
    @Expose
    private String deliveryFee;
    @SerializedName("tax")
    @Expose
    private String tax;

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }
}
