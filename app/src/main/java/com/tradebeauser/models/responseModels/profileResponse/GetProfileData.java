package com.tradebeauser.models.responseModels.profileResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class GetProfileData {

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private Object lastName;
    @SerializedName("phone")
    @Expose
    private Long phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("wallet")
    @Expose
    private Integer wallet;
    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_details")
    @Expose
    private UserDetails userDetails;
    @SerializedName("groups")
    @Expose
    private LinkedList<GroupDetails> listOfGroupsDetails;

    @SerializedName("active_user_address")
    @Expose
    private LinkedList<AddressDetails> listOfActiveAddresses;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getWallet() {
        return wallet;
    }

    public void setWallet(Integer wallet) {
        this.wallet = wallet;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public LinkedList<GroupDetails> getListOfGroupsDetails() {
        return listOfGroupsDetails;
    }

    public LinkedList<AddressDetails> getListOfActiveAddresses() {
        return listOfActiveAddresses;
    }

    public void setListOfActiveAddresses(LinkedList<AddressDetails> listOfActiveAddresses) {
        this.listOfActiveAddresses = listOfActiveAddresses;
    }

    public void setListOfGroupsDetails(LinkedList<GroupDetails> listOfGroupsDetails) {
        this.listOfGroupsDetails = listOfGroupsDetails;


    }
}
