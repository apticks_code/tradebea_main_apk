package com.tradebeauser.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnRequest {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("order_pickup_otp")
    @Expose
    private Integer orderPickupOtp;
    @SerializedName("order_return_otp")
    @Expose
    private Integer orderReturnOtp;
    @SerializedName("return_reason_id")
    @Expose
    private ReturnReasonId returnReasonId;
    @SerializedName("personal_reason")
    @Expose
    private String personalReason;
    @SerializedName("customer_user_id")
    @Expose
    private Integer customerUserId;
    @SerializedName("seller_user_id")
    @Expose
    private Integer sellerUserId;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("order_track_id")
    @Expose
    private String orderTrackId;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("customer_unique_id")
    @Expose
    private String customerUniqueId;
    @SerializedName("seller_id")
    @Expose
    private Integer sellerId;
    @SerializedName("seller_unique_id")
    @Expose
    private String sellerUniqueId;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("seller_accepted_at")
    @Expose
    private String sellerAcceptedAt;
    @SerializedName("adminAcceptedAt")
    @Expose
    private String adminAcceptedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderPickupOtp() {
        return orderPickupOtp;
    }

    public void setOrderPickupOtp(Integer orderPickupOtp) {
        this.orderPickupOtp = orderPickupOtp;
    }

    public Integer getOrderReturnOtp() {
        return orderReturnOtp;
    }

    public void setOrderReturnOtp(Integer orderReturnOtp) {
        this.orderReturnOtp = orderReturnOtp;
    }

    public ReturnReasonId getReturnReasonId() {
        return returnReasonId;
    }

    public void setReturnReasonId(ReturnReasonId returnReasonId) {
        this.returnReasonId = returnReasonId;
    }

    public String getPersonalReason() {
        return personalReason;
    }

    public void setPersonalReason(String personalReason) {
        this.personalReason = personalReason;
    }

    public Integer getCustomerUserId() {
        return customerUserId;
    }

    public void setCustomerUserId(Integer customerUserId) {
        this.customerUserId = customerUserId;
    }

    public Integer getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Integer sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getOrderTrackId() {
        return orderTrackId;
    }

    public void setOrderTrackId(String orderTrackId) {
        this.orderTrackId = orderTrackId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerUniqueId() {
        return customerUniqueId;
    }

    public void setCustomerUniqueId(String customerUniqueId) {
        this.customerUniqueId = customerUniqueId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUniqueId() {
        return sellerUniqueId;
    }

    public void setSellerUniqueId(String sellerUniqueId) {
        this.sellerUniqueId = sellerUniqueId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSellerAcceptedAt() {
        return sellerAcceptedAt;
    }

    public void setSellerAcceptedAt(String sellerAcceptedAt) {
        this.sellerAcceptedAt = sellerAcceptedAt;
    }

    public String getAdminAcceptedAt() {
        return adminAcceptedAt;
    }

    public void setAdminAcceptedAt(String adminAcceptedAt) {
        this.adminAcceptedAt = adminAcceptedAt;
    }
}
