package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class VariantDetails {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("mrp")
    @Expose
    private String mrp;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("variant_code")
    @Expose
    private String variantCode;
    @SerializedName("variant_values")
    @Expose
    private LinkedList<VariantValues> listOfVariantValues;
    @SerializedName("available_qty")
    @Expose
    private String availableQuantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVariantCode() {
        return variantCode;
    }

    public void setVariantCode(String variantCode) {
        this.variantCode = variantCode;
    }

    public LinkedList<VariantValues> getListOfVariantValues() {
        return listOfVariantValues;
    }

    public void setListOfVariantValues(LinkedList<VariantValues> listOfVariantValues) {
        this.listOfVariantValues = listOfVariantValues;
    }

    public String getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(String availableQuantity) {
        this.availableQuantity = availableQuantity;
    }
}
