package com.tradebeauser.models.responseModels.SavedAddressDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetails;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;

import java.io.Serializable;
import java.util.LinkedList;

public class SavedAddressDetailsResponse implements Serializable {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LinkedList<AddressDetails> listOfSavedAddressDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<AddressDetails> getListOfSavedAddressDetails() {
        return listOfSavedAddressDetails;
    }

    public void setListOfSavedAddressDetails(LinkedList<AddressDetails> listOfSavedAddressDetails) {
        this.listOfSavedAddressDetails = listOfSavedAddressDetails;
    }
}
