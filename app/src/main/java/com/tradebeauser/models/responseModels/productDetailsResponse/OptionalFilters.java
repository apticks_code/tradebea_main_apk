package com.tradebeauser.models.responseModels.productDetailsResponse;

import java.util.List;

public class OptionalFilters {
    private List<List<OptionalFilterDetails>> listOfOptionalFilters = null;

    public List<List<OptionalFilterDetails>> getListOfOptionalFilters() {
        return listOfOptionalFilters;
    }

    public void setListOfOptionalFilters(List<List<OptionalFilterDetails>> listOfOptionalFilters) {
        this.listOfOptionalFilters = listOfOptionalFilters;
    }
}
