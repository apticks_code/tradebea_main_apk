package com.tradebeauser.models.responseModels.myOrdersResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class VariantDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("variant_code")
    @Expose
    private Integer variantCode;
    @SerializedName("mrp")
    @Expose
    private Integer mrp;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("values")
    @Expose
    private LinkedList<VariantValue> listOfVariantValues;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariantCode() {
        return variantCode;
    }

    public void setVariantCode(Integer variantCode) {
        this.variantCode = variantCode;
    }

    public Integer getMrp() {
        return mrp;
    }

    public void setMrp(Integer mrp) {
        this.mrp = mrp;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public LinkedList<VariantValue> getListOfVariantValues() {
        return listOfVariantValues;
    }

    public void setListOfVariantValues(LinkedList<VariantValue> listOfVariantValues) {
        this.listOfVariantValues = listOfVariantValues;
    }
}
