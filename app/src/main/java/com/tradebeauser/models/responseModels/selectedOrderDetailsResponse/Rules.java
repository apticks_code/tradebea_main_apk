package com.tradebeauser.models.responseModels.selectedOrderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rules {

    @SerializedName("default_preparation_time_in_hrs")
    @Expose
    private Integer defaultPreparationTimeInHrs;
    @SerializedName("default_return_poilcy")
    @Expose
    private DefaultReturnPolicy defaultReturnPolicy;


    public Integer getDefaultPreparationTimeInHrs() {
        return defaultPreparationTimeInHrs;
    }

    public void setDefaultPreparationTimeInHrs(Integer defaultPreparationTimeInHrs) {
        this.defaultPreparationTimeInHrs = defaultPreparationTimeInHrs;
    }

    public DefaultReturnPolicy getDefaultReturnPolicy() {
        return defaultReturnPolicy;
    }

    public void setDefaultReturnPolicy(DefaultReturnPolicy defaultReturnPolicy) {
        this.defaultReturnPolicy = defaultReturnPolicy;
    }
}
