package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tradebeauser.models.responseModels.CartProductsResponse.Product;

import java.util.LinkedList;
import java.util.List;

public class Data {
    @SerializedName("checkout_status")
    @Expose
    private Boolean checkoutStatus;
    @SerializedName("products")
    @Expose
    private List<ProductDetails> listOfProductDetails;
    @SerializedName("promos")
    @Expose
    private LinkedList<PromoCodeDetails> listOfPromoCodeDetails;
    @SerializedName("total_billing_summery")
    @Expose
    private TotalBillingSummery totalBillingSummery;
    @SerializedName("is_coupon_applied")
    @Expose
    private Boolean isCouponApplied;


    public Boolean getCheckoutStatus() {
        return checkoutStatus;
    }

    public void setCheckoutStatus(Boolean checkoutStatus) {
        this.checkoutStatus = checkoutStatus;
    }

    public List<ProductDetails> getListOfProductDetails() {
        return listOfProductDetails;
    }

    public void setListOfProductDetails(List<ProductDetails> listOfProductDetails) {
        this.listOfProductDetails = listOfProductDetails;
    }

    public LinkedList<PromoCodeDetails> getListOfPromoCodeDetails() {
        return listOfPromoCodeDetails;
    }

    public void setListOfPromoCodeDetails(LinkedList<PromoCodeDetails> listOfPromoCodeDetails) {
        this.listOfPromoCodeDetails = listOfPromoCodeDetails;
    }

    public Boolean getCouponApplied() {
        return isCouponApplied;
    }

    public void setCouponApplied(Boolean couponApplied) {
        isCouponApplied = couponApplied;
    }

    public TotalBillingSummery getTotalBillingSummery() {
        return totalBillingSummery;
    }

    public void setTotalBillingSummery(TotalBillingSummery totalBillingSummery) {
        this.totalBillingSummery = totalBillingSummery;
    }
}
