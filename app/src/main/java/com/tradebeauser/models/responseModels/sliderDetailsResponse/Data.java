package com.tradebeauser.models.responseModels.sliderDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class Data {

    @SerializedName("sliders")
    @Expose
    private LinkedList<SliderDetails> sliders = null;

    public LinkedList<SliderDetails> getSliders() {
        return sliders;
    }

    public void setSliders(LinkedList<SliderDetails> sliders) {
        this.sliders = sliders;
    }
}
