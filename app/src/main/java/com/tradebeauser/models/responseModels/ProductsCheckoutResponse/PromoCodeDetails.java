package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromoCodeDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("promo_title")
    @Expose
    private String promoTitle;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("valid_to")
    @Expose
    private String validTo;
    @SerializedName("discount_type")
    @Expose
    private Integer discountType;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("status")
    @Expose
    private Integer status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPromoTitle() {
        return promoTitle;
    }

    public void setPromoTitle(String promoTitle) {
        this.promoTitle = promoTitle;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public Integer getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Integer discountType) {
        this.discountType = discountType;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
