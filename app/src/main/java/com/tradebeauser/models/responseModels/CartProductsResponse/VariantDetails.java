package com.tradebeauser.models.responseModels.CartProductsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VariantDetails {
    private String id;
    private String mrp;
    private String discount;
    @SerializedName("values")
    @Expose
    private List<VariantValues> listOfVariantValues;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public List<VariantValues> getListOfVariantValues() {
        return listOfVariantValues;
    }

    public void setListOfVariantValues(List<VariantValues> listOfVariantValues) {
        this.listOfVariantValues = listOfVariantValues;
    }
}
