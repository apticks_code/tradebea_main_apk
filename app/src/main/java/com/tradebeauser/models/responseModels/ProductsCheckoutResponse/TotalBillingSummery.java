package com.tradebeauser.models.responseModels.ProductsCheckoutResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalBillingSummery {

    @SerializedName("sub_total")
    @Expose
    private Double subTotal;
    @SerializedName("discount_in_amount")
    @Expose
    private Double discountInAmount;
    @SerializedName("promo_discount_amount")
    @Expose
    private Double promoDiscountAmount;
    @SerializedName("tax_in_amount")
    @Expose
    private Double taxInAmount;
    @SerializedName("delivery_fee")
    @Expose
    private Double deliveryFee;
    @SerializedName("grand_total")
    @Expose
    private Double grandTotal;


    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiscountInAmount() {
        return discountInAmount;
    }

    public void setDiscountInAmount(Double discountInAmount) {
        this.discountInAmount = discountInAmount;
    }

    public Double getPromoDiscountAmount() {
        return promoDiscountAmount;
    }

    public void setPromoDiscountAmount(Double promoDiscountAmount) {
        this.promoDiscountAmount = promoDiscountAmount;
    }

    public Double getTaxInAmount() {
        return taxInAmount;
    }

    public void setTaxInAmount(Double taxInAmount) {
        this.taxInAmount = taxInAmount;
    }

    public Double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }
}
