package com.tradebeauser.models.responseModels.productDetailsResponse;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class Filters implements Serializable {
    @SerializedName("filter_labels")
    @Expose
    private List<FilterLabelDetails> listOfFilterLabels = null;
    @SerializedName("basic_filters")
    @Expose
    private JSONObject basicFilters;

    @SerializedName("option_filters")
    @Expose
    private JSONObject optionalFilters;

    public List<FilterLabelDetails> getListOfFilterLabels() {
        return listOfFilterLabels;
    }

    public void setListOfFilterLabels(List<FilterLabelDetails> listOfFilterLabels) {
        this.listOfFilterLabels = listOfFilterLabels;
    }

    public JSONObject getOptionalFilters() {
        return optionalFilters;
    }

    public void setOptionalFilters(JSONObject optionalFilters) {
        this.optionalFilters = optionalFilters;
    }

    public JSONObject getBasicFilters() {
        return basicFilters;
    }

    public void setBasicFilters(JSONObject basicFilters) {
        this.basicFilters = basicFilters;
    }
}
