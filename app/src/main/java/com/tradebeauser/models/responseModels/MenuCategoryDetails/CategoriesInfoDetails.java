package com.tradebeauser.models.responseModels.MenuCategoryDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoriesInfoDetails {
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ext")
    @Expose
    private String extension;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("sub_categories")
    @Expose
    private List<SubCategoriesInfo> listOfSubCategoriesDetails;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<SubCategoriesInfo> getListOfSubCategoriesDetails() {
        return listOfSubCategoriesDetails;
    }

    public void setListOfSubCategoriesDetails(List<SubCategoriesInfo> listOfSubCategoriesDetails) {
        this.listOfSubCategoriesDetails = listOfSubCategoriesDetails;
    }
}
