package com.tradebeauser.models.requestModels.orderProductsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class OrderProductsRequest {

    @SerializedName("delivery_address_id")
    @Expose
    private Integer deliveryAddressId;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("used_wallet_amount")
    @Expose
    private Double usedWalletAmount;
    @SerializedName("grand_total")
    @Expose
    private Double grandTotal;
    @SerializedName("promo_id")
    @Expose
    private String promoId;
    @SerializedName("products")
    @Expose
    private LinkedList<ProductDetailsRequest> listOfProductDetails;


    public Integer getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(Integer deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Double getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(Double usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public LinkedList<ProductDetailsRequest> getListOfProductDetails() {
        return listOfProductDetails;
    }

    public void setListOfProductDetails(LinkedList<ProductDetailsRequest> listOfProductDetails) {
        this.listOfProductDetails = listOfProductDetails;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }
}
