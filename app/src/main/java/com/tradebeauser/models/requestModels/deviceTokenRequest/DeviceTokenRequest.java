package com.tradebeauser.models.requestModels.deviceTokenRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceTokenRequest {
    @SerializedName("token")
    @Expose
    public String token;
}
