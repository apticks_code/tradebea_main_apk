package com.tradebeauser.models.requestModels.countDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountDetailsRequest {

    @SerializedName("shipping_address_id")
    @Expose
    public Integer shippingAddressId;
    @SerializedName("current_location_latitude")
    @Expose
    public Double currentLocationLatitude;
    @SerializedName("current_location_longitude")
    @Expose
    public Double currentLocationLongitude;
}
