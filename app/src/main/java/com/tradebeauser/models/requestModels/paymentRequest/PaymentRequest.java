package com.tradebeauser.models.requestModels.paymentRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentRequest {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("fee")
    @Expose
    private Double fee;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("invoice_id")
    @Expose
    private String invoiceId;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("card_id")
    @Expose
    private String cardId;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("vpa")
    @Expose
    private String vpa;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("error_code")
    @Expose
    private String errorCode;
    @SerializedName("error_reason")
    @Expose
    private String errorReason;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("captured")
    @Expose
    private Boolean captured;
    @SerializedName("used_wallet_amount")
    @Expose
    private Double usedWalletAmount;
    @SerializedName("payment_method_id")
    @Expose
    private Integer paymentMethodId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getVpa() {
        return vpa;
    }

    public void setVpa(String vpa) {
        this.vpa = vpa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(String errorReason) {
        this.errorReason = errorReason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getCaptured() {
        return captured;
    }

    public void setCaptured(Boolean captured) {
        this.captured = captured;
    }

    public Double getUsedWalletAmount() {
        return usedWalletAmount;
    }

    public void setUsedWalletAmount(Double usedWalletAmount) {
        this.usedWalletAmount = usedWalletAmount;
    }

    public Integer getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }
}
