package com.tradebeauser.models.requestModels.returnRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnRequest {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("return_reason_id")
    @Expose
    private int returnReasonId;
    @SerializedName("personal_reason")
    @Expose
    private String personalReason;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getReturnReasonId() {
        return returnReasonId;
    }

    public void setReturnReasonId(int returnReasonId) {
        this.returnReasonId = returnReasonId;
    }

    public String getPersonalReason() {
        return personalReason;
    }

    public void setPersonalReason(String personalReason) {
        this.personalReason = personalReason;
    }
}
