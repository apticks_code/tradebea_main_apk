package com.tradebeauser.models.requestModels.profileUpdateRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileUpdateRequest {

    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("shop_name")
    @Expose
    public String shopName;
    @SerializedName("ownership_type")
    @Expose
    public String ownershipType;
    public String designation;
    public String mobile;
    public String email;
    public String gst;
    public String pan;
    @SerializedName("signature_image")
    @Expose
    public String signatureImage;
    @SerializedName("profile_image")
    @Expose
    public String profileImage;
    @SerializedName("electricity_bill_image")
    @Expose
    public String electricityBillImage;
    @SerializedName("pan_card_image")
    @Expose
    public String panCardImage;
    @SerializedName("shop_image")
    @Expose
    public String shopImage;
    public String latitude;
    public String longitude;
    @SerializedName("geo_location_address")
    @Expose
    public String geoLocationAddress;
    public String group;
}
