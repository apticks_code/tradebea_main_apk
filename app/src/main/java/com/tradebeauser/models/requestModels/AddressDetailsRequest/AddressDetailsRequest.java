package com.tradebeauser.models.requestModels.AddressDetailsRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressDetailsRequest {
    @SerializedName("geo_location_address")
    @Expose
    public String geoLocation;
    public String id;
    public String tag;
    public String landmark;
    public String pincode;
    public String address;
    public String latitude;
    public String longitude;
}
