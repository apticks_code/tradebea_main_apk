package com.tradebeauser.utils;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.google.gson.JsonObject;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.DefaultReturnReason;

import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class UserData {

    private static UserData userData = null;

    private Context context;
    private Fragment fragment;
    private Fragment walletFragment;

    private Data checkoutData;
    private AddressDetails addressDetails;
    private AddressDetails selectedAddressDetails;

    private JSONObject filters;
    private JsonObject checkoutRequestObject;
    private LinkedList<DefaultReturnReason> listOfDefaultReturnReasons;
    private LinkedHashMap<String, List<String>> mapOfFilterLabelKeyAndSelectedOptions = new LinkedHashMap<>();
    private LinkedHashMap<String, List<String>> mapOfFinalFilterLabelKeyAndSelectedOptions = new LinkedHashMap<>();

    private int quantity = 1;
    private int rowIndex = -1;


    private UserData() {
    }

    public static UserData getInstance() {
        if (userData == null) {
            userData = new UserData();
        }
        return userData;
    }

    public void setActiveLocation(AddressDetails addressDetails) {
        this.addressDetails = addressDetails;
    }

    public void setFilterDetails(JSONObject filters) {
        this.filters = filters;
    }

    public JSONObject getFiltersDetails() {
        return filters;
    }

    public LinkedHashMap<String, List<String>> getMapOfFilterLabelKeyAndSelectedOptions() {
        return mapOfFilterLabelKeyAndSelectedOptions;
    }

    public void setMapOfFilterLabelKeyAndSelectedOptions(LinkedHashMap<String, List<String>> mapOfFilterLabelKeyAndSelectedOptions) {
        this.mapOfFilterLabelKeyAndSelectedOptions = mapOfFilterLabelKeyAndSelectedOptions;
    }

    public LinkedHashMap<String, List<String>> getMapOfFinalFilterLabelKeyAndSelectedOptions() {
        return mapOfFinalFilterLabelKeyAndSelectedOptions;
    }

    public void setMapOfFinalFilterLabelKeyAndSelectedOptions(LinkedHashMap<String, List<String>> mapOfFilterLabelKeyAndSelectedOptions) {
        this.mapOfFinalFilterLabelKeyAndSelectedOptions = mapOfFilterLabelKeyAndSelectedOptions;
    }

    public AddressDetails getActiveLocation() {
        return addressDetails;
    }

    public void setSelectedAddress(AddressDetails selectedAddressDetails) {
        this.selectedAddressDetails = selectedAddressDetails;
    }

    public AddressDetails getSelectedAddress() {
        return selectedAddressDetails;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setSelectedIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getSelectedIndex() {
        return rowIndex;
    }

    public void setCheckoutData(Data checkoutData) {
        this.checkoutData = checkoutData;
    }

    public Data getCheckoutData() {
        return checkoutData;
    }

    public JsonObject getCheckoutRequestObject() {
        return checkoutRequestObject;
    }

    public void setCheckoutRequestObject(JsonObject checkoutRequestObject) {
        this.checkoutRequestObject = checkoutRequestObject;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public LinkedList<DefaultReturnReason> getListOfDefaultReturnReasons() {
        return listOfDefaultReturnReasons;
    }

    public void setListOfDefaultReturnReasons(LinkedList<DefaultReturnReason> listOfDefaultReturnReasons) {
        this.listOfDefaultReturnReasons = listOfDefaultReturnReasons;
    }

    public Fragment getWalletFragment() {
        return walletFragment;
    }

    public void setWalletFragment(Fragment walletFragment) {
        this.walletFragment = walletFragment;
    }
}
