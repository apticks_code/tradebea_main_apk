package com.tradebeauser.utils;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.androidnetworking.error.ANError;
import com.tradebeauser.R;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethods {

    /**
     * to hide the keyboard
     *
     * @param context context of the activity or fragment
     * @param view    any view in that particular activity or fragment
     */
    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * to convert jsonobject to inputstream
     *
     * @param response json object of service
     * @return returns the input stream through reader
     */
    public static Reader getInputStream(String response) {
        InputStream result = new ByteArrayInputStream(response.getBytes(Charset.forName("UTF-8")));
        return new InputStreamReader(result);
    }

    public static boolean isValidEmailFormat(String emailTxt) {
        try {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(emailTxt);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isValidPhoneNumber(String phoneTxt) {
        try {
            Pattern pattern = Pattern.compile("^[0-9]\\d{9}$");
            Matcher matcher = pattern.matcher(phoneTxt);
            return matcher.matches();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void handleErrorResponse(Context context, ANError error) {
        DialogUtils.getDialogUtilsInstance().displayToast(context, context.getString(R.string.status_error));
    }

    public static double getSalePrice(String discountPercentage, String MRP) {
        double discount = Double.parseDouble(discountPercentage);
        double marketPrice = Double.parseDouble(MRP);
        return marketPrice - (marketPrice * discount / 100);
    }

    public static double convertDiscountPercentageIntoAmount(String discountPercentage, String mrp) {
        double discount = Double.parseDouble(discountPercentage);
        double marketPrice = Double.parseDouble(mrp);
        return (marketPrice * discount / 100);
    }

    public static double convertTaxPercentageIntoAmount(String taxPercentage, String mrp) {
        double tax = Double.parseDouble(taxPercentage);
        double marketPrice = Double.parseDouble(mrp);
        return (marketPrice * tax / 100);
    }

    public static String getTimeAgo(String selectedDate) {
        long seconds = 0;
        long minutes = 0;
        long hours = 0;
        SimpleDateFormat createdDateSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        try {
            Date date = createdDateSimpleDateFormat.parse(selectedDate);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            Date datePreparationTime = calendar.getTime();

            long diff = datePreparationTime.getTime() - date.getTime();
            seconds = diff / 1000;
            minutes = seconds / 60;
            hours = seconds / 3600;
            Log.d("hours***", hours + "");
            Log.d("minutes***", minutes + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(hours);
    }

    private static String convertSecsIntoHoursMinutes(long seconds) {
        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        String hoursString = "";
        hoursString = String.valueOf(hours);
        return hoursString;
    }

    private static String getRelationTime(long now, long time) {
        long delta = now - time;
        long resolution = DateUtils.HOUR_IN_MILLIS;
        return DateUtils.getRelativeTimeSpanString(time, now, resolution).toString();
    }
}
