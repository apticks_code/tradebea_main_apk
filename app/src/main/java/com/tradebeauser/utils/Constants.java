package com.tradebeauser.utils;

public class Constants {

    public static final String APP_ID = "TVE9PQ==";
    public static final String GROUP_ID = "5";
    public static final int GROUP_ID_INT = 5;

    public static final int SPLASH_TIME_OUT = 3000;
    public static final int TIME_OUT_THIRTY_SECONDS = 30000;

    public static final String BASE_URL = "https://apticks.com/tradebea/";
    //public static final String BASE_URL = "http://tradebea.com/tradebea/";
    public static final String LOGIN_URL = BASE_URL + "auth/api/auth/login";
    public static final String SOCIAL_LOGIN_URL = BASE_URL + "auth/api/auth/social_login/google";
    public static final String GENERATE_OTP_URL = BASE_URL + "auth/api/auth/otp_gen";
    public static final String VERIFY_OTP_URL = BASE_URL + "auth/api/auth/verify_otp";
    public static final String SIGN_UP_URL = BASE_URL + "auth/api/auth/create_user/user";
    public static final String FORGOT_PASSWORD_URL = BASE_URL + "auth/api/auth/forgot_password";
    public static final String GET_PROFILE_DETAILS_URL = BASE_URL + "auth/api/users/profile/r";
    public static final String UPDATE_PROFILE_DETAILS_URL = BASE_URL + "auth/api/users/profile/u/user";
    public static final String MENU_URL = BASE_URL + "catalogue/api/catalogue/menus";
    public static final String SLIDER_URL = BASE_URL + "admin/api/admin/sliders";
    public static final String BRANDS_URL = BASE_URL + "catalogue/api/catalogue/brands";
    public static final String PRODUCTS_LIST_URL = BASE_URL + "catalogue/api/catalogue/products/";
    public static final String SAVED_ADDRESSES_LIST_URL = BASE_URL + "ecom/api/general/user_address/r";
    public static final String ADD_ADDRESSES_LIST_URL = BASE_URL + "ecom/api/general/user_address/c";
    public static final String UPDATE_ADDRESSES_LIST_URL = BASE_URL + "ecom/api/general/user_address/u";
    public static final String DELETE_ADDRESSES_LIST_URL = BASE_URL + "ecom/api/general/user_address/d";
    public static final String SET_ACTIVE_ADDRESS_URL = BASE_URL + "ecom/api/general/user_address/active";
    public static final String ADD_PRODUCT_TO_CART_URL = BASE_URL + "ecom/api/ecom/cart/add";
    public static final String DELETE_PRODUCT_FROM_CART_URL = BASE_URL + "ecom/api/ecom/cart/d";
    public static final String UPDATE_CART_PRODUCT_STATUS_URL = BASE_URL + "ecom/api/ecom/cart/u";
    public static final String GET_CART_PRODUCTS_URL = BASE_URL + "ecom/api/ecom/cart/r";
    public static final String SHOP_BY_CATEGORY_URL = BASE_URL + "catalogue/api/catalogue/shop_by_category/";
    public static final String MENU_CATEGORY_URL = BASE_URL + "catalogue/api/catalogue/shop_by_category/";
    public static final String ORDER_HISTORY_URL = BASE_URL + "ecom/api/ecom/orders/order_history";
    public static final String PRODUCTS_CHECKOUT_URL = BASE_URL + "ecom/api/ecom/checkout";
    public static final String PAYMENT_URL = BASE_URL + "ecom/api/ecom/payment_response/c";
    public static final String ORDER_URL = BASE_URL + "ecom/api/ecom/orders/c";
    public static final String PAYMENT_GATEWAY_CREDENTIALS = BASE_URL + "payment/api/payment/payment_gateway_credentials";
    public static final String COUNT_URL = BASE_URL + "ecom/api/general/home";
    public static final String DEVICE_TOKEN_URL = BASE_URL + "admin/api/fcm_notify/grant_fcm_permission";
    public static final String SELECTED_ORDER_DETAILS_URL = BASE_URL + "ecom/api/ecom/orders/order_details/";
    public static final String RETURN_ORDER_URL = BASE_URL + "ecom/api/returns/return_requests";
    public static final String CANCEL_ORDER_URL = BASE_URL + "ecom/api/ecom/orders/customer_cancelation";
    public static final String WALLET_BALANCE_URL = BASE_URL + "payment/api/payment/wallet_account";

    public static final int REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int REQUEST_CODE_FOR_CAMERA_PERMISSION = 2;
    public static final int REQUEST_CODE_FOR_LOCATION_PERMISSION = 3;
    public static final int REQUEST_CODE_FOR_PHONE_PERMISSION = 4;
    public static final int PICK_GALLERY = 5;
    public static final int REQUEST_CODE_CAPTURE_IMAGE = 6;

    // Api calls
    public static final int SERVICE_CALL_FOR_LOGIN = 1;
    public static final int SERVICE_CALL_FOR_GENERATE_OTP = 2;
    public static final int SERVICE_CALL_FOR_VERIFY_OTP = 3;
    public static final int SERVICE_CALL_FOR_SIGN_UP = 4;
    public static final int SERVICE_CALL_FOR_FORGOT_PASSWORD = 5;
    public static final int SERVICE_CALL_TO_GET_PROFILE_DETAILS = 6;
    public static final int SERVICE_CALL_FOR_SOCIAL_LOGIN = 7;
    public static final int SERVICE_CALL_FOR_UPDATE_PROFILE = 8;
    public static final int SERVICE_CALL_TO_GET_MENU = 9;
    public static final int SERVICE_CALL_TO_GET_SLIDERS = 10;
    public static final int SERVICE_CALL_TO_GET_BRANDS = 11;

    public static final int SERVICE_CALL_FOR_PRODUCT_SEARCH = 12;

    public static final int SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS = 13;
    public static final int SERVICE_CALL_TO_GET_SAVED_ADDRESSES = 14;
    public static final int SERVICE_CALL_FOR_ADD_ADDRESS = 15;
    public static final int SERVICE_CALL_FOR_UPDATE_ADDRESS = 16;
    public static final int SERVICE_CALL_FOR_DELETE_ADDRESS = 17;
    public static final int SERVICE_CALL_TO_SET_ACTIVE_LOCATION = 18;
    public static final int SERVICE_CALL_FOR_PRODUCT_DETAILS = 19;
    public static final int SERVICE_CALL_FOR_ADD_PRODUCT_TO_CART = 20;
    public static final int SERVICE_CALL_FOR_DELETE_PRODUCT_FROM_CART = 21;
    public static final int SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS = 22;
    public static final int SERVICE_CALL_FOR_GET_CART_PRODUCTS = 23;
    public static final int SERVICE_CALL_FOR_RELATED_PRODUCTS = 24;
    public static final int SERVICE_CALL_FOR_SHOP_BY_CATEGORIES = 25;
    public static final int SERVICE_CALL_FOR_MENU_CATEGORIES = 26;
    public static final int SERVICE_CALL_FOR_TOP_DEAL_PRODUCTS = 27;
    public static final int SERVICE_CALL_FOR_ORDER_HISTORY = 28;
    public static final int SERVICE_CALL_FOR_CHECKOUT_PRODUCTS = 28;
    public static final int SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS = 29;
    public static final int SERVICE_CALL_FOR_PAYMENT_DETAILS = 30;
    public static final int SERVICE_CALL_FOR_ORDER_PRODUCTS_DETAILS = 31;
    public static final int SERVICE_CALL_FOR_COUNT_DETAILS = 32;
    public static final int SERVICE_CALL_FOR_DEVICE_TOKEN = 33;
    public static final int SERVICE_CALL_FOR_SELECTED_ORDER_DETAILS = 34;
    public static final int SERVICE_CALL_FOR_RETURN_ORDER = 35;
    public static final int SERVICE_CALL_FOR_CANCEL_ORDER = 36;
    public static final int SERVICE_CALL_TO_GET_WALLET_BALANCE = 37;
    public static final int SERVICE_CALL_TO_GET_TRANSACTION_DETAILS = 38;
    public static final String NOTIFICATION_CHANNEL_ID = "tradebeaUser_channel";
}
