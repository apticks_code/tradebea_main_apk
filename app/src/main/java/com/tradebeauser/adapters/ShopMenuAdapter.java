package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.graphics.ColorUtils;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.CategoriesDetails;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.Data;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ShopMenuAdapter extends RecyclerView.Adapter<ShopMenuAdapter.ViewHolder> {
    private Context context;

    private List<Data> listOfMenuDetails;
    private LinkedList<Boolean> listOfClickedStatus;

    public ShopMenuAdapter(Context context, List<Data> listOfMenuDetails) {
        this.context = context;
        this.listOfMenuDetails = listOfMenuDetails;
        listOfClickedStatus = new LinkedList<>();
        for (int index = 0; index < listOfMenuDetails.size(); index++) {
            listOfClickedStatus.add(false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_shop_menu_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = listOfMenuDetails.get(position).getName();
        String image = listOfMenuDetails.get(position).getImage();
        holder.tvName.setText(name);
        List<CategoriesDetails> listOfCategoryDetails;
        listOfCategoryDetails = listOfMenuDetails.get(position).getListOfCategoriesDetails();
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        color = ColorUtils.blendARGB(color, Color.WHITE, 0.88f);
        holder.llName.setBackgroundColor(color);

        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);
        boolean clickStatus = listOfClickedStatus.get(position);

        if (listOfCategoryDetails != null && listOfCategoryDetails.size() != 0) {
            if (clickStatus) {
                holder.llMenu.setBackground(context.getDrawable(R.drawable.triangle_shape));
                holder.rvSubList.setVisibility(View.VISIBLE);
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
            } else {
                holder.rvSubList.setVisibility(View.GONE);
                holder.llMenu.setBackground(null);
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
            }
        } else {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }


    @Override
    public int getItemCount() {
        return listOfMenuDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName;
        private LinearLayout llName, llMenu;
        private RecyclerView rvSubList;
        private RoundedImageView ivImage;
        private ImageView ivArrow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llName = itemView.findViewById(R.id.llName);
            llMenu = itemView.findViewById(R.id.llMenu);
            ivImage = itemView.findViewById(R.id.ivImage);
            ivArrow = itemView.findViewById(R.id.ivArrow);
            rvSubList = itemView.findViewById(R.id.rvSubList);
            llName.setOnClickListener(this);
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        @Override
        public void onClick(View view) {
            boolean clickStatus = listOfClickedStatus.get(getAbsoluteAdapterPosition());
            List<CategoriesDetails> listOfCategoryDetails;
            listOfCategoryDetails = listOfMenuDetails.get(getAbsoluteAdapterPosition()).getListOfCategoriesDetails();
            if (listOfCategoryDetails != null && listOfCategoryDetails.size() != 0) {
                if (clickStatus) {
                    listOfClickedStatus.add(getAbsoluteAdapterPosition(), false);
                    llMenu.setBackground(null);
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
                    rvSubList.setVisibility(View.GONE);
                } else {
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
                    llMenu.setBackground(context.getDrawable(R.drawable.triangle_shape));
                    listOfClickedStatus.add(getAbsoluteAdapterPosition(), true);
                    initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
                }
            }else {
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
            }
        }
    }

    private void initializeAdapter(int position, RecyclerView rvCategories) {
        List<CategoriesDetails> listOfCategoryDetails;
        String id = listOfMenuDetails.get(position).getId();
        listOfCategoryDetails = listOfMenuDetails.get(position).getListOfCategoriesDetails();
        if (listOfCategoryDetails != null && listOfCategoryDetails.size() != 0) {
            rvCategories.setVisibility(View.VISIBLE);
            ShopCategoriesAdapter shopCategoriesAdapter = new ShopCategoriesAdapter(context, listOfCategoryDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            rvCategories.setLayoutManager(layoutManager);
            rvCategories.setItemAnimator(new DefaultItemAnimator());
            rvCategories.setAdapter(shopCategoriesAdapter);
        } else {
            gotToProductsList(id);
        }
    }

    private void gotToProductsList(String value) {
        Intent productsIntent = new Intent(context, ProductsListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(context.getString(R.string.coming_from), context.getString(R.string.shop_by_category));
        bundle.putString(context.getString(R.string.filter_key), "menu_id");
        bundle.putString(context.getString(R.string.filter_value), value);
        productsIntent.putExtras(bundle);
        context.startActivity(productsIntent);
    }
}
