package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.SubSubCategoriesDetails;
import com.tradebeauser.utils.StringUtils;

import java.util.List;

public class ShopSubSubCategoriesAdapter extends RecyclerView.Adapter<ShopSubSubCategoriesAdapter.ViewHolder> {
    private Context context;
    private List<SubSubCategoriesDetails> listOfSubSubCategoriesDetails;

    public ShopSubSubCategoriesAdapter(Context context, List<SubSubCategoriesDetails> listOfSubSubCategoriesDetails) {
        this.context = context;
        this.listOfSubSubCategoriesDetails = listOfSubSubCategoriesDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_shop_sub_sub_category_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = listOfSubSubCategoriesDetails.get(position).getName();
        holder.tvName.setText(StringUtils.toTitleCase(name));
    }

    @Override
    public int getItemCount() {
        return listOfSubSubCategoriesDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String id = listOfSubSubCategoriesDetails.get(getAbsoluteAdapterPosition()).getId();
                gotToProductsList(id);

        }

        private void gotToProductsList(String value) {
            Intent productsIntent = new Intent(context, ProductsListActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.coming_from),context.getString(R.string.shop_by_category));
            bundle.putString(context.getString(R.string.filter_key), "sub_sub_cat_id");
            bundle.putString(context.getString(R.string.filter_value), value);
            productsIntent.putExtras(bundle);
            context.startActivity(productsIntent);
        }
    }
}
