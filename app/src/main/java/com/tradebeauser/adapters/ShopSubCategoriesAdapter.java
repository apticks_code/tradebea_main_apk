package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.SubCategoriesDetails;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.SubSubCategoriesDetails;
import com.tradebeauser.utils.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class ShopSubCategoriesAdapter extends RecyclerView.Adapter<ShopSubCategoriesAdapter.ViewHolder> {
    private Context context;
    private List<SubCategoriesDetails> listOfSubCategoriesDetails;
    private LinkedList<Boolean> listOfClickedStatus;

    public ShopSubCategoriesAdapter(Context context, List<SubCategoriesDetails> listOfSubCategoriesDetails) {
        this.context = context;
        this.listOfSubCategoriesDetails = listOfSubCategoriesDetails;
        listOfClickedStatus = new LinkedList<>();
        for (int index = 0; index < listOfSubCategoriesDetails.size(); index++) {
            listOfClickedStatus.add(false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_shop_sub_category_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = listOfSubCategoriesDetails.get(position).getName();
        holder.tvName.setText(StringUtils.toTitleCase(name));
        List<SubSubCategoriesDetails> listOfSubSubCategoriesDetails;
        listOfSubSubCategoriesDetails = listOfSubCategoriesDetails.get(position).getListOfSubSubCategoriesDetails();
        if(listOfSubSubCategoriesDetails!=null && listOfSubSubCategoriesDetails.size()!=0) {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        }else {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        boolean clickStatus = listOfClickedStatus.get(position);

        if (listOfSubSubCategoriesDetails != null && listOfSubSubCategoriesDetails.size() != 0) {
            if (clickStatus) {
                holder.llSubCategory.setBackground(context.getDrawable(R.drawable.triangle_shape));
                holder.rvSubList.setVisibility(View.VISIBLE);
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
            } else {
                holder.rvSubList.setVisibility(View.GONE);
                holder.llSubCategory.setBackground(null);
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
            }
        } else {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return listOfSubCategoriesDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName;
        private RecyclerView rvSubList;
        private LinearLayout llSubCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llSubCategory = itemView.findViewById(R.id.llSubCategory);
            LinearLayout llName = itemView.findViewById(R.id.llName);
            rvSubList = itemView.findViewById(R.id.rvSubList);
            llName.setOnClickListener(this);
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        @Override
        public void onClick(View view) {
            boolean clickStatus = listOfClickedStatus.get(getAbsoluteAdapterPosition());
            List<SubSubCategoriesDetails> listOfSubSubCategoriesDetails;
            listOfSubSubCategoriesDetails = listOfSubCategoriesDetails.get(getAbsoluteAdapterPosition()).getListOfSubSubCategoriesDetails();
            if (listOfSubSubCategoriesDetails != null && listOfSubSubCategoriesDetails.size() != 0) {
                if (clickStatus) {
                    listOfClickedStatus.add(getAbsoluteAdapterPosition(), false);
                    llSubCategory.setBackground(null);
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
                    rvSubList.setVisibility(View.GONE);
                } else {
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
                    llSubCategory.setBackground(context.getDrawable(R.drawable.triangle_shape));
                    listOfClickedStatus.add(getAbsoluteAdapterPosition(), true);
                    initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
                }
            }else {
                tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
            }
        }
    }

    private void initializeAdapter(int position, RecyclerView rvCategories) {
        List<SubSubCategoriesDetails> listOfSubSubCategoriesDetails;
        String id = listOfSubCategoriesDetails.get(position).getId();
        listOfSubSubCategoriesDetails = listOfSubCategoriesDetails.get(position).getListOfSubSubCategoriesDetails();
        if(listOfSubSubCategoriesDetails!=null && listOfSubSubCategoriesDetails.size()!=0) {
            rvCategories.setVisibility(View.VISIBLE);
            ShopSubSubCategoriesAdapter shopSubSubCategoriesAdapter = new ShopSubSubCategoriesAdapter(context, listOfSubSubCategoriesDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            rvCategories.setLayoutManager(layoutManager);
            rvCategories.setItemAnimator(new DefaultItemAnimator());
            rvCategories.setAdapter(shopSubSubCategoriesAdapter);
        }else{
            gotToProductsList(id);
        }
    }

    private void gotToProductsList(String value) {
        Intent productsIntent = new Intent(context, ProductsListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(context.getString(R.string.coming_from),context.getString(R.string.shop_by_category));
        bundle.putString(context.getString(R.string.filter_key), "sub_cat_id");
        bundle.putString(context.getString(R.string.filter_value), value);
        productsIntent.putExtras(bundle);
        context.startActivity(productsIntent);
    }

}
