package com.tradebeauser.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;
import com.tradebeauser.R;
import com.tradebeauser.models.NavigationMenuItems;
import com.tradebeauser.utils.UserData;

import java.util.LinkedList;

public class NavigationMenuAdapter extends RecyclerView.Adapter<NavigationMenuAdapter.ViewHolder> {

    private Context context;
    private ItemClickListener itemClickListener;
    private LinkedList<NavigationMenuItems> listOfNavigationMenuItems;

    private String userName = "";
    private String userImageUrl = "";
    private String userUniqueId = "";

    private Long mobileNumber;

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_HEADER = 0;
    private int rowIndex = 1;

    public NavigationMenuAdapter(Context context, LinkedList<NavigationMenuItems> listOfNavigationMenuItems, String userName, String userImageUrl, String userUniqueId, Long mobileNumber) {
        this.context = context;
        this.userName = userName;
        this.mobileNumber = mobileNumber;
        this.userImageUrl = userImageUrl;
        this.userUniqueId = userUniqueId;
        this.listOfNavigationMenuItems = listOfNavigationMenuItems;
        int selectedIndex = UserData.getInstance().getSelectedIndex();
       /* if(selectedIndex!= -1){
            rowIndex = selectedIndex;
        }*/
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navigation_menu_items_header, parent, false);
            return new ViewHolder(view, viewType);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navigation_menu_items, parent, false);
            return new ViewHolder(view, viewType);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder.holderId == TYPE_ITEM) {
            NavigationMenuItems navigationMenuItems = listOfNavigationMenuItems.get(position - 1);
            int menuItemImage = navigationMenuItems.getMenuItemImage();
            String menuItemName = navigationMenuItems.getMenuItemName();
            holder.tvMenuTitle.setText(menuItemName);
            holder.ivMenuIcon.setImageResource(menuItemImage);

            if (rowIndex == position) {
                holder.llMenuItem.setBackgroundColor(Color.parseColor("#FEDBC7"));
                // holder.tvName.setTextColor(Color.parseColor("#ffffff"));
            } else {
                holder.llMenuItem.setBackgroundColor(Color.parseColor("#20FFFFFF"));
                // holder.tvName.setTextColor(Color.parseColor("#000000"));
            }
        } else {
            if (userName != null) {
                if (!userName.isEmpty()) {
                    holder.tvUserName.setText(userName);
                } else {
                    holder.tvUserName.setText(String.valueOf(mobileNumber));
                }
            } else {
                holder.tvUserName.setText(String.valueOf(mobileNumber));
            }

            holder.tvUniqueCode.setText(userUniqueId);
            if (userImageUrl != null) {
                if (!userImageUrl.isEmpty()) {
                    Glide.with(context)
                            .load(userImageUrl)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .placeholder(R.drawable.ic_user_image)
                            .error(R.drawable.ic_user_image)
                            .into(holder.ivUserPic);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfNavigationMenuItems.size() + 1;
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private int holderId;
        private LinearLayout llMenuItem;
        private ImageView ivMenuIcon, ivCount, ivUserPic;
        private TextView tvMenuTitle, tvUserName, tvUniqueCode;


        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            if (viewType == TYPE_ITEM) {
                holderId = TYPE_ITEM;
                ivCount = itemView.findViewById(R.id.ivCount);
                ivMenuIcon = itemView.findViewById(R.id.ivMenuIcon);
                tvMenuTitle = itemView.findViewById(R.id.tvMenuTitle);
                llMenuItem = itemView.findViewById(R.id.llMenuItem);
                itemView.setOnClickListener(this);
            } else {
                ivUserPic = itemView.findViewById(R.id.ivUserPic);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvUniqueCode = itemView.findViewById(R.id.tvUniqueCode);
                tvUserName.setOnClickListener(this);
                itemView.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(view, getLayoutPosition());
            }
        }
    }
}
