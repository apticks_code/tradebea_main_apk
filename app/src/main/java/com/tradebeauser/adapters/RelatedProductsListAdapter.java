package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.models.responseModels.productDetailsResponse.ResultDetails;
import com.tradebeauser.utils.CommonMethods;

import java.util.List;

public class RelatedProductsListAdapter extends RecyclerView.Adapter<RelatedProductsListAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private List<ResultDetails> listOfProductDetails;

    private int offsetCount = 0;


    public RelatedProductsListAdapter(Context context, List<ResultDetails> listOfProductDetails) {
        this.context = context;
        this.listOfProductDetails = listOfProductDetails;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_related_product_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String image = listOfProductDetails.get(position).getImage();
        String name = listOfProductDetails.get(position).getName();
        String price = listOfProductDetails.get(position).getPrice();
        String discount = listOfProductDetails.get(position).getDiscount();
        String id = listOfProductDetails.get(position).getId();
        Double salePrice = null;

        holder.ibPlus.setOnClickListener(this);
        holder.ibMinus.setOnClickListener(this);
        holder.tvAddCart.setOnClickListener(this);

        holder.ibPlus.setTag(position);
        holder.ibMinus.setTag(position);
        holder.tvAddCart.setTag(position);

        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);
        if(name!=null && !name.isEmpty()){
            holder.tvName.setText(name);
        }else {
            holder.tvName.setText("N/A");
        }

        if(price!=null && !price.isEmpty()){
            holder.tvOriginalPrice.setText(String.format("%s%s", context.getString(R.string.rs), price));
        }else {
            holder.tvOriginalPrice.setText("N/A");
        }

        if(discount!=null && !discount.isEmpty()){
            holder.tvDiscount.setText(String.format("%s%% off", discount));
        }else {
            holder.tvDiscount.setText("N/A");
        }

        if(price!=null && !price.isEmpty() && discount!=null && !discount.isEmpty()){
            salePrice = CommonMethods.getSalePrice(discount,price);
        }
        if(salePrice!=null) {
            holder.tvPriceAfterDiscount.setText(String.valueOf(salePrice));
        } else {
            holder.tvPriceAfterDiscount.setText("N/A");
        }
    }


    @Override
    public int getItemCount() {
        return listOfProductDetails.size();
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        switch (view.getId()){
            case R.id.tvAddCart:
                break;
            case R.id.ibPlus:
                break;
            case R.id.ibMinus:
                break;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RoundedImageView ivImage;
        private ProgressBar loadingSpinner;
        private LinearLayout llQuantity;
        private ImageButton ibPlus, ibMinus;
        private TextView tvOriginalPrice,tvDiscount,tvName,tvAddCart,tvQuantity,tvPriceAfterDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ibPlus = itemView.findViewById(R.id.ibPlus);
            ibMinus = itemView.findViewById(R.id.ibMinus);
            llQuantity = itemView.findViewById(R.id.llQuantity);
            tvPriceAfterDiscount = itemView.findViewById(R.id.tvPriceAfterDiscount);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddCart = itemView.findViewById(R.id.tvAddCart);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            ivImage = itemView.findViewById(R.id.ivImage);
            loadingSpinner = itemView.findViewById(R.id.loadingSpinner);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent productDescriptionIntent = new Intent(context, ProductDescriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.product_id),listOfProductDetails.get(getAbsoluteAdapterPosition()).getId());
            bundle.putString(context.getString(R.string.product_price),listOfProductDetails.get(getAbsoluteAdapterPosition()).getPrice());
            bundle.putString(context.getString(R.string.product_discount),listOfProductDetails.get(getAbsoluteAdapterPosition()).getDiscount());
            bundle.getString(context.getString(R.string.longitude),"78.391487");
            bundle.getString(context.getString(R.string.latitude),"17.44829");
            productDescriptionIntent.putExtras(bundle);
            context.startActivity(productDescriptionIntent);
        }
    }
}
