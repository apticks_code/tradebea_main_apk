package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeauser.R;
import com.tradebeauser.activities.SelectedOrderDetailsActivity;
import com.tradebeauser.models.responseModels.myOrdersResponse.OrderData;
import com.tradebeauser.models.responseModels.myOrdersResponse.OrderDetails;
import com.tradebeauser.models.responseModels.myOrdersResponse.ProductDetails;
import com.tradebeauser.models.responseModels.myOrdersResponse.ProductImages;
import com.tradebeauser.models.responseModels.myOrdersResponse.VariantDetails;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.CustomerAppStatus;

import java.util.LinkedList;

public class MyOrdersFragmentAdapter extends RecyclerView.Adapter<MyOrdersFragmentAdapter.ViewHolder> {

    private Context context;
    private final Transformation transformation;

    private LinkedList<OrderData> listOfOrderData;

    public MyOrdersFragmentAdapter(Context context, LinkedList<OrderData> listOfOrderData) {
        this.context = context;
        this.listOfOrderData = listOfOrderData;

        transformation = new RoundedTransformationBuilder()
                .cornerRadius(0, 5)
                .cornerRadius(1, 5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_my_orders_fragment_items, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderData orderData = listOfOrderData.get(position);
        if (orderData != null) {
            String trackId = orderData.getTrackId();
            String orderDeliveryOTP = orderData.getOrderDeliveryOTP();
            LinkedList<OrderDetails> listOfOrderDetails = orderData.getListOfOrderDetails();
            OrderDetails orderDetails = listOfOrderDetails.get(0);
            LinkedList<CustomerAppStatus> listOfCustomerAppStatuses = orderData.getListOfCustomerAppStatuses();

            holder.tvTrackId.setText("Track Id : " + trackId);

            if (listOfCustomerAppStatuses != null) {
                if (listOfCustomerAppStatuses.size() != 0) {
                    for (int index = 0; index < listOfCustomerAppStatuses.size(); index++) {
                        CustomerAppStatus customerAppStatus = listOfCustomerAppStatuses.get(index);
                        String customerName = customerAppStatus.getName();
                        String createdAt = customerAppStatus.getCreatedAt();
                        if (createdAt != null) {
                            holder.tvProductStatus.setText(customerName);
                        }
                    }
                }
            }

            if (orderDeliveryOTP != null) {
                if (!orderDeliveryOTP.isEmpty()) {
                    holder.tvOTP.setText("Delivery OTP : " + orderDeliveryOTP);
                } else {
                    holder.tvOTP.setText("Delivery OTP : " + "N/A");
                }
            } else {
                holder.tvOTP.setText("Delivery OTP : " + "N/A");
            }
            if (orderDetails != null) {
                int orderId = orderDetails.getOrderId();
                int productId = orderDetails.getProductId();
                int quantity = orderDetails.getQty();
                int variantId = orderDetails.getVariantId();
                ProductDetails productDetails = orderDetails.getProductDetails();
                VariantDetails variantDetails = orderDetails.getVariantDetails();
                LinkedList<ProductImages> listOfProductImages = orderDetails.getListOfProductImages();

                if (productDetails != null) {
                    String productName = productDetails.getName();
                    String productDescription = productDetails.getDesc();
                    String productCode = productDetails.getProductCode();

                    if (productName != null) {
                        holder.tvProductName.setText(productName);
                    }
                    if (productCode != null) {
                        holder.tvProductCode.setText(productCode);
                    }
                    if (productDescription != null) {
                        holder.tvProductDescription.setText(productDescription);
                    }
                }

                holder.tvProductQuantity.setText("Qty: " + quantity);

                if (listOfProductImages != null) {
                    if (listOfProductImages.size() != 0) {
                        ProductImages productImages = listOfProductImages.get(0);
                        if (productImages != null) {
                            String imageUrl = productImages.getImage();
                            setProductImage(holder, imageUrl);
                        }
                    }
                }
            }
        }
    }

    private void setProductImage(ViewHolder holder, String imageUrl) {
        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivImage);
        }
    }

    @Override
    public int getItemCount() {
        return listOfOrderData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivImage;
        private TextView tvTrackId, tvOTP, tvProductStatus, tvProductQuantity, tvProductCode, tvProductName, tvProductDescription, tvProductArrivingDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOTP = itemView.findViewById(R.id.tvOTP);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvTrackId = itemView.findViewById(R.id.tvTrackId);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductCode = itemView.findViewById(R.id.tvProductCode);
            tvProductStatus = itemView.findViewById(R.id.tvProductStatus);
            tvProductQuantity = itemView.findViewById(R.id.tvProductQuantity);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);
            tvProductArrivingDate = itemView.findViewById(R.id.tvProductArrivingDate);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            OrderData orderData = listOfOrderData.get(getLayoutPosition());
            Intent orderDetailsIntent = new Intent(context, SelectedOrderDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(context.getString(R.string.order_id), orderData.getId());
            orderDetailsIntent.putExtras(bundle);
            context.startActivity(orderDetailsIntent);
        }
    }
}
