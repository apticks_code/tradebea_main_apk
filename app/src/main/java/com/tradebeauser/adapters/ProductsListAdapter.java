package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.ApiCalls.ProductsSearchApiCall;
import com.tradebeauser.ApiCalls.ProductsSearchWithFiltersApiCall;
import com.tradebeauser.ApiCalls.RelatedProductsSearchApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.productDetailsResponse.ProductDetailsResponse;
import com.tradebeauser.models.responseModels.productDetailsResponse.ResultDetails;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.StringUtils;

import java.util.List;
import java.util.Objects;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ViewHolder> implements HttpReqResCallBack, View.OnClickListener {

    private Context context;
    private ProgressBar loadingSpinner;
    private List<ResultDetails> listOfProductDetails;

    private int offsetCount = 0;
    private String searchText = "";
    private String sortOrder = "";
    private String longitude = "";
    private String latitude = "";
    private String comingFrom = "";
    private String filterKey = "";
    private String filterValue = "";
    private boolean isFilterApplied;


    public ProductsListAdapter(Context context, List<ResultDetails> listOfProductDetails, String searchText, String sortOrder, boolean isFilterApplied, String latitude, String longitude, String comingFrom, String filterKey, String filterValue) {
        this.context = context;
        this.searchText = searchText;
        this.sortOrder = sortOrder;
        this.latitude = latitude;
        this.longitude = longitude;
        this.filterKey = filterKey;
        this.filterValue = filterValue;
        this.comingFrom = comingFrom;
        this.isFilterApplied = isFilterApplied;
        this.listOfProductDetails = listOfProductDetails;
    }

    public void updateProductsList(List<ResultDetails> listOfProductDetails, String searchText) {
        this.listOfProductDetails = listOfProductDetails;
        this.searchText = searchText;
        offsetCount = 0;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String image = listOfProductDetails.get(position).getImage();
        String name = StringUtils.toTitleCase(listOfProductDetails.get(position).getName());
        String price = listOfProductDetails.get(position).getPrice();
        String discount = listOfProductDetails.get(position).getDiscount();
        String id = listOfProductDetails.get(position).getId();
        Double salePrice = null;

        holder.ibPlus.setOnClickListener(this);
        holder.ibMinus.setOnClickListener(this);
        holder.tvAddCart.setOnClickListener(this);

        holder.ibPlus.setTag(position);
        holder.ibMinus.setTag(position);
        holder.tvAddCart.setTag(position);

        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);

        if (name != null && !name.isEmpty()) {
            holder.tvName.setText(name);
        } else {
            holder.tvName.setText("N/A");
        }

        if (price != null && !price.isEmpty()) {
            if (!price.equalsIgnoreCase("0")) {
                holder.tvOriginalPrice.setVisibility(View.VISIBLE);
                holder.tvOriginalPrice.setText(String.format("%s%s", context.getString(R.string.rs), price));
            } else {
                holder.tvOriginalPrice.setVisibility(View.GONE);
            }
        } else {
            holder.tvOriginalPrice.setVisibility(View.GONE);
            holder.tvOriginalPrice.setText("N/A");
        }

        if (discount != null && !discount.isEmpty()) {
            if (!discount.equalsIgnoreCase("0")) {
                holder.tvDiscount.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText(String.format("%s%% off", discount));
            } else {
                holder.tvDiscount.setVisibility(View.GONE);
            }
        } else {
            holder.tvDiscount.setVisibility(View.GONE);
            holder.tvDiscount.setText("N/A");
        }

        if (price != null && !price.isEmpty() && discount != null && !discount.isEmpty()) {
            salePrice = CommonMethods.getSalePrice(discount, price);
        }
        if (salePrice != null) {
            holder.tvPriceAfterDiscount.setText(String.valueOf(salePrice));
        } else {
            holder.tvPriceAfterDiscount.setText("N/A");
        }
        loadMore(holder, position);
    }

    private void loadMore(ViewHolder holder, int position) {
        if (position == listOfProductDetails.size() - 1) {
            offsetCount = offsetCount + 10;
            loadingSpinner = holder.loadingSpinner;
            if (!isFilterApplied) {
                loadingSpinner.setVisibility(View.VISIBLE);
                ProductsSearchApiCall.serviceCallToGetProductsList(context, null, this, searchText, latitude, longitude, "10", String.valueOf(offsetCount), sortOrder);
            } else {
                if (comingFrom.equalsIgnoreCase(context.getString(R.string.shop_by_category))) {
                    loadingSpinner.setVisibility(View.VISIBLE);
                    RelatedProductsSearchApiCall.serviceCallToGetRelatedProductsList(context, null, this, latitude, longitude, "10", String.valueOf(offsetCount), filterValue, filterKey, sortOrder);
                } else {
                    loadingSpinner.setVisibility(View.VISIBLE);
                    ProductsSearchWithFiltersApiCall.serviceCallToGetProductsListWithFilters(context, null, this, searchText, latitude, longitude, "10", String.valueOf(offsetCount), sortOrder);
                }
            }
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH:
            case Constants.SERVICE_CALL_FOR_RELATED_PRODUCTS:
            case Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS:
                if (jsonResponse != null) {
                    try {
                        ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                        if (productDetailsResponse != null) {
                            boolean status = productDetailsResponse.getStatus();
                            String message = productDetailsResponse.getMessage();
                            if (status) {
                                List<ResultDetails> listOfTempProductDetails = productDetailsResponse.getData().getListOfResults();
                                if (listOfTempProductDetails != null) {
                                    if (listOfTempProductDetails.size() != 0) {
                                        listOfProductDetails.addAll(listOfTempProductDetails);
                                        notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, Objects.requireNonNull(context).getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(context, Objects.requireNonNull(context).getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                if (loadingSpinner != null) {
                    loadingSpinner.setVisibility(View.GONE);
                    loadingSpinner = null;
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listOfProductDetails.size();
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        switch (view.getId()) {
            case R.id.tvAddCart:
                break;
            case R.id.ibPlus:
                break;
            case R.id.ibMinus:
                break;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RoundedImageView ivImage;
        private ProgressBar loadingSpinner;
        private LinearLayout llQuantity;
        private ImageButton ibPlus, ibMinus;
        private TextView tvOriginalPrice, tvDiscount, tvName, tvAddCart, tvQuantity, tvPriceAfterDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ibPlus = itemView.findViewById(R.id.ibPlus);
            ibMinus = itemView.findViewById(R.id.ibMinus);
            llQuantity = itemView.findViewById(R.id.llQuantity);
            tvPriceAfterDiscount = itemView.findViewById(R.id.tvPriceAfterDiscount);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddCart = itemView.findViewById(R.id.tvAddCart);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            ivImage = itemView.findViewById(R.id.ivImage);
            loadingSpinner = itemView.findViewById(R.id.loadingSpinner);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent productDescriptionIntent = new Intent(context, ProductDescriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.product_id), listOfProductDetails.get(getAbsoluteAdapterPosition()).getId());
            bundle.putString(context.getString(R.string.product_price), listOfProductDetails.get(getAbsoluteAdapterPosition()).getPrice());
            bundle.putString(context.getString(R.string.product_discount), listOfProductDetails.get(getAbsoluteAdapterPosition()).getDiscount());
            bundle.getString(context.getString(R.string.longitude), longitude);
            bundle.getString(context.getString(R.string.latitude), latitude);
            productDescriptionIntent.putExtras(bundle);
            context.startActivity(productDescriptionIntent);
        }
    }
}
