package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.ApiCalls.DeleteCartProductApiCall;
import com.tradebeauser.ApiCalls.UpdateCartProductApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.AppliedPromoDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ExtraCharges;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ProductDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ProductInfo;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ProductStatus;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.VariantDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.VariantValues;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.productImages;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.StringUtils;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ProductCheckOutAdapter extends RecyclerView.Adapter<ProductCheckOutAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data checkoutData;

    private List<ProductDetails> listOfCheckoutProductDetails;
    private LinkedHashMap<Integer, ViewHolder> mapOfPositionViewHolders;

    private String token = "";


    public ProductCheckOutAdapter(Context context, com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data checkoutData, String token) {
        this.context = context;
        this.token = token;
        this.checkoutData = checkoutData;
        if (checkoutData != null) {
            listOfCheckoutProductDetails = checkoutData.getListOfProductDetails();
        }
        mapOfPositionViewHolders = new LinkedHashMap<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_checkout_product_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductDetails productDetails = listOfCheckoutProductDetails.get(position);

        int quantity = productDetails.getQuantity();
        ProductInfo productInfo = productDetails.getProductInfo();
        ProductStatus productStatus = productDetails.getProductStatus();
        ExtraCharges extraCharges = productDetails.getExtraCharges();
        AppliedPromoDetails appliedPromoDetails = productDetails.getAppliedPromoDetails();
        LinkedList<VariantDetails> listOfVariantDetails = productDetails.getListOfVariantDetails();

        setProductImageDetails(holder, productInfo);
        setProductName(holder, productInfo);
        setItemPriceCalculations(holder, productInfo, extraCharges, appliedPromoDetails, quantity, listOfVariantDetails);
        setProductPrice(holder, listOfVariantDetails);

        mapOfPositionViewHolders.put(position, holder);
        holder.ibPlus.setOnClickListener(this);
        holder.ibMinus.setOnClickListener(this);
        holder.ivDelete.setOnClickListener(this);
        holder.tvSaveForLater.setOnClickListener(this);
        holder.tvTotalQuantity.setText(String.valueOf(quantity) + " " + "Qty");
        holder.ibPlus.setTag(position);
        holder.ibMinus.setTag(position);
        holder.ivDelete.setTag(position);
        holder.tvSaveForLater.setTag(position);

        holder.tvQuantity.setText(String.valueOf(quantity));

        if (listOfVariantDetails != null) {
            if (listOfVariantDetails.size() != 0) {
                LinkedList<VariantValues> listOfVariantValues = listOfCheckoutProductDetails.get(position).getListOfVariantDetails().get(0).getListOfVariantValues();
                if (listOfVariantValues != null && listOfVariantValues.size() != 0) {
                    for (int index = 0; index < listOfVariantValues.size(); index++) {
                        String title = listOfVariantValues.get(index).getOption().getName();
                        String value = listOfVariantValues.get(index).getOptionItem().getValue();
                        initializeDetailsView(title, value, holder);
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void setItemPriceCalculations(ViewHolder holder, ProductInfo productInfo, ExtraCharges extraCharges, AppliedPromoDetails appliedPromoDetails, int quantity, LinkedList<VariantDetails> listOfVariantDetails) {
        String deliveryCharges = extraCharges.getDeliveryFee();
        String tax = extraCharges.getTax();
        int promoCodeDiscount = 0;
        if (deliveryCharges == null) {
            deliveryCharges = "0";
        } else if (deliveryCharges.isEmpty()) {
            deliveryCharges = "0";
        }
        if (tax == null) {
            tax = "0";
        } else if (tax.isEmpty()) {
            tax = "0";
        }

        if (appliedPromoDetails != null) {
            promoCodeDiscount = appliedPromoDetails.getDiscount();
        }

        if (listOfVariantDetails != null) {
            if (listOfVariantDetails.size() != 0) {
                String mrp = listOfVariantDetails.get(0).getMrp();
                String discount = listOfVariantDetails.get(0).getDiscount();

                double mrpDouble = quantity * Double.parseDouble(mrp);
                double deliveryFeeDouble = Double.parseDouble(deliveryCharges);
                double discountAmount = CommonMethods.convertDiscountPercentageIntoAmount(discount, String.valueOf(mrpDouble));
                double promoCodeDiscountAmount = CommonMethods.convertDiscountPercentageIntoAmount(String.valueOf(promoCodeDiscount), String.valueOf(mrpDouble));
                double taxAmount = CommonMethods.convertTaxPercentageIntoAmount(tax, String.valueOf(mrpDouble));
                double grandTotal = mrpDouble - discountAmount + taxAmount + deliveryFeeDouble;

                holder.tvSubTotal.setText(context.getString(R.string.rs) + "" + mrp + "/-");
                holder.tvDeliveryFee.setText(context.getString(R.string.rs) + "" + deliveryCharges + "/-");
                holder.tvDiscountOnMRP.setText("-" + context.getString(R.string.rs) + "" + discountAmount + "/-");
                holder.tvTax.setText(context.getString(R.string.rs) + "" + taxAmount + "/-");
                holder.tvGrandTotal.setText(context.getString(R.string.rs) + "" + grandTotal + "/-");
            }
        }
    }

    private void setProductName(ViewHolder holder, ProductInfo productInfo) {
        if (productInfo != null) {
            String productName = productInfo.getName();
            if (productName != null) {
                if (!productName.isEmpty()) {
                    holder.tvName.setText(StringUtils.toTitleCase(productName));
                } else {
                    holder.tvName.setText("N/A");
                }
            }
        }
    }

    private void setProductImageDetails(ViewHolder holder, ProductInfo productInfo) {
        if (productInfo != null) {
            List<productImages> listOfProductImages = productInfo.getListOfProductImages();
            if (listOfProductImages != null) {
                if (listOfProductImages.size() != 0) {
                    String imageUrl = listOfProductImages.get(0).getImage();
                    if (imageUrl != null) {
                        if (!imageUrl.isEmpty()) {
                            Glide.with(context)
                                    .load(imageUrl)
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder)
                                    .into(holder.ivImage);
                        }
                    }
                }
            }
        }
    }

    private void setProductPrice(ViewHolder holder, List<VariantDetails> listOfVariantDetails) {
        if (listOfVariantDetails != null) {
            if (listOfVariantDetails.size() != 0) {
                String mrp = listOfVariantDetails.get(0).getMrp();
                String discount = listOfVariantDetails.get(0).getDiscount();


                if (discount != null) {
                    if (!discount.isEmpty()) {
                        double discountAmount = CommonMethods.convertDiscountPercentageIntoAmount(discount, mrp);
                        holder.tvDiscountOnMRP.setText(String.format("%s%s", context.getString(R.string.rs), discountAmount));
                    } else {
                        holder.tvDiscountOnMRP.setText(String.format("%s%s", context.getString(R.string.rs), 0.0));
                    }
                } else {
                    holder.tvDiscountOnMRP.setText(String.format("%s%s", context.getString(R.string.rs), 0.0));
                }

                if (mrp != null) {
                    if (!mrp.isEmpty()) {
                        if (!mrp.equalsIgnoreCase("0")) {
                            holder.tvOriginalPrice.setVisibility(View.VISIBLE);
                            holder.tvOriginalPrice.setText(String.format("%s%s", context.getString(R.string.rs), mrp));
                        } else {
                            holder.tvOriginalPrice.setVisibility(View.GONE);
                        }
                    } else {
                        mrp = "";
                        holder.tvOriginalPrice.setText("N/A");
                        holder.tvOriginalPrice.setVisibility(View.GONE);
                    }
                } else {
                    mrp = "";
                    holder.tvOriginalPrice.setText("N/A");
                    holder.tvOriginalPrice.setVisibility(View.GONE);
                }


                if (discount != null) {
                    if (!discount.isEmpty()) {
                        if (!discount.equalsIgnoreCase("0")) {
                            holder.tvDiscount.setVisibility(View.VISIBLE);
                            holder.tvDiscount.setText(String.format("%s%% off", discount));
                        } else {
                            holder.tvDiscount.setVisibility(View.GONE);
                        }
                    } else {
                        discount = "";
                        holder.tvDiscount.setText("N/A");
                        holder.tvDiscount.setVisibility(View.GONE);
                    }
                } else {
                    discount = "";
                    holder.tvDiscount.setText("N/A");
                    holder.tvDiscount.setVisibility(View.GONE);
                }


                if (!mrp.isEmpty() && !discount.isEmpty()) {
                    double salePrice = CommonMethods.getSalePrice(discount, mrp);
                    holder.tvPriceAfterDiscount.setText(String.format("%s%s", context.getString(R.string.rs), salePrice));
                } else {
                    holder.tvPriceAfterDiscount.setText("N/A");
                }
            }
        }
    }

    private void initializeDetailsView(String Title, String name, ViewHolder holder) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_cart_product_info_container_item, holder.llProductDetails, false);
        TextView tvKey = view.findViewById(R.id.tvKey);
        TextView tvValue = view.findViewById(R.id.tvValue);
        tvKey.setText(String.format("%s : ", Title));
        if (name != null && !name.isEmpty()) {
            tvValue.setText(name);
        } else {
            tvValue.setText("N/A");
        }
        holder.llProductDetails.addView(view);
    }

    @Override
    public int getItemCount() {
        return listOfCheckoutProductDetails.size();
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        ViewHolder holder = mapOfPositionViewHolders.get(position);
        int quantity = 0;
        assert holder != null;
        quantity = Integer.parseInt(holder.tvQuantity.getText().toString());
        switch (view.getId()) {

            case R.id.ibPlus:

                quantity++;
                // holder.tvQuantity.setText(quantity);
                updateProduct(listOfCheckoutProductDetails.get(position).getProductId(), quantity, "1");
                break;
            case R.id.ibMinus:
                quantity--;
                //  holder.tvQuantity.setText(quantity);
                updateProduct(listOfCheckoutProductDetails.get(position).getProductId(), quantity, "1");
                break;
            case R.id.tvDelete:
                deleteProduct(listOfCheckoutProductDetails.get(position).getProductId());
                break;
            case R.id.tvSaveForLater:
                updateProduct(listOfCheckoutProductDetails.get(position).getProductId(), quantity, "2");
                break;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivDelete;
        private RoundedImageView ivImage;
        private ImageButton ibPlus, ibMinus;
        private LinearLayout llQuantity, llProductDetails;
        private TextView tvOriginalPrice, tvDiscount, tvDiscountOnMRP, tvName, tvQuantity, tvPriceAfterDiscount, tvSaveForLater, tvSubTotal, tvDeliveryFee, tvTax, tvGrandTotal, tvTotalQuantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTax = itemView.findViewById(R.id.tvTax);
            tvName = itemView.findViewById(R.id.tvName);
            ibPlus = itemView.findViewById(R.id.ibPlus);
            ivImage = itemView.findViewById(R.id.ivImage);
            ibMinus = itemView.findViewById(R.id.ibMinus);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            tvSubTotal = itemView.findViewById(R.id.tvSubTotal);
            llQuantity = itemView.findViewById(R.id.llQuantity);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvGrandTotal = itemView.findViewById(R.id.tvGrandTotal);
            tvDeliveryFee = itemView.findViewById(R.id.tvDeliveryFee);
            tvSaveForLater = itemView.findViewById(R.id.tvSaveForLater);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvTotalQuantity = itemView.findViewById(R.id.tvTotalQuantity);
            tvDiscountOnMRP = itemView.findViewById(R.id.tvDiscountOnMRP);
            llProductDetails = itemView.findViewById(R.id.llProductDetails);
            tvPriceAfterDiscount = itemView.findViewById(R.id.tvPriceAfterDiscount);

            ibPlus.setOnClickListener(this);
            ibMinus.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent productDescriptionIntent = new Intent(context, ProductDescriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.product_id), listOfCheckoutProductDetails.get(getAbsoluteAdapterPosition()).getProductId());
            bundle.putString(context.getString(R.string.product_price), listOfCheckoutProductDetails.get(getAbsoluteAdapterPosition()).getListOfVariantDetails().get(0).getMrp());
            bundle.putString(context.getString(R.string.product_discount), listOfCheckoutProductDetails.get(getAbsoluteAdapterPosition()).getListOfVariantDetails().get(0).getDiscount());
            bundle.getString(context.getString(R.string.longitude), "");
            bundle.getString(context.getString(R.string.latitude), "");
            productDescriptionIntent.putExtras(bundle);
            context.startActivity(productDescriptionIntent);
        }

    }


    private void deleteProduct(String id) {
        //showProgressBar();
        DeleteCartProductApiCall.serviceCallToDeleteCartProduct(context, null, null, id, token);
    }

    private void updateProduct(String id, int quantity, String status) {
        //showProgressBar();
        UpdateCartProductApiCall.serviceCallToUpdateCartProduct(context, null, null, id, status, String.valueOf(quantity), token);
    }


}