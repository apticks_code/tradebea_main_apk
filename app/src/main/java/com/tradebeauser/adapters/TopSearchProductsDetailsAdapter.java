package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.models.responseModels.productDetailsResponse.ResultDetails;

import java.util.LinkedList;
import java.util.List;

public class TopSearchProductsDetailsAdapter extends RecyclerView.Adapter<TopSearchProductsDetailsAdapter.ViewHolder> {

    private Context context;
    private Transformation transformation;

    private List<ResultDetails> listOfTopDealsProductDetails;

    private String latitude = "";
    private String longitude = "";

    public TopSearchProductsDetailsAdapter(Context context, List<ResultDetails> listOfTopDealsProductDetails, String latitude, String longitude) {
        this.context = context;
        this.latitude = latitude;
        this.longitude = longitude;
        this.listOfTopDealsProductDetails = listOfTopDealsProductDetails;

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(3)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_search_product_details_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ResultDetails resultDetails = listOfTopDealsProductDetails.get(position);
        String discount = resultDetails.getDiscount();
        String imageUrl = resultDetails.getImage();
        holder.tvPrice.setText(String.format("%s%% off", discount));
        setImage(holder, imageUrl);
    }

    private void setImage(ViewHolder holder, String imageUrl) {
        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivImage);
        }
    }

    @Override
    public int getItemCount() {
        return listOfTopDealsProductDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvPrice;
        private ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvPrice = itemView.findViewById(R.id.tvPrice);
            ivImage = itemView.findViewById(R.id.ivImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent productDescriptionIntent = new Intent(context, ProductDescriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.product_id), listOfTopDealsProductDetails.get(getAbsoluteAdapterPosition()).getId());
            bundle.putString(context.getString(R.string.product_price), listOfTopDealsProductDetails.get(getAbsoluteAdapterPosition()).getPrice());
            bundle.putString(context.getString(R.string.product_discount), listOfTopDealsProductDetails.get(getAbsoluteAdapterPosition()).getDiscount());
            bundle.getString(context.getString(R.string.longitude), longitude);
            bundle.getString(context.getString(R.string.latitude), latitude);
            productDescriptionIntent.putExtras(bundle);
            context.startActivity(productDescriptionIntent);
        }
    }
}
