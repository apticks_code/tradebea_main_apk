package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.activities.MenuOptionsActivity;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.MenuBrands;

import java.util.List;

public class MenuBrandsAdapter extends RecyclerView.Adapter<MenuBrandsAdapter.ViewHolder> {

    private Context context;
    private List<MenuBrands> listOfMenuItemDetails;

    public MenuBrandsAdapter(Context context, List<MenuBrands> listOfMenuItemDetails) {
        this.context = context;
        this.listOfMenuItemDetails = listOfMenuItemDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu_brands_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = listOfMenuItemDetails.get(position).getName();
        String image = listOfMenuItemDetails.get(position).getImage();
        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);
        holder.tvName.setText(name);
    }

    @Override
    public int getItemCount() {
        return listOfMenuItemDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;
        private ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            tvName = itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent productsIntent = new Intent(context, ProductsListActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.coming_from), context.getString(R.string.shop_by_category));
            bundle.putString(context.getString(R.string.filter_key), "brand_id");
            bundle.putString(context.getString(R.string.filter_value), listOfMenuItemDetails.get(getAbsoluteAdapterPosition()).getID());
            productsIntent.putExtras(bundle);
            context.startActivity(productsIntent);
        }
    }
}
