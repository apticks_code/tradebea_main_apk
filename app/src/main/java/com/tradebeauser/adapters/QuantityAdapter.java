package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.QuantityPopupActivity;
import com.tradebeauser.interfaces.LocationUpdateCallBack;
import com.tradebeauser.interfaces.SelectedQuantityCallBack;

public class QuantityAdapter extends RecyclerView.Adapter<QuantityAdapter.ViewHolder> {
    private Context context;
    private int quantity;

    public QuantityAdapter(Context context, int quantity) {
        this.context = context;
        this.quantity = quantity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_quantity_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCount.setText(String.valueOf(position+1));
    }

    @Override
    public int getItemCount() {
        return quantity;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvCount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCount = itemView.findViewById(R.id.tvCount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            SelectedQuantityCallBack selectedQuantityCallBack = (SelectedQuantityCallBack) context;
            selectedQuantityCallBack.OnQuantitySelected(getAbsoluteAdapterPosition()+1);

        }
    }
}
