package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.models.responseModels.productDescriptionResponse.Values;

import java.util.List;

public class ProductOptionValueChipsAdapter extends RecyclerView.Adapter<ProductOptionValueChipsAdapter.ViewHolder> {
    private Context context;
    private List<Values> listOfOptionValues;
    private ItemClickListener clickListener;

    private String labelId = "";
    private int selectedItemPosition = -1;
    private String selectedItemID = "";

    public ProductOptionValueChipsAdapter(Context context, List<Values> listOfOptionValues, String labelId) {
        this.context = context;
        this.labelId = labelId;
        this.listOfOptionValues = listOfOptionValues;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_option_chips_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String id = listOfOptionValues.get(position).getId();
        String value = listOfOptionValues.get(position).getValue();

        if(value!=null){
            holder.llContainer.setTag(labelId+"&&"+id);
            holder.tvOptionValueName.setText(value);
        }

        if(position== selectedItemPosition){
            holder.llContainer.setBackground(ContextCompat.getDrawable(context,R.drawable.orange_box_bg));
        }else{
            holder.llContainer.setBackground(ContextCompat.getDrawable(context,R.drawable.grey_box_bg));
        }
    }

    public void updateSelectedItem(int position) {
        selectedItemPosition = position;
        selectedItemID = listOfOptionValues.get(position).getId();
        notifyDataSetChanged();
    }

    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return listOfOptionValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {
        private LinearLayout llContainer;
        private TextView tvOptionValueName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            llContainer = itemView.findViewById(R.id.llContainer);
            tvOptionValueName = itemView.findViewById(R.id.tvOptionValueName);
            llContainer.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onClick(view, getLayoutPosition());
        }
    }

}
