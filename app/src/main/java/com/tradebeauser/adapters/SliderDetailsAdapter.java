package com.tradebeauser.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetails;
import com.tradebeauser.utils.ImageHelper;

import java.io.IOException;
import java.util.List;

public class SliderDetailsAdapter extends RecyclerView.Adapter<SliderDetailsAdapter.ViewHolder>{
    private Context context;
    private List<SliderDetails> listOfSliderDetails;

    public SliderDetailsAdapter(Context context, List<SliderDetails> listOfSliderDetails) {
        this.context = context;
        this.listOfSliderDetails = listOfSliderDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_slider_details_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String image = listOfSliderDetails.get(position).getImage();
        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);
 /*       Bitmap imageBitmap = null;
        Bitmap finalImage = null;
        //URL url = new URL(image);
        imageBitmap =  ImageHelper.getBitmapFromURL(image);
        if (imageBitmap != null) {
            finalImage = ImageHelper.getRoundedCornerBitmap(imageBitmap);
        }
        holder.ivImage.setImageBitmap(finalImage);*/
    }

    @Override
    public int getItemCount() {
        return listOfSliderDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private RoundedImageView ivImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
        }
    }
}
