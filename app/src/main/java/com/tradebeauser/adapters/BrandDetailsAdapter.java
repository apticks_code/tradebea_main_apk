package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.brandsDetailsResponse.BrandDetails;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetails;

import java.util.LinkedList;
import java.util.List;

public class BrandDetailsAdapter extends RecyclerView.Adapter<BrandDetailsAdapter.ViewHolder> {

    private Context context;
    private List<BrandDetails> listOfBrandItemDetails;

    public BrandDetailsAdapter(Context context, LinkedList<BrandDetails> listOfBrandItemDetails) {
        this.context = context;
        this.listOfBrandItemDetails = listOfBrandItemDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_brand_details_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String image = listOfBrandItemDetails.get(position).getImage();
        if (image != null) {
            if (!image.isEmpty()) {
                Glide.with(context)
                        .load(image)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .into(holder.ivImage);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfBrandItemDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RoundedImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            BrandDetails brandDetails = listOfBrandItemDetails.get(getLayoutPosition());
            Intent productsIntent = new Intent(context, ProductsListActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.coming_from), context.getString(R.string.shop_by_category));
            bundle.putString(context.getString(R.string.filter_key), "brand_id");
            bundle.putString(context.getString(R.string.filter_value), brandDetails.getID());
            productsIntent.putExtras(bundle);
            context.startActivity(productsIntent);
        }
    }
}
