package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetails;

import java.util.LinkedList;
import java.util.List;

public class HomePageBannerImagesAdapter extends PagerAdapter {

    private Context context;
    private LinkedList<SliderDetails> listOfBannerDetails;

    public HomePageBannerImagesAdapter(Context context, LinkedList<SliderDetails> listOfBannerDetails) {
        this.context = context;
        this.listOfBannerDetails = listOfBannerDetails;
    }

    @Override
    public int getCount() {
        return listOfBannerDetails.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_slider_images_pager_item, container, false);
        SliderDetails sliderDetails = listOfBannerDetails.get(position);
        String bannerImageUrl = sliderDetails.getImage();
        ImageView ivImage = view.findViewById(R.id.ivImage);
        Glide.with(context)
                .load(bannerImageUrl)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(ivImage);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //container.removeView( object);
    }
}
