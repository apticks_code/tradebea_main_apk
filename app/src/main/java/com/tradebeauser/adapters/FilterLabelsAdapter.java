package com.tradebeauser.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsFilterActivity;
import com.tradebeauser.models.responseModels.productDetailsResponse.FilterLabelDetails;

import java.util.List;

public class FilterLabelsAdapter extends RecyclerView.Adapter<FilterLabelsAdapter.ViewHolder> {

    private Context context;
    private List<FilterLabelDetails> listOfFilterLabels;

    private ItemClickListener clickListener;

    private int rowIndex = 0;

    public FilterLabelsAdapter(Context context, List<FilterLabelDetails> listOfFilterLabels) {
        this.context = context;
        this.listOfFilterLabels = listOfFilterLabels;
    }


    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_filter_label_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(listOfFilterLabels.get(position).getName());

        if(rowIndex==position){
            holder.tvName.setBackgroundColor(Color.parseColor("#ffffff"));
           // holder.tvName.setTextColor(Color.parseColor("#ffffff"));
        }
        else
        {
            holder.tvName.setBackgroundColor(Color.parseColor("#10000000"));
           // holder.tvName.setTextColor(Color.parseColor("#000000"));
        }
    }

    @Override
    public int getItemCount() {
        return listOfFilterLabels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvName;
        private LinearLayout llItemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llItemView = itemView.findViewById(R.id.llItemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                rowIndex=getLayoutPosition();
                notifyDataSetChanged();
                clickListener.onClick(view, getLayoutPosition());
            }
        }
    }
}
