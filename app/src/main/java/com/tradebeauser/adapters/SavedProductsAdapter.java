package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeauser.ApiCalls.UpdateCartProductApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.interfaces.CloseCallBack;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.AddOrUpdateOrDeleteResponse.AddOrUpdateOrDeleteResponse;
import com.tradebeauser.models.responseModels.CartProductsResponse.Data;
import com.tradebeauser.models.responseModels.CartProductsResponse.Product;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.DialogUtils;
import com.tradebeauser.utils.StringUtils;

import java.util.List;

public class SavedProductsAdapter extends RecyclerView.Adapter<SavedProductsAdapter.ViewHolder> implements HttpReqResCallBack, View.OnClickListener {

    private Context context;
    private Dialog progressDialog;
    private Transformation transformation;

    private List<Data> listOfSavedProductDetails;

    private String token = "";


    public SavedProductsAdapter(Context context, List<Data> listOfSavedProductDetails, String token) {
        this.context = context;
        this.token = token;
        this.listOfSavedProductDetails = listOfSavedProductDetails;

        transformation = new RoundedTransformationBuilder()
                .cornerRadius(0, 5)
                .cornerRadius(1, 5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_saved_product_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Data data = listOfSavedProductDetails.get(position);
        String image = data.getImage();
        Product product = data.getProductDetails();
        String mrp = data.getListOfVariantDetails().get(0).getMrp();
        String discount = data.getListOfVariantDetails().get(0).getDiscount();

        setProductName(holder, product);
        setProductPrice(holder, mrp, discount);
        setProductImage(holder, image);
        holder.tvAddCart.setTag(position);
        holder.tvAddCart.setOnClickListener(this);
    }

    private void setProductName(ViewHolder holder, Product product) {
        String name = "";
        if (product != null) {
            name = StringUtils.toTitleCase(product.getName());
        }
        if (!name.isEmpty()) {
            holder.tvName.setText(name);
        } else {
            holder.tvName.setText("N/A");
        }
    }

    private void setProductPrice(ViewHolder holder, String price, String discount) {
        Double salePrice = 0.0;
        if (price != null && !price.isEmpty()) {
            holder.tvOriginalPrice.setText(String.format("%s%s", context.getString(R.string.rs), price));
        } else {
            holder.tvOriginalPrice.setText("N/A");
        }

        if (discount != null && !discount.isEmpty()) {
            holder.tvDiscount.setText(String.format("%s%% off", discount));
        } else {
            holder.tvDiscount.setText("N/A");
        }

        if (price != null && !price.isEmpty() && discount != null && !discount.isEmpty()) {
            salePrice = CommonMethods.getSalePrice(discount, price);
        }
        if (salePrice != null) {
            holder.tvPriceAfterDiscount.setText(context.getString(R.string.rs) + "" + String.valueOf(salePrice));
        } else {
            holder.tvPriceAfterDiscount.setText("N/A");
        }
    }

    private void setProductImage(ViewHolder holder, String imageUrl) {
        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivImage);
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS:
                if (jsonResponse != null) {
                    AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                    if (addOrUpdateOrDeleteResponse != null) {
                        boolean status = addOrUpdateOrDeleteResponse.getStatus();
                        String message = addOrUpdateOrDeleteResponse.getMessage();
                        if (status) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                        }
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listOfSavedProductDetails.size();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        String quantity = listOfSavedProductDetails.get(position).getQuantity();
        switch (view.getId()) {
            case R.id.tvAddCart:
                showProgressBar();
                updateProduct(listOfSavedProductDetails.get(position).getId(), quantity, "1");
                break;
            default:
                break;
        }
    }

    private void updateProduct(String id, String quantity, String status) {
        UpdateCartProductApiCall.serviceCallToUpdateCartProduct(context, null, this, id, status, quantity, token);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivImage;
        private TextView tvOriginalPrice, tvDiscount, tvName, tvAddCart, tvPriceAfterDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvAddCart = itemView.findViewById(R.id.tvAddCart);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvPriceAfterDiscount = itemView.findViewById(R.id.tvPriceAfterDiscount);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent productDescriptionIntent = new Intent(context, ProductDescriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.product_id), listOfSavedProductDetails.get(getAbsoluteAdapterPosition()).getId());
            bundle.putString(context.getString(R.string.product_price), listOfSavedProductDetails.get(getAbsoluteAdapterPosition()).getListOfVariantDetails().get(0).getMrp());
            bundle.putString(context.getString(R.string.product_discount), listOfSavedProductDetails.get(getAbsoluteAdapterPosition()).getListOfVariantDetails().get(0).getDiscount());
            bundle.getString(context.getString(R.string.longitude), "");
            bundle.getString(context.getString(R.string.latitude), "");
            productDescriptionIntent.putExtras(bundle);
            context.startActivity(productDescriptionIntent);
        }
    }

    public void showProgressBar() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

