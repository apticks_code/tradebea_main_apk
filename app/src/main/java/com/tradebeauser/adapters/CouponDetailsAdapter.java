package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.PromoCodeDetails;

import java.util.LinkedList;

public class CouponDetailsAdapter extends RecyclerView.Adapter<CouponDetailsAdapter.ViewHolder> {

    private Context context;
    private ItemClickListener clickListener;

    private LinkedList<PromoCodeDetails> listOfPromoCodeDetails;

    public CouponDetailsAdapter(Context context, LinkedList<PromoCodeDetails> listOfPromoCodeDetails) {
        this.context = context;
        this.listOfPromoCodeDetails = listOfPromoCodeDetails;
    }

    public interface ItemClickListener {
        void onClick(View view, int position);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_coupon_details_bottom_sheet_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PromoCodeDetails promoCodeDetails = listOfPromoCodeDetails.get(position);
        if (promoCodeDetails != null) {
            String promoCode = promoCodeDetails.getPromoCode();
            String promoTitle = promoCodeDetails.getPromoTitle();

            holder.tvCouponCode.setText(promoCode);
            holder.tvCouponDescription.setText(promoTitle);
        }
    }

    @Override
    public int getItemCount() {
        return listOfPromoCodeDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvCouponCode, tvCouponDescription, tvApply;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvApply = itemView.findViewById(R.id.tvApply);
            tvCouponCode = itemView.findViewById(R.id.tvCouponCode);
            tvCouponDescription = itemView.findViewById(R.id.tvCouponDescription);
            tvApply.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onClick(view, getLayoutPosition());
        }
    }
}
