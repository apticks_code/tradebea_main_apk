package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.tradebeauser.R;

import java.util.LinkedList;

public class WelcomeBannerImagesAdapter extends PagerAdapter {

    private Context context;
    private LinkedList<Integer> listOfBannerImages;

    public WelcomeBannerImagesAdapter(Context context, LinkedList<Integer> listOfBannerImages) {
        this.context = context;
        this.listOfBannerImages = listOfBannerImages;
    }

    @Override
    public int getCount() {
        return listOfBannerImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_welcome_banner_images_pager_item, container, false);
        int bannerImageUrl = listOfBannerImages.get(position);
        ImageView ivImage = view.findViewById(R.id.ivImage);
        ivImage.setImageResource(bannerImageUrl);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //container.removeView( object);
    }
}
