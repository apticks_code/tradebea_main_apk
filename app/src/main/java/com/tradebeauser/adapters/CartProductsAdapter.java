package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeauser.ApiCalls.DeleteCartProductApiCall;
import com.tradebeauser.ApiCalls.UpdateCartProductApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductDescriptionActivity;
import com.tradebeauser.interfaces.CloseCallBack;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.AddOrUpdateOrDeleteResponse.AddOrUpdateOrDeleteResponse;
import com.tradebeauser.models.responseModels.CartProductsResponse.Data;
import com.tradebeauser.models.responseModels.CartProductsResponse.VariantValues;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.DialogUtils;
import com.tradebeauser.utils.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;

public class CartProductsAdapter extends RecyclerView.Adapter<CartProductsAdapter.ViewHolder> implements View.OnClickListener, HttpReqResCallBack {

    private Context context;
    private Dialog progressDialog;
    private final Transformation transformation;

    private List<Data> listOfCartProductDetails;
    private LinkedHashMap<Integer, ViewHolder> mapOfPositionViewHolders;

    private String token = "";


    public CartProductsAdapter(Context context, List<Data> listOfCartProductDetails, String token) {
        this.context = context;
        this.token = token;
        this.listOfCartProductDetails = listOfCartProductDetails;
        mapOfPositionViewHolders = new LinkedHashMap<>();

        transformation = new RoundedTransformationBuilder()
                .cornerRadius(0, 5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cart_product_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String imageUrl = listOfCartProductDetails.get(position).getImage();
        String name = StringUtils.toTitleCase(listOfCartProductDetails.get(position).getProductDetails().getName());
        String price = listOfCartProductDetails.get(position).getListOfVariantDetails().get(0).getMrp();
        String discount = listOfCartProductDetails.get(position).getListOfVariantDetails().get(0).getDiscount();
        String id = listOfCartProductDetails.get(position).getId();
        String quantity = listOfCartProductDetails.get(position).getQuantity();

        Double salePrice = null;

        mapOfPositionViewHolders.put(position, holder);
        holder.ibPlus.setOnClickListener(this);
        holder.ibMinus.setOnClickListener(this);
        holder.tvDelete.setOnClickListener(this);
        holder.tvSaveForLater.setOnClickListener(this);

        holder.ibPlus.setTag(position);
        holder.ibMinus.setTag(position);
        holder.tvDelete.setTag(position);
        holder.tvSaveForLater.setTag(position);

        holder.tvQuantity.setText(quantity);

        setProductName(holder, name);
        setProductImage(holder, imageUrl);
        setProductPrice(holder, price, discount, salePrice);
        setVariantsDetails(holder, position);
    }

    private void setProductName(ViewHolder holder, String name) {
        if (!name.isEmpty()) {
            holder.tvName.setText(name);
        } else {
            holder.tvName.setText("N/A");
        }
    }

    private void setProductImage(ViewHolder holder, String imageUrl) {
        if (imageUrl != null) {
            if (!imageUrl.isEmpty()) {
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivImage);
        }
    }

    private void setProductPrice(ViewHolder holder, String price, String discount, Double salePrice) {
        if (price != null && !price.isEmpty()) {
            if (!price.equalsIgnoreCase("0")) {
                holder.tvOriginalPrice.setVisibility(View.VISIBLE);
                holder.tvOriginalPrice.setText(String.format("%s%s", context.getString(R.string.rs), price));
            } else {
                holder.tvOriginalPrice.setVisibility(View.GONE);
            }
        } else {
            holder.tvOriginalPrice.setVisibility(View.GONE);
            holder.tvOriginalPrice.setText("N/A");
        }

        if (discount != null && !discount.isEmpty()) {
            if (!discount.equalsIgnoreCase("0")) {
                holder.tvDiscount.setVisibility(View.VISIBLE);
                holder.tvDiscount.setText(String.format("%s%% off", discount));
            } else {
                holder.tvDiscount.setVisibility(View.GONE);
            }
        } else {
            holder.tvDiscount.setText("N/A");
            holder.tvDiscount.setVisibility(View.GONE);
        }

        if (price != null && !price.isEmpty() && discount != null && !discount.isEmpty()) {
            salePrice = CommonMethods.getSalePrice(discount, price);
        }

        if (salePrice != null) {
            holder.tvPriceAfterDiscount.setText(context.getString(R.string.rs) + "" + salePrice);
        } else {
            holder.tvPriceAfterDiscount.setText("N/A");
        }
    }

    private void setVariantsDetails(ViewHolder holder, int position) {
        List<VariantValues> listOfVariantValues = listOfCartProductDetails.get(position).getListOfVariantDetails().get(0).getListOfVariantValues();
        if (listOfVariantValues != null && listOfVariantValues.size() != 0) {
            for (int index = 0; index < listOfVariantValues.size(); index++) {
                String title = listOfVariantValues.get(index).getOption().getName();
                String value = listOfVariantValues.get(index).getOptionItem().getValue();
                initializeDetailsView(title, value, holder);
            }
        }
    }

    private void initializeDetailsView(String Title, String name, ViewHolder holder) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_cart_product_info_container_item, holder.llProductDetails, false);
        TextView tvKey = view.findViewById(R.id.tvKey);
        TextView tvValue = view.findViewById(R.id.tvValue);
        tvKey.setText(String.format("%s : ", Title));
        if (name != null && !name.isEmpty()) {
            tvValue.setText(name);
        } else {
            tvValue.setText("N/A");
        }
        holder.llProductDetails.addView(view);
    }


    @Override
    public int getItemCount() {
        return listOfCartProductDetails.size();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        ViewHolder holder = mapOfPositionViewHolders.get(position);
        int quantity = Integer.parseInt(holder.tvQuantity.getText().toString());
        assert holder != null;
        switch (view.getId()) {
            case R.id.ibPlus:
                quantity++;
                updateProduct(listOfCartProductDetails.get(position).getId(), quantity, "1");
                break;
            case R.id.ibMinus:
                quantity--;
                updateProduct(listOfCartProductDetails.get(position).getId(), quantity, "1");
                break;
            case R.id.tvDelete:
                deleteProduct(listOfCartProductDetails.get(position).getId());
                break;
            case R.id.tvSaveForLater:
                updateProduct(listOfCartProductDetails.get(position).getId(), quantity, "2");
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivImage;
        private ImageButton ibPlus, ibMinus;
        private LinearLayout llQuantity, llProductDetails;
        private TextView tvOriginalPrice, tvDiscount, tvName, tvQuantity, tvPriceAfterDiscount, tvDelete, tvSaveForLater;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            ibPlus = itemView.findViewById(R.id.ibPlus);
            ibMinus = itemView.findViewById(R.id.ibMinus);
            tvDelete = itemView.findViewById(R.id.tvDelete);
            llQuantity = itemView.findViewById(R.id.llQuantity);
            tvSaveForLater = itemView.findViewById(R.id.tvSaveForLater);
            tvOriginalPrice = itemView.findViewById(R.id.tvOriginalPrice);
            llProductDetails = itemView.findViewById(R.id.llProductDetails);
            tvPriceAfterDiscount = itemView.findViewById(R.id.tvPriceAfterDiscount);


            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            ivImage = itemView.findViewById(R.id.ivImage);

            ibPlus.setOnClickListener(this);
            ibMinus.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Data data = listOfCartProductDetails.get(getLayoutPosition());
            if (data != null) {
                Intent productDescriptionIntent = new Intent(context, ProductDescriptionActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.product_id), data.getId());
                bundle.putString(context.getString(R.string.product_price), data.getListOfVariantDetails().get(0).getMrp());
                bundle.putString(context.getString(R.string.product_discount), data.getListOfVariantDetails().get(0).getDiscount());
                bundle.getString(context.getString(R.string.longitude), "");
                bundle.getString(context.getString(R.string.latitude), "");
                productDescriptionIntent.putExtras(bundle);
                context.startActivity(productDescriptionIntent);
            }
        }

    }


    private void deleteProduct(String id) {
        showProgressBar();
        DeleteCartProductApiCall.serviceCallToDeleteCartProduct(context, null, this, id, token);
    }

    private void updateProduct(String id, int quantity, String status) {
        showProgressBar();
        UpdateCartProductApiCall.serviceCallToUpdateCartProduct(context, null, this, id, status, String.valueOf(quantity), token);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_DELETE_PRODUCT_FROM_CART:
            case Constants.SERVICE_CALL_FOR_UPDATE_CART_PRODUCT_STATUS:
                if (jsonResponse != null) {
                    AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                    if (addOrUpdateOrDeleteResponse != null) {
                        boolean status = addOrUpdateOrDeleteResponse.getStatus();
                        String message = addOrUpdateOrDeleteResponse.getMessage();
                        if (status) {
                            CloseCallBack closeCallBack = (CloseCallBack) context;
                            closeCallBack.close();
                        }
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    public void showProgressBar() {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
