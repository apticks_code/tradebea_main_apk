package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.CategoriesDetails;
import com.tradebeauser.models.responseModels.shopByCategoryResponse.SubCategoriesDetails;
import com.tradebeauser.utils.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class ShopCategoriesAdapter extends RecyclerView.Adapter<ShopCategoriesAdapter.ViewHolder> {
    private Context context;
    private List<CategoriesDetails> listOfCategoryDetails;
    private LinkedList<Boolean> listOfClickedStatus;

    public ShopCategoriesAdapter(Context context, List<CategoriesDetails> listOfCategoryDetails) {
        this.context = context;
        this.listOfCategoryDetails = listOfCategoryDetails;
        listOfClickedStatus = new LinkedList<>();
        for (int index = 0; index < listOfCategoryDetails.size(); index++) {
            listOfClickedStatus.add(false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_shop_category_item, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = listOfCategoryDetails.get(position).getName();
        holder.tvName.setText(StringUtils.toTitleCase(name));
        List<SubCategoriesDetails> listOfSubCategoriesDetails;
        listOfSubCategoriesDetails = listOfCategoryDetails.get(position).getListOfSubCategoriesDetails();
        boolean clickStatus = listOfClickedStatus.get(position);

        if (listOfSubCategoriesDetails != null && listOfSubCategoriesDetails.size() != 0) {
            if (clickStatus) {
                holder.llCategory.setBackground(context.getDrawable(R.drawable.grey_triangle_shape));
                holder.rvSubList.setVisibility(View.VISIBLE);
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
            } else {
                holder.rvSubList.setVisibility(View.GONE);
                holder.llCategory.setBackground(null);
                holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
            }
        } else {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return listOfCategoryDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName;
        private RecyclerView rvSubList;
        private LinearLayout llName, llCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            llName = itemView.findViewById(R.id.llName);
            llCategory = itemView.findViewById(R.id.llCategory);
            LinearLayout llName = itemView.findViewById(R.id.llName);
            rvSubList = itemView.findViewById(R.id.rvSubList);
            llName.setOnClickListener(this);
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        @Override
        public void onClick(View view) {
            boolean clickStatus = listOfClickedStatus.get(getAbsoluteAdapterPosition());
            List<SubCategoriesDetails> listOfSubCategoriesDetails;
            listOfSubCategoriesDetails = listOfCategoryDetails.get(getAbsoluteAdapterPosition()).getListOfSubCategoriesDetails();
            if (listOfSubCategoriesDetails != null && listOfSubCategoriesDetails.size() != 0) {
                if (clickStatus) {
                    listOfClickedStatus.add(getAbsoluteAdapterPosition(), false);
                    llCategory.setBackground(null);
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
                    rvSubList.setVisibility(View.GONE);
                } else {
                    tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
                    llCategory.setBackground(context.getDrawable(R.drawable.grey_triangle_shape));
                    listOfClickedStatus.add(getAbsoluteAdapterPosition(), true);
                    initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
                }
            }else {
                tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
            }
        }

    }

    private void initializeAdapter(int position, RecyclerView rvCategories) {
        List<SubCategoriesDetails> listOfSubCategoriesDetails;
        String id = listOfCategoryDetails.get(position).getId();
        listOfSubCategoriesDetails = listOfCategoryDetails.get(position).getListOfSubCategoriesDetails();
        if(listOfSubCategoriesDetails!=null && listOfSubCategoriesDetails.size()!=0) {
            rvCategories.setVisibility(View.VISIBLE);
            ShopSubCategoriesAdapter ShopSubCategoriesAdapter = new ShopSubCategoriesAdapter(context, listOfSubCategoriesDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            rvCategories.setLayoutManager(layoutManager);
            rvCategories.setItemAnimator(new DefaultItemAnimator());
            rvCategories.setAdapter(ShopSubCategoriesAdapter);
        }else{
            gotToProductsList(id);
        }
    }

    private void gotToProductsList(String value) {
        Intent productsIntent = new Intent(context, ProductsListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(context.getString(R.string.coming_from),context.getString(R.string.shop_by_category));
        bundle.putString(context.getString(R.string.filter_key), "cat_id");
        bundle.putString(context.getString(R.string.filter_value), value);
        productsIntent.putExtras(bundle);
        context.startActivity(productsIntent);
    }

}
