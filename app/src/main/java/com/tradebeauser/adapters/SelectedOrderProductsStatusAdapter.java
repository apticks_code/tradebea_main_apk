package com.tradebeauser.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import com.github.vipulasri.timelineview.TimelineView;
import com.tradebeauser.R;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.CustomerAppStatus;

import java.util.LinkedList;

public class SelectedOrderProductsStatusAdapter extends RecyclerView.Adapter<SelectedOrderProductsStatusAdapter.ViewHolder> {

    private Context context;
    private LinkedList<CustomerAppStatus> listOfCustomerAppStatus;

    public SelectedOrderProductsStatusAdapter(Context context, LinkedList<CustomerAppStatus> listOfCustomerAppStatus) {
        this.context = context;
        this.listOfCustomerAppStatus = listOfCustomerAppStatus;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_selected_order_product_status_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CustomerAppStatus customerAppStatus = listOfCustomerAppStatus.get(position);
        int id = customerAppStatus.getId();
        String name = customerAppStatus.getName();
        String createdAt = customerAppStatus.getCreatedAt();
        if (createdAt != null) {
            holder.tvStatusDate.setVisibility(View.VISIBLE);
            holder.tvStatusDate.setText(createdAt);
            holder.tvStatusMessage.setText(name);
            holder.timelineView.setMarker(VectorDrawableCompat.create(context.getResources(), R.drawable.custom_marker, context.getTheme()));
        } else {
            holder.tvStatusDate.setVisibility(View.GONE);
            holder.tvStatusMessage.setText(name);
            holder.timelineView.setMarker(VectorDrawableCompat.create(context.getResources(), R.drawable.custom_in_active_marker, context.getTheme()));
        }

        holder.timelineView.setLineStyle(1);
        holder.timelineView.setStartLineColor(Color.parseColor("#D50000"), 1);
        holder.timelineView.setEndLineColor(Color.parseColor("#D50000"), 1);
        holder.timelineView.setLinePadding(5);
        holder.timelineView.setLineWidth(2);
        holder.timelineView.setMarkerInCenter(false);
    }

    @Override
    public int getItemCount() {
        return listOfCustomerAppStatus.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TimelineView timelineView;
        private TextView tvStatusDate, tvStatusMessage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvStatusDate = itemView.findViewById(R.id.tvStatusDate);
            timelineView = itemView.findViewById(R.id.timelineView);
            tvStatusMessage = itemView.findViewById(R.id.tvStatusMessage);
        }
    }
}
