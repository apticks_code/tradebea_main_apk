package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.models.responseModels.productDetailsResponse.OptionalFilterDetails;
import com.tradebeauser.utils.UserData;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class OptionalFilterOptionsAdapter  extends RecyclerView.Adapter<OptionalFilterOptionsAdapter.ViewHolder> implements CompoundButton.OnCheckedChangeListener {
    private Context context;
    private String labelKey;
    private List<OptionalFilterDetails> listOfOptionalFilterDetails;
    private List<String> listOfSelectedOptions;
    private LinkedHashMap<String, List<String>> mapOfFilterLabelKeyAndSelectedOptions;

    public OptionalFilterOptionsAdapter(Context context, List<OptionalFilterDetails> listOfOptionalFilterDetails, String labelKey) {
        this.context = context;
        this.labelKey = labelKey;
        this.listOfOptionalFilterDetails = listOfOptionalFilterDetails;
        mapOfFilterLabelKeyAndSelectedOptions = UserData.getInstance().getMapOfFilterLabelKeyAndSelectedOptions();
        listOfSelectedOptions = mapOfFilterLabelKeyAndSelectedOptions.get(labelKey);
        if(mapOfFilterLabelKeyAndSelectedOptions== null)
            mapOfFilterLabelKeyAndSelectedOptions = new LinkedHashMap<>();
        if(listOfSelectedOptions==null)
            listOfSelectedOptions = new LinkedList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_filter_option_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(listOfOptionalFilterDetails.get(position).getValue());
        String ID = listOfOptionalFilterDetails.get(position).getId();
        if(listOfSelectedOptions.contains(ID)){
            holder.cbCheckbox.setChecked(true);
        }else {
            holder.cbCheckbox.setChecked(false);
        }
        holder.cbCheckbox.setTag(position);
        holder.cbCheckbox.setOnCheckedChangeListener(this);
    }

    @Override
    public int getItemCount() {
        return listOfOptionalFilterDetails.size();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        int position = (int) compoundButton.getTag();
        String ID = listOfOptionalFilterDetails.get(position).getId();
        if(compoundButton.isChecked()){
            if(!listOfSelectedOptions.contains(ID)) {
                listOfSelectedOptions.add(ID);
            }
        }else{
            listOfSelectedOptions.remove(ID);
        }

        mapOfFilterLabelKeyAndSelectedOptions.put(labelKey,listOfSelectedOptions);

        UserData.getInstance().setMapOfFilterLabelKeyAndSelectedOptions(mapOfFilterLabelKeyAndSelectedOptions);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private AppCompatCheckBox cbCheckbox;
        private TextView tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            cbCheckbox = itemView.findViewById(R.id.cbCheckbox);
        }
    }
}
