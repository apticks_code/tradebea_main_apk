package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rishabhharit.roundedimageview.RoundedImageView;
import com.tradebeauser.R;
import com.tradebeauser.activities.MenuOptionsActivity;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetails;
import com.tradebeauser.utils.ImageHelper;
import com.tradebeauser.utils.StringUtils;


import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;

public class MenuDetailsAdapter extends RecyclerView.Adapter<MenuDetailsAdapter.ViewHolder> {

    private Context context;
    private LinkedList<MenuDetails> listOfMenuItemDetails;

    public MenuDetailsAdapter(Context context, LinkedList<MenuDetails> listOfMenuItemDetails) {
        this.context = context;
        this.listOfMenuItemDetails = listOfMenuItemDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu_details_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = StringUtils.toTitleCase(listOfMenuItemDetails.get(position).getName());
        String image = listOfMenuItemDetails.get(position).getImage();
        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);
       /* Bitmap imageBitmap = null;
        Bitmap finalImage = null;
        try {
            URL url = new URL(image);
            imageBitmap =  ImageHelper.getBitmapFromURL(image);
            if (imageBitmap != null) {
                finalImage = ImageHelper.getRoundedCornerBitmap(imageBitmap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        holder.ivImage.setImageBitmap(finalImage);*/
        holder.tvName.setText(name);
    }

    @Override
    public int getItemCount() {
        return listOfMenuItemDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RoundedImageView ivImage;
        private TextView tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvName = itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, MenuOptionsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.menu_id),listOfMenuItemDetails.get(getAbsoluteAdapterPosition()).getID());
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }
}
