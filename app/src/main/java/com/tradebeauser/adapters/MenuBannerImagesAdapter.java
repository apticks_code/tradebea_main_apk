package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.tradebeauser.R;

import java.util.LinkedList;
import java.util.List;

public class MenuBannerImagesAdapter  extends PagerAdapter {

    private Context context;
    private List<String> listOfBannerImages;

    public MenuBannerImagesAdapter(Context context, List<String> listOfBannerImages) {
        this.context = context;
        this.listOfBannerImages = listOfBannerImages;
    }

    @Override
    public int getCount() {
        return listOfBannerImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_welcome_banner_images_pager_item, container, false);
        String bannerImageUrl = listOfBannerImages.get(position);
        ImageView ivImage = view.findViewById(R.id.ivImage);
        Glide.with(context)
                .load(bannerImageUrl)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(ivImage);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //container.removeView( object);
    }
}
