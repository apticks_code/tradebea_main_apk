package com.tradebeauser.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tradebeauser.R;
import com.tradebeauser.activities.ProductImagesFullScreenActivity;
import com.tradebeauser.models.responseModels.menuDetailsResponse.MenuDetails;
import com.tradebeauser.models.responseModels.productDescriptionResponse.ProductImageDetails;

import java.util.LinkedList;
import java.util.List;

public class ProductImagesAdapter extends RecyclerView.Adapter<ProductImagesAdapter.ViewHolder> {
    private Context context;
    private ImageView ivProductImage;
    private List<ProductImageDetails> listOfProductImagesDetails;

    public ProductImagesAdapter(Context context, List<ProductImageDetails> listOfProductImagesDetails, ImageView ivProductImage) {
        this.context = context;
        this.ivProductImage = ivProductImage;
        this.listOfProductImagesDetails = listOfProductImagesDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_container_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String image = listOfProductImagesDetails.get(position).getImage();
        Glide.with(context)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(holder.ivImage);
    }

    @Override
    public int getItemCount() {
        return listOfProductImagesDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String image = listOfProductImagesDetails.get(getAbsoluteAdapterPosition()).getImage();
            Glide.with(context)
                    .load(image)
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .into(ivProductImage);
        }
    }
}
