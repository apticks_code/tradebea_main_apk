package com.tradebeauser.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tradebeauser.R;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.DefaultReturnPolicy;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.Invoice;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.OrderDetail;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.Product;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.ProductImage;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.ReturnReasonId;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.ReturnRequest;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.Rules;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.SellerVarinat;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.Varinat;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

public class SelectedOrderProductsAdapter extends RecyclerView.Adapter<SelectedOrderProductsAdapter.ViewHolder> implements View.OnClickListener {

    private Context context;
    private Invoice invoice;
    private ReturnRequest returnRequest;
    private final Transformation transformation;

    private LinkedList<OrderDetail> listOfOrderDetails;

    private String trackId = "";
    private Dialog alertDialog;

    public SelectedOrderProductsAdapter(Context context, LinkedList<OrderDetail> listOfOrderDetails, ReturnRequest returnRequest, Invoice invoice, String trackId) {
        this.context = context;
        this.invoice = invoice;
        this.trackId = trackId;
        this.returnRequest = returnRequest;
        this.listOfOrderDetails = listOfOrderDetails;

        transformation = new RoundedTransformationBuilder()
                .cornerRadius(0, 5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_selected_order_product_items, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderDetail orderDetail = listOfOrderDetails.get(position);
        Product product = orderDetail.getProduct();
        Varinat varinat = orderDetail.getVarinat();
        SellerVarinat sellerVarinat = orderDetail.getSellerVarinat();

        holder.tvInvoice.setText("Track Id:" + " " + trackId);

        if (product != null) {
            String productName = product.getName();
            LinkedList<ProductImage> listOfProductImages = product.getListOfProductImages();

            setProductName(holder, productName);
            setProductImage(holder, listOfProductImages);
        }

        if (varinat != null) {
            Double discount = varinat.getDiscount();
            Double mrp = varinat.getMrp();
            if (discount != null) {
                holder.tvDiscount.setText(context.getString(R.string.discount) + " : " + context.getString(R.string.rs) + " " + discount);
            }
            if (mrp != null) {
                holder.tvMrp.setText(context.getString(R.string.mrp) + " : " + context.getString(R.string.rs) + " " + mrp);
            }
        }

        /*if (sellerVarinat != null) {
            int sku = sellerVarinat.getSku();
            if (invoice != null) {
                String invoiceNumber = invoice.getInvoiceNumber();
                holder.tvInvoice.setText("Invoice No :" + " " + invoiceNumber);
            }
        }*/

        StringBuilder stringBuilder = new StringBuilder();
        if (returnRequest != null) {
            holder.cvReturnedOrderDetails.setVisibility(View.VISIBLE);
            if (returnRequest.getOrderPickupOtp() != null) {
                holder.tvOTP.setVisibility(View.VISIBLE);
                int orderPickupOTP = returnRequest.getOrderPickupOtp();
                holder.tvOTP.setText("Return Pickup OTP" + " : " + orderPickupOTP);
            } else {
                holder.tvOTP.setVisibility(View.GONE);
            }

            if (returnRequest.getOrderReturnOtp() != null) {
                int orderReturnOTP = returnRequest.getOrderReturnOtp();
                Log.d("orderReturnOTP****", orderReturnOTP + "");
            }

            if (returnRequest.getReturnReasonId() != null) {
                ReturnReasonId returnReasonId = returnRequest.getReturnReasonId();
                if (returnReasonId != null) {
                    String reason = returnReasonId.getReason();
                    if (returnReasonId.getReason() != null) {
                        stringBuilder.append("Reason" + " : ").append(reason);
                        holder.tvReturnReason.setText(stringBuilder);
                    }
                }
            }

            if (returnRequest.getPersonalReason() != null) {
                if (!returnRequest.getPersonalReason().isEmpty()) {
                    stringBuilder.append("\n").append("Personal Reason" + " : ").append(returnRequest.getPersonalReason());
                    holder.tvReturnReason.setText(stringBuilder);
                }
            }

            if (returnRequest.getSellerAcceptedAt() != null) {
                holder.tvSellerAcceptedTime.setVisibility(View.VISIBLE);
                String sellerAcceptedAt = returnRequest.getSellerAcceptedAt();
                holder.tvSellerAcceptedTime.setText("seller accepted at" + " " + sellerAcceptedAt);
            } else {
                holder.tvSellerAcceptedTime.setVisibility(View.GONE);
            }

            if (returnRequest.getAdminAcceptedAt() != null) {
                holder.tvAdminAcceptedTime.setVisibility(View.VISIBLE);
                String adminAcceptedAt = returnRequest.getAdminAcceptedAt();
                holder.tvAdminAcceptedTime.setText("Admin accepted at" + " " + adminAcceptedAt);
            } else {
                holder.tvAdminAcceptedTime.setVisibility(View.GONE);
            }
        } else {
            holder.cvReturnedOrderDetails.setVisibility(View.GONE);
        }

        holder.tvReturnPolices.setTag(position);
        holder.tvReturnPolices.setOnClickListener(this);
    }

    private void setProductName(ViewHolder holder, String name) {
        if (!name.isEmpty()) {
            holder.tvProductName.setText(name);
        } else {
            holder.tvProductName.setText("N/A");
        }
    }

    private void setProductImage(ViewHolder holder, LinkedList<ProductImage> listOfProductImages) {
        if (listOfProductImages != null) {
            if (listOfProductImages.size() != 0) {
                ProductImage productImage = listOfProductImages.get(0);
                String imageUrl = productImage.getImage();
                Picasso.get()
                        .load(imageUrl)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            } else {
                Picasso.get()
                        .load(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivImage);
        }
    }

    @Override
    public int getItemCount() {
        return listOfOrderDetails.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        int pos = (int) view.getTag();
        if (id == R.id.tvReturnPolices) {
            OrderDetail orderDetail = listOfOrderDetails.get(pos);
            if (orderDetail != null) {
                Rules rules = orderDetail.getRules();
                if (rules != null) {
                    prepareReturnRulesDetails(rules);
                }
            }
        }
    }

    private void prepareReturnRulesDetails(Rules rules) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_return_rules, null);
        alertDialogBuilder.setView(view);
        TextView tvReturnTitle = view.findViewById(R.id.tvReturnTitle);
        TextView tvReturnBeforeTime = view.findViewById(R.id.tvReturnBeforeTime);
        TextView tvReturnPolicy = view.findViewById(R.id.tvReturnPolicy);
        DefaultReturnPolicy defaultReturnPolicy = rules.getDefaultReturnPolicy();
        if (defaultReturnPolicy != null) {
            if (defaultReturnPolicy.getReturnTitle() != null) {
                String returnTitle = defaultReturnPolicy.getReturnTitle();
                tvReturnTitle.setText(returnTitle);
            }
            if (defaultReturnPolicy.getReturnPolicies() != null) {
                String returnPolicies = defaultReturnPolicy.getReturnPolicies();
                tvReturnPolicy.setText(returnPolicies);
            }
            if (defaultReturnPolicy.getReturnBeforeInHrs() != null) {
                int returnBeforeInHrs = defaultReturnPolicy.getReturnBeforeInHrs();
                tvReturnBeforeTime.setText(String.valueOf(returnBeforeInHrs));
            }
        }
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvOk = view.findViewById(R.id.tvOk);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivImage;
        private CardView cvReturnedOrderDetails;
        private TextView tvInvoice, tvProductName, tvMrp, tvDiscount, tvOTP, tvReturnReason, tvAdminAcceptedTime, tvSellerAcceptedTime, tvReturnPolices;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvOTP = itemView.findViewById(R.id.tvOTP);
            tvMrp = itemView.findViewById(R.id.tvMrp);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvInvoice = itemView.findViewById(R.id.tvInvoice);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvReturnReason = itemView.findViewById(R.id.tvReturnReason);
            tvReturnPolices = itemView.findViewById(R.id.tvReturnPolices);
            tvAdminAcceptedTime = itemView.findViewById(R.id.tvAdminAcceptedTime);
            tvSellerAcceptedTime = itemView.findViewById(R.id.tvSellerAcceptedTime);
            cvReturnedOrderDetails = itemView.findViewById(R.id.cvReturnedOrderDetails);
        }
    }
}
