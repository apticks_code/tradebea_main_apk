package com.tradebeauser.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.AddAddressApiCall;
import com.tradebeauser.ApiCalls.DeleteAddressApiCall;
import com.tradebeauser.ApiCalls.SetActiveAddressApiCall;
import com.tradebeauser.R;
import com.tradebeauser.activities.ChooseLocationActivity;
import com.tradebeauser.activities.EditAddressActivity;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.interfaces.SelectedAddressCallBack;
import com.tradebeauser.models.responseModels.AddOrUpdateOrDeleteResponse.AddOrUpdateOrDeleteResponse;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.sliderDetailsResponse.SliderDetails;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.DialogUtils;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import java.io.Serializable;
import java.util.List;

public class SavedAddressDetailsAdapter extends RecyclerView.Adapter<SavedAddressDetailsAdapter.ViewHolder> implements View.OnClickListener, HttpReqResCallBack {

    private Context context;
    private Dialog progressDialog;

    private List<AddressDetails> listOfActiveAddress;

    private String token = "";

    private int clickedPosition;
    private int activeLocationID;

    public SavedAddressDetailsAdapter(Context context, List<AddressDetails> listOfActiveAddress, String token, int activeLocationID) {
        this.context = context;
        this.token = token;
        this.activeLocationID = activeLocationID;
        this.listOfActiveAddress = listOfActiveAddress;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_saved_address_details_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int id = listOfActiveAddress.get(position).getId();
        String tag = listOfActiveAddress.get(position).getTag();
        String geoLocation = listOfActiveAddress.get(position).getLocation().getGeoLocation();
        String address = listOfActiveAddress.get(position).getAddress();
        String landmark = listOfActiveAddress.get(position).getLandmark();
        String pinCode = String.valueOf(listOfActiveAddress.get(position).getPincode());

        String manualAddress = address + "," + landmark + "," + pinCode;
        holder.tvTag.setText(tag);
        holder.tvGeoLocation.setText(geoLocation);
        holder.tvAddress.setText(manualAddress);

        holder.ivMore.setTag(position);
        holder.ivMore.setOnClickListener(this);
        if (tag != null) {
            if (tag.equalsIgnoreCase("home")) {
                holder.ivIcon.setImageResource(R.drawable.ic_home_address);
            } else if (tag.equalsIgnoreCase("work")) {
                holder.ivIcon.setImageResource(R.drawable.ic_work_address);
            } else {
                holder.ivIcon.setImageResource(R.drawable.ic_work_address);
            }
        }
        if (id == activeLocationID) {
            holder.ivIcon.setColorFilter(ContextCompat.getColor(context, R.color.green), PorterDuff.Mode.SRC_IN);
            holder.tvTag.setTextColor(Color.BLACK);
            holder.tvAddress.setTextColor(Color.BLACK);
            holder.tvGeoLocation.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return listOfActiveAddress.size();
    }

    @Override
    public void onClick(View view) {

        int position = (int) view.getTag();
        clickedPosition = position;

        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().toString().equalsIgnoreCase(context.getString(R.string.edit))) {
                    UserData.getInstance().setSelectedAddress(listOfActiveAddress.get(position));
                    Intent intent = new Intent(context, EditAddressActivity.class);
                    context.startActivity(intent);
                } else if (item.getTitle().toString().equalsIgnoreCase(context.getString(R.string.delete))) {
                    showDeleteConfirmationDialog(position);
                }
                return true;
            }
        });
        popup.show();//showing popup menu
    }

    private void showDeleteConfirmationDialog(int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.app_name));
        alertDialogBuilder.setMessage(context.getResources().getString(R.string.do_you_want_to_delete_the_address));
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int id = listOfActiveAddress.get(clickedPosition).getId();
                showProgressBar(context);
                DeleteAddressApiCall.serviceCallToDeleteAddresses(context, null, SavedAddressDetailsAdapter.this, token, id);
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_SET_ACTIVE_LOCATION:
                if (jsonResponse != null) {
                    AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                    if (addOrUpdateOrDeleteResponse != null) {
                        boolean status = addOrUpdateOrDeleteResponse.getStatus();
                        String message = addOrUpdateOrDeleteResponse.getMessage();
                        if (status) {
                            PreferenceConnector.writeBoolean(context, context.getString(R.string.refresh_location), true);
                            UserData.getInstance().setActiveLocation(listOfActiveAddress.get(clickedPosition));
                            ((Activity) context).finish();

                            Context context = UserData.getInstance().getContext();
                            Fragment fragment = UserData.getInstance().getFragment();
                            if (fragment != null) {
                                SelectedAddressCallBack selectedAddressCallBack = (SelectedAddressCallBack) fragment;
                                selectedAddressCallBack.selectedAddress(listOfActiveAddress.get(clickedPosition));
                            } else if (context != null) {
                                SelectedAddressCallBack selectedAddressCallBack = (SelectedAddressCallBack) context;
                                selectedAddressCallBack.selectedAddress(listOfActiveAddress.get(clickedPosition));
                            }
                        }
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_DELETE_ADDRESS:
                if (jsonResponse != null) {
                    AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                    if (addOrUpdateOrDeleteResponse != null) {
                        boolean status = addOrUpdateOrDeleteResponse.getStatus();
                        String message = addOrUpdateOrDeleteResponse.getMessage();
                        if (status) {
                            listOfActiveAddress.remove(clickedPosition);
                            notifyDataSetChanged();
                        }
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;

        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView ivIcon, ivMore;
        private TextView tvTag, tvAddress, tvGeoLocation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTag = itemView.findViewById(R.id.tvTag);
            ivMore = itemView.findViewById(R.id.ivMore);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvGeoLocation = itemView.findViewById(R.id.tvGeoLocation);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickedPosition = getAbsoluteAdapterPosition();
            int id = listOfActiveAddress.get(getAbsoluteAdapterPosition()).getId();
            showProgressBar(context);
            SetActiveAddressApiCall.serviceCallToSetActiveAddresses(context, null, SavedAddressDetailsAdapter.this, token, id);
        }
    }

    private void showProgressBar(Context context) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            } else {
                closeProgressbar();
                progressDialog = null;
                progressDialog = DialogUtils.getDialogUtilsInstance().progressDialog(context);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void closeProgressbar() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
