package com.tradebeauser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.activities.ProductsListActivity;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.SubCategoriesInfo;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.SubSubCategoriesInfo;
import com.tradebeauser.utils.StringUtils;

import java.util.List;

public class MenuSubCategoriesAdapter extends RecyclerView.Adapter<MenuSubCategoriesAdapter.ViewHolder> {
    private Context context;
    private List<SubCategoriesInfo> listOfSubCategoriesDetails;

    public MenuSubCategoriesAdapter(Context context, List<SubCategoriesInfo> listOfSubCategoriesDetails) {
        this.context = context;
        this.listOfSubCategoriesDetails = listOfSubCategoriesDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_shop_sub_category_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = listOfSubCategoriesDetails.get(position).getName();
        holder.tvName.setText(StringUtils.toTitleCase(name));
        List<SubSubCategoriesInfo> listOfSubSubCategoriesDetails;
        listOfSubSubCategoriesDetails = listOfSubCategoriesDetails.get(position).getListOfSubSubCategoriesDetails();
        if(listOfSubSubCategoriesDetails!=null && listOfSubSubCategoriesDetails.size()!=0) {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
        }else {
            holder.tvName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return listOfSubCategoriesDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName;
        private RecyclerView rvSubList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            LinearLayout llName = itemView.findViewById(R.id.llName);
            rvSubList = itemView.findViewById(R.id.rvSubList);
            llName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            initializeAdapter(getAbsoluteAdapterPosition(), rvSubList);
        }
    }

    private void initializeAdapter(int position, RecyclerView rvCategories) {
        List<SubSubCategoriesInfo> listOfSubSubCategoriesDetails;
        String id = listOfSubCategoriesDetails.get(position).getId();
        listOfSubSubCategoriesDetails = listOfSubCategoriesDetails.get(position).getListOfSubSubCategoriesDetails();
        if(listOfSubSubCategoriesDetails!=null && listOfSubSubCategoriesDetails.size()!=0) {
            rvCategories.setVisibility(View.VISIBLE);
            MenuSubSubCategoriesAdapter shopSubSubCategoriesAdapter = new MenuSubSubCategoriesAdapter(context, listOfSubSubCategoriesDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            rvCategories.setLayoutManager(layoutManager);
            rvCategories.setItemAnimator(new DefaultItemAnimator());
            rvCategories.setAdapter(shopSubSubCategoriesAdapter);
        }else{
            gotToProductsList(id);
        }
    }

    private void gotToProductsList(String value) {
        Intent productsIntent = new Intent(context, ProductsListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(context.getString(R.string.coming_from),context.getString(R.string.shop_by_category));
        bundle.putString(context.getString(R.string.filter_key), "sub_cat_id");
        bundle.putString(context.getString(R.string.filter_value), value);
        productsIntent.putExtras(bundle);
        context.startActivity(productsIntent);
    }

}
