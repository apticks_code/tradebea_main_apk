package com.tradebeauser.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.ProductsSearchApiCall;
import com.tradebeauser.ApiCalls.ProductsSearchWithFiltersApiCall;
import com.tradebeauser.ApiCalls.RelatedProductsSearchApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.ProductsListAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.productDetailsResponse.ProductDetailsResponse;
import com.tradebeauser.models.responseModels.productDetailsResponse.ResultDetails;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.LocationInfo;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import org.json.JSONObject;

import java.util.List;

public class ProductsListActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, RadioGroup.OnCheckedChangeListener {

    private TextView tvError;
    private EditText etSearch;
    private RecyclerView rvProducts;
    private ImageView ivCart, ivBack;
    private LinearLayout llSort, llFilters;
    private ProductsListAdapter productsListAdapter;
    private List<ResultDetails> listOfProductDetails;
    private BottomSheetDialog pickerBottomSheetDialog;

    private String searchText = "";
    private String sortOrder = "";
    private String selectedSortOrder = "";
    private String latitude = "";
    private String longitude = "";
    private String comingFrom = "";
    private String filterKey = "";
    private String filterValue = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_products_list);
        getIntentData();
        initializeUI();
        initializeListeners();
        prepareDetails();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.products))) {
                listOfProductDetails = (List<ResultDetails>) bundle.getSerializable(getString(R.string.products));
            }
            if (bundle.containsKey(getString(R.string.searched_text))) {
                searchText = bundle.getString(getString(R.string.searched_text));
            }
            if (bundle.containsKey(getString(R.string.coming_from))) {
                comingFrom = bundle.getString(getString(R.string.coming_from));
            }
            if (bundle.containsKey(getString(R.string.filter_key))) {
                filterKey = bundle.getString(getString(R.string.filter_key));
            }
            if (bundle.containsKey(getString(R.string.filter_value))) {
                filterValue = bundle.getString(getString(R.string.filter_value));
            }
        }
        PrepareLocationDetails();
    }

    private void PrepareLocationDetails() {
        AddressDetails addressDetails = UserData.getInstance().getActiveLocation();
        if (addressDetails != null) {
            LocationInfo locationInfo = addressDetails.getLocation();
            latitude = String.valueOf(addressDetails.getLocation().getLatitude());
            longitude = String.valueOf(addressDetails.getLocation().getLongitude());
        }
    }

    private void initializeUI() {
        ivCart = findViewById(R.id.ivCart);
        ivBack = findViewById(R.id.ivBack);
        llSort = findViewById(R.id.llSort);
        tvError = findViewById(R.id.tvError);
        etSearch = findViewById(R.id.etSearch);
        llFilters = findViewById(R.id.llFilters);
        rvProducts = findViewById(R.id.rvProducts);
        if (searchText != null) {
            etSearch.setText(searchText);
        }
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llSort.setOnClickListener(this);
        llFilters.setOnClickListener(this);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isFilterApplied = PreferenceConnector.readBoolean(this, getString(R.string.filter_applied), false);
        boolean isFilterCleared = PreferenceConnector.readBoolean(this, getString(R.string.filter_cleared), false);

        if (isFilterCleared) {
            PreferenceConnector.writeBoolean(this, getString(R.string.filter_cleared), false);
            performSearch("");
        } else if (isFilterApplied) {
            PreferenceConnector.writeBoolean(this, getString(R.string.filter_applied), false);
            performSearchWithFilters();
        }
    }

    private void performSearchWithFilters() {
        showProgressBar(this);
        ProductsSearchWithFiltersApiCall.serviceCallToGetProductsListWithFilters(this, null, null, searchText, latitude, longitude, "10", "0", sortOrder);
    }

    private void performSearch(String searchText) {
        showProgressBar(this);
        ProductsSearchApiCall.serviceCallToGetProductsList(this, null, null, searchText, latitude, longitude, "10", "0", sortOrder);
    }

    private void prepareDetails() {
        if (comingFrom.equalsIgnoreCase(getString(R.string.shop_by_category))) {
            showProgressBar(this);
            RelatedProductsSearchApiCall.serviceCallToGetRelatedProductsList(this, null, null, latitude, longitude, "10", "0", filterValue, filterKey, sortOrder);
        } else {
            etSearch.setText(searchText);
            if (listOfProductDetails != null && listOfProductDetails.size() != 0) {
                listFull();
            } else {
                listEmpty();
            }
        }
    }

    private void listFull() {
        tvError.setVisibility(View.GONE);
        rvProducts.setVisibility(View.VISIBLE);
        initializeAdapter(false, comingFrom);
    }

    private void listEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvProducts.setVisibility(View.GONE);
    }

    private void initializeAdapter(boolean isFilterApplied, String comingFrom) {
        searchText = etSearch.getText().toString();
        productsListAdapter = new ProductsListAdapter(this, listOfProductDetails, searchText, sortOrder, isFilterApplied, latitude, longitude, comingFrom, filterKey, filterValue);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvProducts.setLayoutManager(layoutManager);
        rvProducts.setItemAnimator(new DefaultItemAnimator());
        rvProducts.setAdapter(productsListAdapter);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        JSONObject filtersObject;
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH:
                if (jsonResponse != null) {
                    try {
                        ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                        if (productDetailsResponse != null) {
                            boolean status = productDetailsResponse.getStatus();
                            String message = productDetailsResponse.getMessage();
                            if (status) {
                                listOfProductDetails = productDetailsResponse.getData().getListOfResults();
                                if (listOfProductDetails != null && listOfProductDetails.size() != 0) {
                                    listFull();
                                    JSONObject jsonRootObject = new JSONObject(jsonResponse);
                                    JSONObject data = jsonRootObject.getJSONObject("data");
                                    filtersObject = data.getJSONObject("filters");
                                    UserData.getInstance().setFilterDetails(filtersObject);
                                    initializeAdapter(false, comingFrom);
                                } else {
                                    listEmpty();
                                    Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                listEmpty();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            listEmpty();
                            Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_PRODUCT_SEARCH_WITH_FILTERS:
                if (jsonResponse != null) {
                    try {
                        ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                        if (productDetailsResponse != null) {
                            boolean status = productDetailsResponse.getStatus();
                            String message = productDetailsResponse.getMessage();
                            if (status) {
                                listOfProductDetails = productDetailsResponse.getData().getListOfResults();
                                if (listOfProductDetails != null && listOfProductDetails.size() != 0) {
                                    listFull();
                                    initializeAdapter(true, comingFrom);
                                } else {
                                    listEmpty();
                                    Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                listEmpty();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            listEmpty();
                            Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        listEmpty();
                        Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_RELATED_PRODUCTS:
                if (jsonResponse != null) {
                    try {
                        ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                        if (productDetailsResponse != null) {
                            boolean status = productDetailsResponse.getStatus();
                            String message = productDetailsResponse.getMessage();
                            if (status) {
                                listFull();
                                listOfProductDetails = productDetailsResponse.getData().getListOfResults();
                                JSONObject jsonRootObject = new JSONObject(jsonResponse);
                                JSONObject data = jsonRootObject.getJSONObject("data");
                                filtersObject = data.getJSONObject("filters");
                                UserData.getInstance().setFilterDetails(filtersObject);
                                initializeAdapter(true, comingFrom);
                            } else {
                                listEmpty();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            listEmpty();
                            Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        listEmpty();
                        Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void updateProductsList() {
        productsListAdapter.updateProductsList(listOfProductDetails, etSearch.getText().toString());
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.ivCart:
                Intent cartIntent = new Intent(this, CartActivity.class);
                startActivity(cartIntent);
                break;
            case R.id.llSort:
                showSortPopup();
                break;
            case R.id.llFilters:
                goToFilter();
                break;
            case R.id.ivCancel:
                closeBottomSheetView();
                break;
            default:
                break;
        }
    }

    private void goToFilter() {
        Intent filterIntent = new Intent(this, ProductsFilterActivity.class);
        startActivity(filterIntent);
    }

    private void showSortPopup() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_sort_order_sheet, null);

        ImageView ivCancel = view.findViewById(R.id.ivCancel);
        RadioGroup radioGroup = view.findViewById(R.id.radioGroup);
        RadioButton rbPriceHighToLow = view.findViewById(R.id.rbPriceHighToLow);
        RadioButton rbPriceLowToHigh = view.findViewById(R.id.rbPriceLowToHigh);

        rbPriceHighToLow.setTag("DESC");
        rbPriceLowToHigh.setTag("ASC");
        ivCancel.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);

        if (selectedSortOrder.equalsIgnoreCase(getString(R.string.price_high_to_low))) {
            rbPriceHighToLow.setChecked(true);
        } else if (selectedSortOrder.equalsIgnoreCase(getString(R.string.price_low_to_high))) {
            rbPriceLowToHigh.setChecked(true);
        }

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();

    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        selectedSortOrder = radioButton.getText().toString();
        String previouslySelectedOrder = sortOrder;
        sortOrder = radioButton.getTag().toString();
        if (radioButton.isChecked()) {
            if (!sortOrder.equalsIgnoreCase(previouslySelectedOrder)) {
                if (comingFrom.equalsIgnoreCase(getString(R.string.shop_by_category))) {
                    showProgressBar(this);
                    RelatedProductsSearchApiCall.serviceCallToGetRelatedProductsList(this, null, null, latitude, longitude, "10", "0", filterValue, filterKey, sortOrder);
                } else {
                    showProgressBar(this);
                    ProductsSearchApiCall.serviceCallToGetProductsList(this, null, null, etSearch.getText().toString(), latitude, longitude, "10", "0", sortOrder);
                }
            }
        }
        closeBottomSheetView();
    }
}
