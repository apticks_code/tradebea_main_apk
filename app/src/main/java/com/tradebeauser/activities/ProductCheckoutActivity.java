package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.tradebeauser.ApiCalls.CheckoutProductsApiCall;
import com.tradebeauser.ApiCalls.CountDetailsApiCall;
import com.tradebeauser.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeauser.ApiCalls.GetRazorPayKeyDetailsApiCall;
import com.tradebeauser.ApiCalls.OrderProductsDetailsApiCall;
import com.tradebeauser.ApiCalls.PaymentDetailsApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.CouponDetailsAdapter;
import com.tradebeauser.adapters.ProductCheckOutAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.interfaces.SelectedAddressCallBack;
import com.tradebeauser.models.requestModels.orderProductsRequest.OrderProductsRequest;
import com.tradebeauser.models.requestModels.orderProductsRequest.ProductDetailsRequest;
import com.tradebeauser.models.requestModels.paymentRequest.PaymentRequest;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.AppliedPromoDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.CheckoutProductDetailsResponse;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ExtraCharges;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ProductDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ProductInfo;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.PromoCodeDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.TotalBillingSummery;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.VariantDetails;
import com.tradebeauser.models.responseModels.countDetailsResponse.CountDetailsData;
import com.tradebeauser.models.responseModels.countDetailsResponse.CountDetailsResponse;
import com.tradebeauser.models.responseModels.orderProductsDetailsResponse.OrderProductsDetailsResponse;
import com.tradebeauser.models.responseModels.paymentDetailsResponse.PaymentDetailsData;
import com.tradebeauser.models.responseModels.paymentDetailsResponse.PaymentDetailsResponse;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.GetProfileData;
import com.tradebeauser.models.responseModels.profileResponse.LocationInfo;
import com.tradebeauser.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeauser.models.responseModels.profileResponse.UserDetails;
import com.tradebeauser.models.responseModels.razorPayKeyDetailsResponse.RazorPayData;
import com.tradebeauser.models.responseModels.razorPayKeyDetailsResponse.RazorPayKeyDetailsResponse;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class ProductCheckoutActivity extends BaseActivity implements View.OnClickListener, CouponDetailsAdapter.ItemClickListener, HttpReqResCallBack, SelectedAddressCallBack, PaymentResultWithDataListener {

    private Data checkoutData;
    private ImageView ivBackArrow;
    private LocationInfo locationInfo;
    private RecyclerView rvCartProducts;
    private RecyclerView rvCouponDetails;
    private LinearLayout llApplyCode, llPromoCodeDiscount;
    private BottomSheetDialog couponsPickerBottomSheetDialog;
    private TextView tvTag, tvAddress, tvChange, tvCheckoutBottom, tvTotal, tvSubTotal, tvDiscountOnMRP, tvCouponDiscount, tvDeliveryFee, tvTax, tvAppliedCode;

    private LinkedList<PromoCodeDetails> listOfPromoCodeDetails;

    private String tag = "";
    private String token = "";
    private String emailId = "";
    private String firstName = "";
    private String razorPayKey = "";
    private String geoLocation = "";
    private String appliedPromoCode = "";

    private double tempMRPDouble = 0.0;
    private double tempDeliveryFeeDouble = 0.0;
    private double tempDiscountAmount = 0.0;
    private double tempTaxAmount = 0.0;
    private double tempGrandTotal = 0.0;
    private double tempPromoCodeDiscountAmount = 0.0;

    private int paymentId;
    private int activeLocationId = -1;

    private long phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_product_checkout_activity);
        getDataFromIntent();
        initializeUI();
        initializeListeners();
        prepareDetails();
        preparePriceDetails();
        prepareProfileDetails();
        prepareGetRazorPayKeyDetails();
    }

    private void getDataFromIntent() {
        checkoutData = UserData.getInstance().getCheckoutData();
        AddressDetails addressDetails = UserData.getInstance().getActiveLocation();
        if (addressDetails != null) {
            locationInfo = addressDetails.getLocation();
            activeLocationId = addressDetails.getId();
            tag = addressDetails.getTag();
            geoLocation = addressDetails.getLocation().getGeoLocation();
        }
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }

    private void initializeUI() {
        tvTax = findViewById(R.id.tvTax);
        tvTag = findViewById(R.id.tvTag);
        tvTotal = findViewById(R.id.tvTotal);
        tvChange = findViewById(R.id.tvChange);
        tvAddress = findViewById(R.id.tvAddress);
        tvSubTotal = findViewById(R.id.tvSubTotal);
        llApplyCode = findViewById(R.id.llApplyCode);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvAppliedCode = findViewById(R.id.tvAppliedCode);
        tvDeliveryFee = findViewById(R.id.tvDeliveryFee);
        rvCartProducts = findViewById(R.id.rvCartProducts);
        tvDiscountOnMRP = findViewById(R.id.tvDiscountOnMRP);
        tvCouponDiscount = findViewById(R.id.tvCouponDiscount);
        tvCheckoutBottom = findViewById(R.id.tvCheckoutBottom);
        llPromoCodeDiscount = findViewById(R.id.llPromoCodeDiscount);
    }

    private void initializeListeners() {
        tvChange.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        llApplyCode.setOnClickListener(this);
        tvCheckoutBottom.setOnClickListener(this);

        UserData.getInstance().setContext(this);
        UserData.getInstance().setFragment(null);
    }

    private void prepareDetails() {
        if (tag != null) {
            tvTag.setText(tag);
        }
        if (geoLocation != null) {
            tvAddress.setText(geoLocation);
        }

        initializeAdapter();

        tvAddress.setFocusable(true);
        tvAddress.setFocusableInTouchMode(true);
        tvAddress.requestFocus();
        tvAddress.requestFocus();
    }

    @SuppressLint("SetTextI18n")
    private void preparePriceDetails() {
        tempMRPDouble = 0.0;
        tempDeliveryFeeDouble = 0.0;
        tempDiscountAmount = 0.0;
        tempTaxAmount = 0.0;
        tempGrandTotal = 0.0;
        tempPromoCodeDiscountAmount = 0.0;

        List<ProductDetails> listOfCheckoutProductDetails = checkoutData.getListOfProductDetails();
        boolean isCouponApplied = checkoutData.getCouponApplied();
        TotalBillingSummery totalBillingSummery = checkoutData.getTotalBillingSummery();
        for (int index = 0; index < listOfCheckoutProductDetails.size(); index++) {
            ProductDetails productDetails = listOfCheckoutProductDetails.get(index);
            int quantity = productDetails.getQuantity();
            ProductInfo productInfo = productDetails.getProductInfo();
            ExtraCharges extraCharges = productDetails.getExtraCharges();
            AppliedPromoDetails appliedPromoDetails = productDetails.getAppliedPromoDetails();
            LinkedList<VariantDetails> listOfVariantDetails = productDetails.getListOfVariantDetails();

            String deliveryCharges = extraCharges.getDeliveryFee();
            String tax = extraCharges.getTax();
            if (deliveryCharges == null) {
                deliveryCharges = "0";
            } else if (deliveryCharges.isEmpty()) {
                deliveryCharges = "0";
            }
            if (tax == null) {
                tax = "0";
            } else if (tax.isEmpty()) {
                tax = "0";
            }

            int promoCodeDiscount = 0;
            if (appliedPromoDetails != null) {
                promoCodeDiscount = appliedPromoDetails.getDiscount();
            }
            if (listOfVariantDetails != null) {
                if (listOfVariantDetails.size() != 0) {
                    String mrp = listOfVariantDetails.get(0).getMrp();
                    String discount = listOfVariantDetails.get(0).getDiscount();

                    double mrpDouble = quantity * Double.parseDouble(mrp);
                    double deliveryFeeDouble = Double.parseDouble(deliveryCharges);
                    double discountAmount = CommonMethods.convertDiscountPercentageIntoAmount(discount, String.valueOf(mrpDouble));
                    double promoCodeDiscountAmount = CommonMethods.convertDiscountPercentageIntoAmount(String.valueOf(promoCodeDiscount), String.valueOf(mrpDouble));
                    double taxAmount = CommonMethods.convertTaxPercentageIntoAmount(tax, String.valueOf(mrpDouble));
                    double grandTotal = mrpDouble - discountAmount + taxAmount + deliveryFeeDouble - promoCodeDiscountAmount;

                    tempMRPDouble = tempMRPDouble + mrpDouble;
                    tempDeliveryFeeDouble = tempDeliveryFeeDouble + deliveryFeeDouble;
                    tempDiscountAmount = tempDiscountAmount + discountAmount;
                    tempTaxAmount = tempTaxAmount + taxAmount;
                    tempGrandTotal = tempGrandTotal + grandTotal;
                    tempPromoCodeDiscountAmount = tempPromoCodeDiscountAmount + promoCodeDiscountAmount;

                    if (isCouponApplied) {
                        tvAppliedCode.setVisibility(View.VISIBLE);
                        tvAppliedCode.setText(appliedPromoCode + "(" + " -" + tempPromoCodeDiscountAmount + "off)");
                    } else {
                        tvAppliedCode.setVisibility(View.GONE);
                    }
                }
            }
        }

        tempMRPDouble = totalBillingSummery.getSubTotal();
        tempDiscountAmount = totalBillingSummery.getDiscountInAmount();
        tempDeliveryFeeDouble = totalBillingSummery.getDeliveryFee();
        tempTaxAmount = totalBillingSummery.getTaxInAmount();
        tempGrandTotal = totalBillingSummery.getGrandTotal();
        tempPromoCodeDiscountAmount = totalBillingSummery.getPromoDiscountAmount();

        tvSubTotal.setText(getString(R.string.rs) + "" + tempMRPDouble);
        tvDiscountOnMRP.setText(" - " + getString(R.string.rs) + "" + tempDiscountAmount);
        tvDeliveryFee.setText(getString(R.string.rs) + "" + tempDeliveryFeeDouble);
        tvTax.setText(getString(R.string.rs) + "" + tempTaxAmount);
        tvTotal.setText(getString(R.string.rs) + "" + tempGrandTotal);
        tvCouponDiscount.setText(getString(R.string.rs) + "" + tempPromoCodeDiscountAmount);
    }

    private void prepareProfileDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(ProductCheckoutActivity.this, getString(R.string.user_token), "");
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(ProductCheckoutActivity.this, null, null, token);
    }

    private void prepareGetRazorPayKeyDetails() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                token = PreferenceConnector.readString(ProductCheckoutActivity.this, getString(R.string.user_token), "");
                GetRazorPayKeyDetailsApiCall.serviceCallToGetRazorPayKeyDetails(ProductCheckoutActivity.this, null, null, token);
            }
        }).start();
    }

    private void initializeAdapter() {
        ProductCheckOutAdapter checkOutProductsAdapter = new ProductCheckOutAdapter(this, checkoutData, token);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvCartProducts.setLayoutManager(layoutManager);
        rvCartProducts.setItemAnimator(new DefaultItemAnimator());
        rvCartProducts.setAdapter(checkOutProductsAdapter);
    }

    private void prepareCartCountDetails() {
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        showProgressBar(this);
        CountDetailsApiCall.serviceCallForCountDetails(this, null, null, token, activeLocationId, "", "");
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.tvChange:
                goToAddOrChangeLocation();
                break;
            case R.id.ivClose:
                closeBottomSheetView();
                break;
            case R.id.llApplyCode:
                prepareApplyCodeDetails();
                break;
            case R.id.tvCheckoutBottom:
                prepareCartCountDetails();
                break;
            default:
                break;
        }
    }

    private void prepareProceedToPayDetails() {
        double total = tempGrandTotal;
        total = total * 100;
        Intent paymentModesIntent = new Intent(this, PaymentModesActivity.class);
        Bundle bundle = new Bundle();
        bundle.putDouble(getString(R.string.total_amount), total);
        bundle.putString(getString(R.string.first_name), firstName);
        bundle.putString(getString(R.string.email), emailId);
        bundle.putString(getString(R.string.phone_number), String.valueOf(phoneNumber));
        bundle.putString(getString(R.string.razor_pay_key), razorPayKey);
        bundle.putString("tempGrandTotal", String.valueOf(tempGrandTotal));
        bundle.putString("tempDeliveryFeeDouble", String.valueOf(tempDeliveryFeeDouble));
        bundle.putString("tempTaxAmount", String.valueOf(tempTaxAmount));
        bundle.putInt("activeLocationId", activeLocationId);
        bundle.putString(getString(R.string.promo_code), appliedPromoCode);
        paymentModesIntent.putExtras(bundle);
        startActivity(paymentModesIntent);
        //startPayment();
    }

    private void goToAddOrChangeLocation() {
        Intent intent = new Intent(this, ChooseLocationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.active_location_id), activeLocationId);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void prepareApplyCodeDetails() {
        showCouponDetailsBottomSheetView();
    }

    @SuppressLint("InflateParams")
    private void showCouponDetailsBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_coupon_details_bottom_sheet, null);
        ImageView ivClose = view.findViewById(R.id.ivClose);
        rvCouponDetails = view.findViewById(R.id.rvCouponDetails);
        ivClose.setOnClickListener(this);
        initializeCouponDetails();
        couponsPickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        couponsPickerBottomSheetDialog.setContentView(view);
        couponsPickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        couponsPickerBottomSheetDialog.show();
    }

    private void initializeCouponDetails() {
        listOfPromoCodeDetails = checkoutData.getListOfPromoCodeDetails();
        if (listOfPromoCodeDetails != null) {
            if (listOfPromoCodeDetails.size() != 0) {
                CouponDetailsAdapter couponDetailsAdapter = new CouponDetailsAdapter(this, listOfPromoCodeDetails);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
                rvCouponDetails.setLayoutManager(layoutManager);
                rvCouponDetails.setItemAnimator(new DefaultItemAnimator());
                couponDetailsAdapter.setClickListener(this);
                rvCouponDetails.setAdapter(couponDetailsAdapter);
            }
        }
    }

    private void closeBottomSheetView() {
        if (couponsPickerBottomSheetDialog != null) {
            couponsPickerBottomSheetDialog.cancel();
        }
    }

    @Override
    public void onClick(View view, int position) {
        PromoCodeDetails promoCodeDetails = listOfPromoCodeDetails.get(position);
        if (promoCodeDetails != null) {
            appliedPromoCode = promoCodeDetails.getPromoCode();
            prepareCheckout();
            closeBottomSheetView();
        }
    }

    private void prepareCheckout() {
        JsonObject checkoutRequestObject = UserData.getInstance().getCheckoutRequestObject();
        if (checkoutRequestObject != null) {
            checkoutRequestObject.remove("applied_promo_code");
            checkoutRequestObject.addProperty("applied_promo_code", appliedPromoCode);
            UserData.getInstance().setCheckoutRequestObject(checkoutRequestObject);
            showProgressBar(this);
            CheckoutProductsApiCall.serviceCallToCheckoutProducts(this, null, null, token, checkoutRequestObject);
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_CHECKOUT_PRODUCTS:
                if (jsonResponse != null) {
                    try {
                        CheckoutProductDetailsResponse checkoutProductDetailsResponse = new Gson().fromJson(jsonResponse, CheckoutProductDetailsResponse.class);
                        if (checkoutProductDetailsResponse != null) {
                            boolean status = checkoutProductDetailsResponse.getStatus();
                            String message = checkoutProductDetailsResponse.getMessage();
                            if (status) {
                                checkoutData = checkoutProductDetailsResponse.getData();
                                UserData.getInstance().setCheckoutData(checkoutData);
                                preparePriceDetails();
                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                    if (profileResponse != null) {
                        boolean status = profileResponse.getStatus();
                        String message = profileResponse.getMessage();
                        if (status) {
                            GetProfileData getProfileData = profileResponse.getGetProfileData();
                            if (getProfileData != null) {
                                UserDetails userDetails = getProfileData.getUserDetails();
                                emailId = userDetails.getEmail();
                                phoneNumber = userDetails.getMobile();
                                firstName = userDetails.getFirstName();
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_TO_GET_RAZOR_PAY_KEY_DETAILS:
                if (jsonResponse != null) {
                    RazorPayKeyDetailsResponse razorPayKeyDetailsResponse = new Gson().fromJson(jsonResponse, RazorPayKeyDetailsResponse.class);
                    if (razorPayKeyDetailsResponse != null) {
                        boolean status = razorPayKeyDetailsResponse.getStatus();
                        String message = razorPayKeyDetailsResponse.getMessage();
                        if (status) {
                            RazorPayData razorPayData = razorPayKeyDetailsResponse.getRazorPayData();
                            if (razorPayData != null) {
                                razorPayKey = razorPayData.getKeyId();
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
            case Constants.SERVICE_CALL_FOR_PAYMENT_DETAILS:
                if (jsonResponse != null) {
                    PaymentDetailsResponse paymentDetailsResponse = new Gson().fromJson(jsonResponse, PaymentDetailsResponse.class);
                    if (paymentDetailsResponse != null) {
                        boolean status = paymentDetailsResponse.getStatus();
                        if (status) {
                            PaymentDetailsData paymentDetailsData = paymentDetailsResponse.getPaymentDetailsData();
                            if (paymentDetailsData != null) {
                                paymentId = paymentDetailsData.getPaymentId();
                                closeProgressbar();
                                try {
                                    OrderProductsRequest orderProductsRequest = new OrderProductsRequest();
                                    orderProductsRequest.setDeliveryAddressId(activeLocationId);
                                    orderProductsRequest.setPaymentMethodId(2);
                                    orderProductsRequest.setPaymentId(paymentId);
                                    LinkedList<ProductDetailsRequest> listOfProductDetails = new LinkedList<>();
                                    List<ProductDetails> listOfCheckoutProductDetails = checkoutData.getListOfProductDetails();
                                    if (listOfCheckoutProductDetails != null) {
                                        if (listOfCheckoutProductDetails.size() != 0) {
                                            for (int index = 0; index < listOfCheckoutProductDetails.size(); index++) {
                                                ProductDetails productDetails = listOfCheckoutProductDetails.get(index);
                                                int quantity = productDetails.getQuantity();
                                                ExtraCharges extraCharges = productDetails.getExtraCharges();
                                                AppliedPromoDetails appliedPromoDetails = productDetails.getAppliedPromoDetails();
                                                LinkedList<VariantDetails> listOfVariantDetails = productDetails.getListOfVariantDetails();
                                                if (listOfVariantDetails != null) {
                                                    if (listOfVariantDetails.size() != 0) {
                                                        VariantDetails variantDetails = listOfVariantDetails.get(0);
                                                        String id = variantDetails.getId();
                                                        String price = variantDetails.getMrp();
                                                        String discount = variantDetails.getDiscount();
                                                        String appliedPromoDiscount = variantDetails.getDiscount();
                                                        String deliveryCharges = extraCharges.getDeliveryFee();
                                                        String tax = extraCharges.getTax();
                                                        int promoId = 0;

                                                        if (appliedPromoDetails != null) {
                                                            promoId = appliedPromoDetails.getId();
                                                        }
                                                        double priceDouble = Double.parseDouble(price);
                                                        double subTotal = priceDouble * quantity;
                                                        double deliveryChargesDouble = Double.parseDouble(deliveryCharges);
                                                        double discountAmount = CommonMethods.convertDiscountPercentageIntoAmount(discount, String.valueOf(subTotal));
                                                        double promoDiscountAmount = CommonMethods.convertDiscountPercentageIntoAmount(appliedPromoDiscount, String.valueOf(subTotal));
                                                        double taxAmount = CommonMethods.convertTaxPercentageIntoAmount(tax, String.valueOf(subTotal));

                                                        double totalAmount = subTotal - discountAmount - promoDiscountAmount + taxAmount + deliveryChargesDouble;

                                                        ProductDetailsRequest productDetailsRequest = new ProductDetailsRequest();

                                                        productDetailsRequest.setSellerUserId(productDetails.getSellerUserId());
                                                        productDetailsRequest.setProductId(productDetails.getProductId());
                                                        productDetailsRequest.setVariantId(id);
                                                        productDetailsRequest.setSellerVariantId(productDetails.getProductSellerVariantId());
                                                        productDetailsRequest.setQty(productDetails.getQuantity());
                                                        productDetailsRequest.setPrice(price);
                                                        productDetailsRequest.setSubTotal(String.valueOf(subTotal));
                                                        productDetailsRequest.setDiscount(String.valueOf(discountAmount));
                                                        productDetailsRequest.setPromoDiscount(String.valueOf(promoDiscountAmount));
                                                        productDetailsRequest.setDeliveryFee(deliveryCharges);
                                                        productDetailsRequest.setTax(String.valueOf(taxAmount));
                                                        productDetailsRequest.setTotal(String.valueOf(totalAmount));
                                                        productDetailsRequest.setPromoId(promoId);
                                                        listOfProductDetails.add(productDetailsRequest);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    orderProductsRequest.setListOfProductDetails(listOfProductDetails);
                                    showProgressBar(this);
                                    token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                                    OrderProductsDetailsApiCall.serviceCallForOrderProductsDetails(this, null, null, orderProductsRequest, token);
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            } else {
                                closeProgressbar();
                            }
                        } else {
                            closeProgressbar();
                        }
                    } else {
                        closeProgressbar();
                    }
                } else {
                    closeProgressbar();
                }
                break;
            case Constants.SERVICE_CALL_FOR_ORDER_PRODUCTS_DETAILS:
                if (jsonResponse != null) {
                    OrderProductsDetailsResponse orderProductsDetailsResponse = new Gson().fromJson(jsonResponse, OrderProductsDetailsResponse.class);
                    if (orderProductsDetailsResponse != null) {
                        boolean status = orderProductsDetailsResponse.getStatus();
                        String message = orderProductsDetailsResponse.getMessage();
                        if (status) {
                            goToHome();
                            Toast.makeText(this, getString(R.string.order_placed_successfully), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_COUNT_DETAILS:
                if (jsonResponse != null) {
                    CountDetailsResponse countDetailsResponse = new Gson().fromJson(jsonResponse, CountDetailsResponse.class);
                    if (countDetailsResponse != null) {
                        boolean status = countDetailsResponse.getStatus();
                        if (status) {
                            CountDetailsData countDetailsData = countDetailsResponse.getCountDetailsData();
                            if (countDetailsData != null) {
                                int isServiceAvailable = countDetailsData.getIsServiceAvailable();
                                if (isServiceAvailable == 1) {
                                    prepareProceedToPayDetails();
                                } else {
                                    showLocationErrorDialog();
                                }
                            }
                        }
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void showLocationErrorDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setMessage("Selected address is not under our service please change location to continue");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void goToHome() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    @Override
    public void selectedAddress(AddressDetails addressDetails) {
        AddressDetails addressDetailsTemp = UserData.getInstance().getActiveLocation();
        if (addressDetailsTemp != null) {
            locationInfo = addressDetailsTemp.getLocation();
            activeLocationId = addressDetailsTemp.getId();
            tag = addressDetailsTemp.getTag();
            geoLocation = addressDetailsTemp.getLocation().getGeoLocation();

            if (tag != null) {
                tvTag.setText(tag);
            }

            if (geoLocation != null) {
                tvAddress.setText(geoLocation);
            }
        }
    }

    private void startPayment() {
        final Activity activity = this;
        Checkout checkout = new Checkout();
        checkout.setKeyID(razorPayKey);
        checkout.setImage(R.mipmap.ic_launcher);
        double total = tempGrandTotal;
        total = total * 100;
        try {
            JSONObject options = new JSONObject();
            options.put("name", firstName);
            options.put("description", "Product Checkout");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("theme.color", "#3F51B5");
            options.put("currency", "INR");
            options.put("amount", total);//pass amount in currency subunits
            JSONObject preFill = new JSONObject();
            preFill.put("email", emailId);
            preFill.put("contact", String.valueOf(phoneNumber));
            options.put("prefill", preFill);
            checkout.open(activity, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorPayPaymentId, PaymentData paymentData) {
        showProgressBar(ProductCheckoutActivity.this);
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setId(razorPayPaymentId);
        paymentRequest.setEmail(emailId);
        paymentRequest.setAmount(Double.parseDouble(String.valueOf(tempGrandTotal)));
        paymentRequest.setFee(Double.parseDouble(String.valueOf(tempDeliveryFeeDouble)));
        paymentRequest.setTax(Double.parseDouble(String.valueOf(tempTaxAmount)));
        paymentRequest.setContact(String.valueOf(phoneNumber));
        paymentRequest.setDescription("Product Checkout");
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        PaymentDetailsApiCall.serviceCallForPaymentDetails(this, null, null, paymentRequest, token);
    }

    @Override
    public void onPaymentError(int statusCode, String errorMessage, PaymentData paymentData) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
