package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.CountDetailsApiCall;
import com.tradebeauser.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.NavigationMenuAdapter;
import com.tradebeauser.fragments.HomeFragment;
import com.tradebeauser.fragments.ShopByCategoryFragment;
import com.tradebeauser.fragments.WalletFragment;
import com.tradebeauser.fragments.YourAddressesFragment;
import com.tradebeauser.fragments.YourCartFragment;
import com.tradebeauser.fragments.MyOrdersFragment;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.NavigationMenuItems;
import com.tradebeauser.models.responseModels.countDetailsResponse.CountDetailsData;
import com.tradebeauser.models.responseModels.countDetailsResponse.CountDetailsResponse;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.GetProfileData;
import com.tradebeauser.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeauser.models.responseModels.profileResponse.UserDetails;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.StringUtils;
import com.tradebeauser.utils.UserData;

import java.util.LinkedList;
import java.util.List;

import ru.nikartm.support.ImageBadgeView;


public class HomeActivity extends BaseActivity implements View.OnClickListener, NavigationMenuAdapter.ItemClickListener, HttpReqResCallBack {

    private TextView tvTitle;
    private Fragment fragment;
    private RecyclerView rvMenu;
    private ImageView ivMenuNav;
    private DrawerLayout drawerLayout;
    private ImageBadgeView ivNotification, ivCart;

    private LinkedList<NavigationMenuItems> listOfNavigationMenuItems;

    private String token = "";
    private String userName = "";
    private String userUniqueId = "";
    private String userImageUrl = "";

    private Long mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);
        initializeUi();
        initializeListeners();
        getUserProfile();
        prepareCartCountDetails();
    }

    private void initializeUi() {
        rvMenu = findViewById(R.id.rvMenu);
        ivCart = findViewById(R.id.ivCart);
        tvTitle = findViewById(R.id.tvTitle);
        ivMenuNav = findViewById(R.id.ivMenuNav);
        drawerLayout = findViewById(R.id.drawerLayout);
        ivNotification = findViewById(R.id.ivNotification);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        PreferenceConnector.writeBoolean(this, getString(R.string.refresh_profile), false);
    }


    private void initializeListeners() {
        ivMenuNav.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
    }

    private void prepareDetails() {
        listOfNavigationMenuItems = new LinkedList<>();
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_home, getResources().getString(R.string.home)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_category, getResources().getString(R.string.shop_by_category)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_orders, getResources().getString(R.string.your_cart)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_products, getResources().getString(R.string.my_orders)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_my_wallet, getString(R.string.wallet)));
        //listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_place_24px, getResources().getString(R.string.your_addresses)));
        listOfNavigationMenuItems.add(new NavigationMenuItems(R.drawable.ic_logout, getResources().getString(R.string.logout)));
        initializeAdapter();
        onItemClick(null, 1);
    }

    private void initializeAdapter() {
        NavigationMenuAdapter navigationMenuAdapter = new NavigationMenuAdapter(HomeActivity.this, listOfNavigationMenuItems, userName, userImageUrl, userUniqueId, mobileNumber);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvMenu.setLayoutManager(layoutManager);
        rvMenu.setItemAnimator(new DefaultItemAnimator());
        navigationMenuAdapter.setClickListener(this);
        rvMenu.setAdapter(navigationMenuAdapter);
    }

    private void getUserProfile() {
        showProgressBar(this);
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(this, null, null, token);
    }

    private void prepareCartCountDetails() {
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        new Thread(new Runnable() {
            @Override
            public void run() {
                CountDetailsApiCall.serviceCallForCountDetails(HomeActivity.this, null, null, token, -1, "", "");
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean refreshProfile = PreferenceConnector.readBoolean(this, getString(R.string.refresh_profile), false);
        if (refreshProfile) {
            getUserProfile();
            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_profile), false);
        }
    }

    @SuppressLint("NonConstantResourceId")
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivMenuNav:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else
                    drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.ivCart:
                Intent cartIntent = new Intent(this, CartActivity.class);
                startActivity(cartIntent);
                break;
            case R.id.ivNotification:
                goToNotifications();
                break;
            default:
                break;

        }
    }

    private void goToNotifications() {
        Intent notificationsIntent = new Intent(this, NotificationsActivity.class);
        startActivity(notificationsIntent);
    }

    @Override
    public void onBackPressed() {
        exitMaterialAlert();
    }

    private void exitMaterialAlert() {
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name));
        alertDialogBuilder.setMessage(getResources().getString(R.string.are_you_sure_do_you_want_to_exit));
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position != 0) {
            NavigationMenuItems navigationMenuItems = listOfNavigationMenuItems.get(position - 1);
            String menuTitle = navigationMenuItems.getMenuItemName();
            if (menuTitle.equalsIgnoreCase(getString(R.string.home))) {
                tvTitle.setText(menuTitle);
                fragment = new HomeFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.shop_by_category))) {
                tvTitle.setText(menuTitle);
                fragment = new ShopByCategoryFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.your_cart))) {
                tvTitle.setText(menuTitle);
                fragment = new YourCartFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.my_orders))) {
                tvTitle.setText(menuTitle);
                fragment = new MyOrdersFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.your_addresses))) {
                tvTitle.setText(menuTitle);
                fragment = new YourAddressesFragment();
            } else if (menuTitle.equalsIgnoreCase(getString(R.string.wallet))) {
                tvTitle.setText(menuTitle);
                fragment = new WalletFragment();
            } else {
                showLogoutAlertDialog();
            }
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.detailsFragment, fragment);
                fragmentTransaction.setTransitionStyle(R.style.Theme_Transparent);
                fragmentTransaction.commitAllowingStateLoss();
            }

            if (view != null) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        } else {
            goToEditProfile();
        }
    }

    private void goToEditProfile() {
        Intent editProfileIntent = new Intent(this, EditProfileDetailsActivity.class);
        startActivity(editProfileIntent);
    }

    private void showLogoutAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(getString(R.string.are_you_sure_do_you_want_to_logout));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearDataInPreferences();
                goToLogin();
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void goToLogin() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }

    private void clearDataInPreferences() {
        PreferenceConnector.clearData(this);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                    if (profileResponse != null) {
                        boolean status = profileResponse.getStatus();
                        String message = profileResponse.getMessage();
                        if (status) {
                            GetProfileData getProfileData = profileResponse.getGetProfileData();
                            if (getProfileData != null) {
                                UserDetails userDetails = getProfileData.getUserDetails();
                                List<AddressDetails> listOfActiveAddress = getProfileData.getListOfActiveAddresses();
                                if (userDetails != null) {
                                    userName = StringUtils.toTitleCase(userDetails.getFirstName());
                                    userUniqueId = userDetails.getUniqueId();
                                    userImageUrl = userDetails.getProfileImage();
                                    mobileNumber = userDetails.getMobile();
                                }
                                if (listOfActiveAddress != null && listOfActiveAddress.size() != 0) {
                                    UserData.getInstance().setActiveLocation(listOfActiveAddress.get(0));
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                prepareDetails();
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_COUNT_DETAILS:
                if (jsonResponse != null) {
                    CountDetailsResponse countDetailsResponse = new Gson().fromJson(jsonResponse, CountDetailsResponse.class);
                    if (countDetailsResponse != null) {
                        boolean status = countDetailsResponse.getStatus();
                        if (status) {
                            CountDetailsData countDetailsData = countDetailsResponse.getCountDetailsData();
                            if (countDetailsData != null) {
                                int cartCount = countDetailsData.getCartCount();
                                int notificationCount = countDetailsData.getNotificationsCount();
                                ivNotification.setBadgeValue(notificationCount);
                                ivCart.setBadgeValue(cartCount);
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}
