package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.adapters.MenuDetailsAdapter;
import com.tradebeauser.adapters.ProductImagesAdapter;
import com.tradebeauser.models.responseModels.productDescriptionResponse.ProductImageDetails;

import java.util.List;
import java.util.Objects;

public class ProductImagesFullScreenActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView rvImages;
    private ImageView ivProductImage,ivBack;
    private List<ProductImageDetails> listOfProductImagesDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_prodcut_image_full_screen);
        getIntentData();
        initializeUI();
        initializeListeners();
        initializeAdapter();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.images))) {
                listOfProductImagesDetails = (List<ProductImageDetails>) bundle.getSerializable(getString(R.string.images));
            }
        }
    }

    private void initializeUI() {
        ivBack = findViewById(R.id.ivBack);
        rvImages = findViewById(R.id.rvImages);
        ivProductImage = findViewById(R.id.ivProductImage);
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
    }

    private void initializeAdapter() {
        if(listOfProductImagesDetails!=null && !listOfProductImagesDetails.isEmpty()){
            ProductImagesAdapter productImagesAdapter = new ProductImagesAdapter(this, listOfProductImagesDetails,ivProductImage);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
            rvImages.setLayoutManager(layoutManager);
            rvImages.setItemAnimator(new DefaultItemAnimator());
            rvImages.setAdapter(productImagesAdapter);
            rvImages.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    if(rvImages.findViewHolderForAdapterPosition(0)!=null )
                    {
                        Objects.requireNonNull(rvImages.findViewHolderForAdapterPosition(0)).itemView.performClick();
                    }
                }
            },50);
        }
    }

    @Override
    public void onClick(View view) {
        onBackPressed();
    }
}
