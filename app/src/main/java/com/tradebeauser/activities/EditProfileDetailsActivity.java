package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.GetProfileDetailsApiCall;
import com.tradebeauser.ApiCalls.UpdateProfileDetails;
import com.tradebeauser.R;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.profileResponse.GetProfileData;
import com.tradebeauser.models.responseModels.profileResponse.ProfileResponse;
import com.tradebeauser.models.responseModels.profileResponse.UserDetails;
import com.tradebeauser.models.responseModels.updateProfileDetailsResponse.UpdateProfileDetailsResponse;
import com.tradebeauser.utils.AppPermissions;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class EditProfileDetailsActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, HttpReqResCallBack {

    private RadioGroup radioGroup;
    private TextView tvSaveChanges;
    private LinearLayout llSaveChanges;
    private ImageView ivBackArrow, ivProfilePic;
    private RadioButton rbMale, rbFemale, rbOthers;
    private BottomSheetDialog pickerBottomSheetDialog;
    private EditText etFirstName, etLastName, etEmailId, etMobile;

    private ArrayList<String> listOfPhotoPaths;

    private String token = "";
    private String selectedGender = "";
    private String selectedProfileImageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_profile_details);
        initializeUi();
        initializeListeners();
        prepareProfileDetails();
    }

    private void initializeUi() {
        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        rbOthers = findViewById(R.id.rbOthers);
        etMobile = findViewById(R.id.etMobile);
        etEmailId = findViewById(R.id.etEmailId);
        etLastName = findViewById(R.id.etLastName);
        radioGroup = findViewById(R.id.radioGroup);
        etFirstName = findViewById(R.id.etFirstName);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        tvSaveChanges = findViewById(R.id.tvSaveChanges);
        llSaveChanges = findViewById(R.id.llSaveChanges);

        listOfPhotoPaths = new ArrayList<>();
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
        ivProfilePic.setOnClickListener(this);
        tvSaveChanges.setOnClickListener(this);
        llSaveChanges.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
    }

    private void prepareProfileDetails() {
        showProgressBar(this);
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetProfileDetailsApiCall.serviceCallToGetProfileDetails(this, null, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivProfilePic:
                prepareImageDetails();
                break;
            case R.id.tvImagePicker:
                listOfPhotoPaths = new ArrayList<>();
                onPickPhoto();
                closeBottomSheetView();
                break;
            case R.id.tvCamera:
                listOfPhotoPaths = new ArrayList<>();
                captureImage();
                closeBottomSheetView();
                break;
            case R.id.tvCancel:
                closeBottomSheetView();
                break;
            case R.id.llSaveChanges:
            case R.id.tvSaveChanges:
                prepareSaveChanges();
                break;
            default:
                break;
        }
    }

    private void prepareImageDetails() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                showBottomSheetView();
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }


    @SuppressLint("InflateParams")
    private void showBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_picker_sheet, null);

        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvImagePicker = view.findViewById(R.id.tvImagePicker);

        tvCancel.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvImagePicker.setOnClickListener(this);

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();
    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    private void onPickPhoto() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(new ArrayList<>())
                .setActivityTheme(R.style.FilePickerTheme)
                .setActivityTitle(getString(R.string.select_image))
                .enableVideoPicker(false)
                .enableCameraSupport(true)
                .showGifs(true)
                .showFolderView(true)
                .enableSelectAll(false)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.image_placeholder)
                .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .pickPhoto(this, Constants.PICK_GALLERY);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    private void prepareSaveChanges() {
        showProgressBar(this);
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String emailId = etEmailId.getText().toString();
        String mobile = etMobile.getText().toString();
        String profileImageBase64 = base64Converter(ivProfilePic);

        if (!firstName.isEmpty()) {
            if (!lastName.isEmpty()) {
                if (!emailId.isEmpty()) {
                    if (!mobile.isEmpty()) {
                        if (selectedGender != null) {
                            if (!selectedGender.isEmpty()) {
                                if (!profileImageBase64.isEmpty()) {
                                    UpdateProfileDetails.serviceCallForUpdateProfileDetails(this, firstName, lastName, emailId, mobile, selectedGender, profileImageBase64, token);
                                } else {
                                    closeProgressbar();
                                    Toast.makeText(this, getString(R.string.please_select_profile_image), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                closeProgressbar();
                                Toast.makeText(this, getString(R.string.please_select_gender), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            closeProgressbar();
                            Toast.makeText(this, getString(R.string.please_select_gender), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        closeProgressbar();
                        Toast.makeText(this, getString(R.string.please_enter_mobile_number), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    closeProgressbar();
                    Toast.makeText(this, getString(R.string.please_enter_email_id), Toast.LENGTH_SHORT).show();
                }
            } else {
                closeProgressbar();
                Toast.makeText(this, getString(R.string.please_enter_last_name), Toast.LENGTH_SHORT).show();
            }
        } else {
            closeProgressbar();
            Toast.makeText(this, getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT).show();
        }
    }

    private String base64Converter(ImageView imageView) {
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, 0);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        selectedGender = radioButton.getText().toString();
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_PROFILE_DETAILS:
                if (jsonResponse != null) {
                    ProfileResponse profileResponse = new Gson().fromJson(jsonResponse, ProfileResponse.class);
                    if (profileResponse != null) {
                        boolean status = profileResponse.getStatus();
                        String message = profileResponse.getMessage();
                        if (status) {
                            GetProfileData getProfileData = profileResponse.getGetProfileData();
                            if (getProfileData != null) {
                                UserDetails userDetails = getProfileData.getUserDetails();
                                if (userDetails != null) {
                                    prepareUserDetails(userDetails);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_UPDATE_PROFILE:
                if (jsonResponse != null) {
                    UpdateProfileDetailsResponse updateProfileDetailsResponse = new Gson().fromJson(jsonResponse, UpdateProfileDetailsResponse.class);
                    if (updateProfileDetailsResponse != null) {
                        boolean status = updateProfileDetailsResponse.getStatus();
                        String message = updateProfileDetailsResponse.getMessage();
                        if (status) {
                            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_profile), true);
                            finish();
                        }
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void prepareUserDetails(UserDetails userDetails) {
        String name = userDetails.getFirstName();
        String lastName = userDetails.getLastName();
        Long mobile = userDetails.getMobile();
        String emailId = userDetails.getEmail();
        selectedGender = userDetails.getGender();
        String profileImageUrl = userDetails.getProfileImage();

        if (name != null) {
            if (!name.isEmpty()) {
                etFirstName.setText(name);
            }
        }

        if (lastName != null) {
            if (!lastName.isEmpty()) {
                etLastName.setText(lastName);
            }
        }

        if (mobile != null) {
            etMobile.setText(String.valueOf(mobile));
        }

        if (emailId != null) {
            if (!emailId.isEmpty()) {
                etEmailId.setText(emailId);
            }
        }

        if (profileImageUrl != null) {
            if (!profileImageUrl.isEmpty()) {
                Glide.with(this)
                        .load(profileImageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .placeholder(R.drawable.ic_user_image)
                        .error(R.drawable.ic_user_image)
                        .into(ivProfilePic);
            }
        }

        if (selectedGender != null) {
            if (selectedGender.equalsIgnoreCase(getString(R.string.male))) {
                rbMale.setChecked(true);
            } else if (selectedGender.equalsIgnoreCase(getString(R.string.female))) {
                rbFemale.setChecked(true);
            } else if (selectedGender.equalsIgnoreCase(getString(R.string.others))) {
                rbOthers.setChecked(true);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        switch (requestCode) {
            case Constants.PICK_GALLERY:
                listOfPhotoPaths = new ArrayList<>();
                if (resultIntent != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        listOfPhotoPaths.addAll(resultIntent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                        if (listOfPhotoPaths != null) {
                            if (listOfPhotoPaths.size() != 0) {
                                String selectedImageUrl = listOfPhotoPaths.get(0);

                                selectedProfileImageUrl = selectedImageUrl;
                                Glide.with(this)
                                        .load(new File(selectedProfileImageUrl))
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .placeholder(R.drawable.image_placeholder)
                                        .error(R.drawable.image_placeholder)
                                        .into(ivProfilePic);

                            }
                        }
                    }
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE_IMAGE:
                listOfPhotoPaths = new ArrayList<>();
                List<Image> listOfImages = ImagePicker.getImages(resultIntent);
                if (listOfImages != null) {
                    if (listOfImages.size() != 0) {
                        String selectedImageUrl = listOfImages.get(0).getPath();
                        listOfPhotoPaths.add(selectedImageUrl);

                        selectedProfileImageUrl = selectedImageUrl;
                        Glide.with(this)
                                .load(new File(selectedProfileImageUrl))
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.image_placeholder)
                                .error(R.drawable.image_placeholder)
                                .into(ivProfilePic);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
