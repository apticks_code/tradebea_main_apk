package com.tradebeauser.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.GetSavedAddressesApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.SavedAddressDetailsAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.interfaces.LocationUpdateCallBack;
import com.tradebeauser.models.responseModels.SavedAddressDetailsResponse.SavedAddressDetailsResponse;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.utils.AppPermissions;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.GetCurrentLocation;
import com.tradebeauser.utils.PreferenceConnector;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ChooseLocationActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, LocationUpdateCallBack {

    private LatLng latLng;
    private ImageView ivBack;
    private TextView tvError, tvSearch;
    private RecyclerView rvSavedAddresses;
    private LinearLayout llCurrentLocation;
    private ShimmerFrameLayout savedAddressShimmerViewContainer;

    private List<AddressDetails> listOfActiveAddress;

    private String token = "";
    private String latitude = "";
    private String longitude = "";

    private int activeLocationID = -1;
    private int AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_choose_location_activity);
        getIntentData();
        initializeUI();
        initializeListeners();
        prepareSavedAddresses();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.active_location_id))) {
                activeLocationID = bundle.getInt(getString(R.string.active_location_id));
            }
        }
    }

    private void initializeUI() {
        ivBack = findViewById(R.id.ivBack);
        tvError = findViewById(R.id.tvError);
        tvSearch = findViewById(R.id.tvSearch);
        rvSavedAddresses = findViewById(R.id.rvSavedAddresses);
        llCurrentLocation = findViewById(R.id.llCurrentLocation);
        savedAddressShimmerViewContainer = findViewById(R.id.savedAddressShimmerViewContainer);

        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        tvSearch.setOnClickListener(this);
        llCurrentLocation.setOnClickListener(this);
    }

    private void prepareSavedAddresses() {
        savedAddressShimmerViewContainer.startShimmer();
        GetSavedAddressesApiCall.serviceCallToGetSavedAddresses(this, null, null, token);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean refreshAddress = PreferenceConnector.readBoolean(this, getString(R.string.refresh_saved_address), false);
        if (refreshAddress) {
            prepareSavedAddresses();
            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_saved_address), false);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.tvSearch:
                showPlacesAutoComplete();
                break;
            case R.id.llCurrentLocation:
                checkLocationAccessPermission();
                break;
            default:
                break;
        }
    }

    private void checkLocationAccessPermission() {
        if (AppPermissions.checkPermissionForAccessLocation(this)) {
            showProgressBar(this);
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            new GetCurrentLocation(this, null, locationManager);
        } else {
            AppPermissions.requestPermissionForLocation(this);
        }
    }

    @Override
    public void OnLatLongReceived(double latitude, double longitude) {
        closeProgressbar();
        latLng = new LatLng(latitude, longitude);
        getCurrentLocation();
    }

    private void getCurrentLocation() {
        String errorMessage = "";
        latitude = String.valueOf(latLng.latitude);
        longitude = String.valueOf(latLng.longitude);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = getString(R.string.service_not_available);
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = getString(R.string.invalid_lat_long_used);
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.no_address_found);
                Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
            }

        } else {
            Address address = addresses.get(0);
            StringBuilder builder = new StringBuilder();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                builder.append(address.getAddressLine(i));
            }
           /* String city = "";
            String state = "";
            String country = "";
            String currentLocation = "";
            if(address.getLocality() != null){
                city = address.getLocality();
            }
            if(address.getAdminArea() != null){
                state = address.getAdminArea();
            }
            if(address.getCountryName() != null){
                country = address.getCountryName();
            }

            tvLocation.setText(String.format("%s,%s", addresses.get(0).getSubLocality(), addresses.get(0).getLocality()));
            if(!city.isEmpty() && !state.isEmpty() && !country.isEmpty()){
                currentLocation = city + "," + state + "," + country;
                setContentView().setText(currentLocation);
            }else {
                if(address.getLocality() != null)
                    currentLocation = addresses.get(0).getLocality();
                tvLocation.setText(currentLocation);
            }*/
            tvSearch.setText(builder.toString());
            closeProgressbar();
            goToSaveAddress(builder.toString());
        }
    }

    private void goToSaveAddress(String address) {
        Intent intent = new Intent(this, AddAddressActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.latitude), latitude);
        bundle.putString(getString(R.string.longitude), longitude);
        bundle.putString(getString(R.string.address), address);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    private void showPlacesAutoComplete() {
        Places.initialize(getApplicationContext(), getString(R.string.places_api_key));

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = Autocomplete.getPlaceFromIntent(data);
                latitude = String.valueOf(Objects.requireNonNull(place.getLatLng()).latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
                tvSearch.setText(place.getAddress());
                goToSaveAddress(tvSearch.getText().toString());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                String status = Autocomplete.getStatusFromIntent(data).getStatusMessage();
                Log.d("asd", status);
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("asd", "asd");
            }
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SAVED_ADDRESSES:
                if (jsonResponse != null) {
                    try {
                        SavedAddressDetailsResponse savedAddressDetailsResponse = new Gson().fromJson(jsonResponse, SavedAddressDetailsResponse.class);
                        if (savedAddressDetailsResponse != null) {
                            boolean status = savedAddressDetailsResponse.getStatus();
                            String message = savedAddressDetailsResponse.getMessage();
                            if (status) {
                                listOfActiveAddress = savedAddressDetailsResponse.getListOfSavedAddressDetails();
                                if (listOfActiveAddress != null && listOfActiveAddress.size() != 0) {
                                    initializeSavedAddressesAdapter();
                                } else {
                                    savedAddressShimmerViewContainer.stopShimmer();
                                    savedAddressShimmerViewContainer.setVisibility(View.GONE);
                                    rvSavedAddresses.setVisibility(View.GONE);
                                    tvError.setVisibility(View.VISIBLE);
                                }
                            } else {
                                savedAddressShimmerViewContainer.startShimmer();
                                savedAddressShimmerViewContainer.setVisibility(View.VISIBLE);
                                rvSavedAddresses.setVisibility(View.GONE);
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            savedAddressShimmerViewContainer.startShimmer();
                            savedAddressShimmerViewContainer.setVisibility(View.VISIBLE);
                            rvSavedAddresses.setVisibility(View.GONE);
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        savedAddressShimmerViewContainer.stopShimmer();
                        savedAddressShimmerViewContainer.setVisibility(View.VISIBLE);
                        rvSavedAddresses.setVisibility(View.GONE);
                    }
                }
                break;
        }
    }

    private void initializeSavedAddressesAdapter() {
        SavedAddressDetailsAdapter savedAddressDetailsAdapter = new SavedAddressDetailsAdapter(this, listOfActiveAddress, token, activeLocationID);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvSavedAddresses.setLayoutManager(layoutManager);
        rvSavedAddresses.setItemAnimator(new DefaultItemAnimator());
        rvSavedAddresses.setAdapter(savedAddressDetailsAdapter);

        savedAddressShimmerViewContainer.stopShimmer();
        savedAddressShimmerViewContainer.setVisibility(View.GONE);
        rvSavedAddresses.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);
    }
}
