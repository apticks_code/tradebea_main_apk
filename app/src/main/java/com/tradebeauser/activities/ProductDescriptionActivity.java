package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tradebeauser.ApiCalls.AddCartApiCall;
import com.tradebeauser.ApiCalls.CheckoutProductsApiCall;
import com.tradebeauser.ApiCalls.GetProductDescriptionDetailsApiCall;
import com.tradebeauser.ApiCalls.RelatedProductsSearchApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.ProductOptionValueChipsAdapter;
import com.tradebeauser.adapters.RelatedProductsListAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.AddOrUpdateOrDeleteResponse.AddOrUpdateOrDeleteResponse;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.CheckoutProductDetailsResponse;
import com.tradebeauser.models.responseModels.productDescriptionResponse.OptionDetails;
import com.tradebeauser.models.responseModels.productDescriptionResponse.ProductDescriptionResponse;
import com.tradebeauser.models.responseModels.productDescriptionResponse.ProductImageDetails;
import com.tradebeauser.models.responseModels.productDescriptionResponse.Values;
import com.tradebeauser.models.responseModels.productDescriptionResponse.VariantDetails;
import com.tradebeauser.models.responseModels.productDescriptionResponse.VariantValues;
import com.tradebeauser.models.responseModels.productDetailsResponse.ProductDetailsResponse;
import com.tradebeauser.models.responseModels.productDetailsResponse.ResultDetails;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.LocationInfo;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.StringUtils;
import com.tradebeauser.utils.UserData;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ProductDescriptionActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener {

    private RecyclerView rvRelatedProducts;
    private LinearLayout llRelatedProducts;
    private VariantDetails selectedVariantDetails;
    private ImageView ivBack, ivCart, ivProductImage;
    private ProductDescriptionResponse productDescriptionResponse;
    private LinearLayout llProductOptions, llImagePreview, llAddCart, llBuyNow, llProductDetails;
    private TextView tvName, tvPrice, tvQuantity, tvPriceAfterDiscount, tvOriginalPrice, tvDiscount, tvDescription;
    private com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data checkoutData;

    private JsonObject checkoutRequestObject;

    private String menu = "";
    private String brand = "";
    private String filter = "";
    private String latitude = "";
    private String category = "";
    private String longitude = "";
    private String productID = "";
    private String subCategory = "";
    private String productPrice = "";
    private String subSubCategory = "";
    private String productQuantity = "";
    private String productDiscount = "";
    private String selectedVariantID = "";

    private int selectedQuantity = 1;


    private List<OptionDetails> listOfOptionDetails;
    private List<ResultDetails> listOfProductDetails;
    private List<VariantDetails> listOfVariantDetails;
    private Map<String, String> mapOfLabelIDSelectedOptionID;
    private Map<String, String> mapOfProductDetailsTitleValue;
    private Map<String, RecyclerView> mapOfOptionLabelAndViews;
    private List<ProductImageDetails> listOfProductImagesDetails;
    private Map<String, List<VariantValues>> mapOfVariantIDValues;
    private Map<String, VariantDetails> mapOfVariantIDVariantDetails;
    private Map<String, List<String>> mapOfVariantIDItemIdCombinations;
    private Map<String, List<String>> mapOfTempVariantIDItemIdCombinations;


    private String token = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_product_description_activity);
        getIntentData();
        initializeUI();
        initializeListeners();
        prepareProductDescription();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.product_id))) {
                productID = bundle.getString(getString(R.string.product_id));
            }
            if (bundle.containsKey(getString(R.string.product_price))) {
                productPrice = bundle.getString(getString(R.string.product_price));
            }
            if (bundle.containsKey(getString(R.string.product_discount))) {
                productDiscount = bundle.getString(getString(R.string.product_discount));
            }
            if (bundle.containsKey(getString(R.string.longitude))) {
                longitude = bundle.getString(getString(R.string.longitude));
            }
            if (bundle.containsKey(getString(R.string.latitude))) {
                latitude = bundle.getString(getString(R.string.latitude));
            }
        }
        if (latitude != null && longitude != null) {
            if (latitude.isEmpty() && longitude.isEmpty())
                PrepareLocationDetails();
        } else {
            PrepareLocationDetails();
        }
        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }

    private void PrepareLocationDetails() {
        AddressDetails addressDetails = UserData.getInstance().getActiveLocation();
        if (addressDetails != null) {
            LocationInfo locationInfo = addressDetails.getLocation();
            latitude = String.valueOf(addressDetails.getLocation().getLatitude());
            longitude = String.valueOf(addressDetails.getLocation().getLongitude());
        }
    }

    private void initializeUI() {
        tvName = findViewById(R.id.tvName);
        ivBack = findViewById(R.id.ivBack);
        ivCart = findViewById(R.id.ivCart);
        tvPrice = findViewById(R.id.tvPrice);
        llBuyNow = findViewById(R.id.llBuyNow);
        llAddCart = findViewById(R.id.llAddCart);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvQuantity = findViewById(R.id.tvQuantity);
        tvDescription = findViewById(R.id.tvDescription);
        ivProductImage = findViewById(R.id.ivProductImage);
        llImagePreview = findViewById(R.id.llImagePreview);
        tvOriginalPrice = findViewById(R.id.tvOriginalPrice);
        llProductOptions = findViewById(R.id.llProductOptions);
        llProductDetails = findViewById(R.id.llProductDetails);
        llRelatedProducts = findViewById(R.id.llRelatedProducts);
        rvRelatedProducts = findViewById(R.id.rvRelatedProducts);
        tvPriceAfterDiscount = findViewById(R.id.tvPriceAfterDiscount);


        mapOfVariantIDValues = new LinkedHashMap<>();
        mapOfOptionLabelAndViews = new LinkedHashMap<>();
        mapOfLabelIDSelectedOptionID = new LinkedHashMap<>();
        mapOfVariantIDVariantDetails = new LinkedHashMap<>();
        mapOfProductDetailsTitleValue = new LinkedHashMap<>();
        mapOfVariantIDItemIdCombinations = new LinkedHashMap<>();
        mapOfTempVariantIDItemIdCombinations = new LinkedHashMap<>();
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        ivCart.setOnClickListener(this);
        llBuyNow.setOnClickListener(this);
        llAddCart.setOnClickListener(this);
        tvQuantity.setOnClickListener(this);
    }

    private void prepareProductDescription() {
        showProgressBar(this);
        GetProductDescriptionDetailsApiCall.serviceCallToProductDescription(this, null, null, productID, latitude, longitude);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceConnector.readBoolean(this, getString(R.string.isQuantityUpdated), false)) {
            PreferenceConnector.writeBoolean(this, getString(R.string.isQuantityUpdated), false);
            selectedQuantity = UserData.getInstance().getQuantity();
            tvQuantity.setText(String.format("Qty : %s", selectedQuantity));
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_ADD_PRODUCT_TO_CART:
                if (jsonResponse != null) {
                    AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                    if (addOrUpdateOrDeleteResponse != null) {
                        boolean status = addOrUpdateOrDeleteResponse.getStatus();
                        String message = addOrUpdateOrDeleteResponse.getMessage();
                        if (status) {
                            Toast.makeText(this, "Added to cart.", Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_PRODUCT_DETAILS:
                if (jsonResponse != null) {
                    try {
                        productDescriptionResponse = new Gson().fromJson(jsonResponse, ProductDescriptionResponse.class);
                        if (productDescriptionResponse != null) {
                            boolean status = productDescriptionResponse.getStatus();
                            String message = productDescriptionResponse.getMessage();

                            if (status) {
                                listOfOptionDetails = productDescriptionResponse.getData().getListOfOptionDetails();
                                listOfVariantDetails = productDescriptionResponse.getData().getListOfVariantDetails();
                                listOfProductImagesDetails = productDescriptionResponse.getData().getListOfProductImageDetails();
                                brand = StringUtils.toTitleCase(productDescriptionResponse.getData().getBrandDetails().getName());
                                menu = StringUtils.toTitleCase(productDescriptionResponse.getData().getMenuDetails().getName());
                                category = StringUtils.toTitleCase(productDescriptionResponse.getData().getCategoryDetails().getName());
                                subCategory = StringUtils.toTitleCase(productDescriptionResponse.getData().getSubCategoryDetails().getName());
                                subSubCategory = StringUtils.toTitleCase(productDescriptionResponse.getData().getSubSubCategoryDetails().getName());
                                initializeDetailsView("Brand", brand);
                                initializeDetailsView("Menu", menu);
                                initializeDetailsView("Category", category);
                                initializeDetailsView("Sub Category", subCategory);
                                initializeDetailsView("Sub Sub Category", subSubCategory);
                                preparePriceDetails();
                                prepareDetails();
                                prepareRelatedItems();

                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(this, getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_RELATED_PRODUCTS:
                if (jsonResponse != null) {
                    try {
                        ProductDetailsResponse productDetailsResponse = new Gson().fromJson(jsonResponse, ProductDetailsResponse.class);
                        if (productDetailsResponse != null) {
                            boolean status = productDetailsResponse.getStatus();
                            String message = productDetailsResponse.getMessage();
                            if (status) {
                                listOfProductDetails = productDetailsResponse.getData().getListOfResults();
                                if (listOfProductDetails != null) {
                                    if (listOfProductDetails.size() != 0) {
                                        llRelatedProducts.setVisibility(View.VISIBLE);
                                        initializeRelatedProductsAdapter();
                                    } else {
                                        llRelatedProducts.setVisibility(View.GONE);
                                    }
                                } else {
                                    llRelatedProducts.setVisibility(View.GONE);
                                }
                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(this, getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_CHECKOUT_PRODUCTS:
                if (jsonResponse != null) {
                    try {
                        CheckoutProductDetailsResponse checkoutProductDetailsResponse = new Gson().fromJson(jsonResponse, CheckoutProductDetailsResponse.class);
                        if (checkoutProductDetailsResponse != null) {
                            boolean status = checkoutProductDetailsResponse.getStatus();
                            String message = checkoutProductDetailsResponse.getMessage();
                            if (status) {
                                checkoutData = checkoutProductDetailsResponse.getData();
                                UserData.getInstance().setCheckoutData(checkoutData);
                                gotToCheckout();
                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(this, getString(R.string.no_products_found), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    private void gotToCheckout() {
        Intent productCheckoutIntent = new Intent(this, ProductCheckoutActivity.class);
        startActivity(productCheckoutIntent);
    }

    private void initializeRelatedProductsAdapter() {
        RelatedProductsListAdapter productsListAdapter = new RelatedProductsListAdapter(this, listOfProductDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvRelatedProducts.setLayoutManager(layoutManager);
        rvRelatedProducts.setItemAnimator(new DefaultItemAnimator());
        rvRelatedProducts.setAdapter(productsListAdapter);
    }

    private void prepareRelatedItems() {
        String filterKey = "";
        String filterValue = "";

        if (subSubCategory != null && !subSubCategory.isEmpty()) {
            filterKey = "sub_sub_cat_id";
            filterValue = subSubCategory;
        } else if (subCategory != null && !subCategory.isEmpty()) {
            filterKey = "sub_cat_id";
            filterValue = subCategory;
        } else if (category != null && !category.isEmpty()) {
            filterKey = "cat_id";
            filterValue = category;
        } else if (menu != null && !menu.isEmpty()) {
            filterKey = "menu_id";
            filterValue = subSubCategory;
        } else if (brand != null && !brand.isEmpty()) {
            filterKey = "brand_id";
            filterValue = subSubCategory;
        }
        showProgressBar(this);
        RelatedProductsSearchApiCall.serviceCallToGetRelatedProductsList(this, null, null, latitude, longitude, "10", "0", filterValue, filterKey, "ASC");
    }

    private void initializeDetailsView(String Title, String name) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_product_details_container_item, llImagePreview, false);
        TextView tvKey = view.findViewById(R.id.tvKey);
        TextView tvValue = view.findViewById(R.id.tvValue);
        tvKey.setText(Title);
        if (name != null && !name.isEmpty()) {
            tvValue.setText(name);
        } else {
            tvValue.setText("N/A");
        }
        llProductDetails.addView(view);
    }

    private void prepareDetails() {
        tvName.setText(StringUtils.toTitleCase(productDescriptionResponse.getData().getName()));
        tvDescription.setText(StringUtils.toFirstLetterTitleCase(productDescriptionResponse.getData().getDescription()));
        if (listOfProductImagesDetails != null && listOfProductImagesDetails.size() != 0) {
            int size = listOfProductImagesDetails.size();
            for (int index = 0; index < size; index++) {
                ProductImageDetails productImageDetails = listOfProductImagesDetails.get(index);
                String image = productImageDetails.getImage();
                if (index == 0) {
                    Glide.with(this)
                            .load(image)
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .into(ivProductImage);
                }
                if (index < 3) {
                    initializeImageView(index, image);
                }
            }
            if (size > 3) {
                initializeImagesCountView(listOfProductImagesDetails.size() - 3);
            }
        }
        ivProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductDescriptionActivity.this, ProductImagesFullScreenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(getString(R.string.images), (Serializable) listOfProductImagesDetails);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        prepareOptions();
    }

    @SuppressLint("DefaultLocale")
    private void initializeImagesCountView(int count) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_count_container_item, llImagePreview, false);
        TextView tvCount = view.findViewById(R.id.tvCount);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductDescriptionActivity.this, ProductImagesFullScreenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(getString(R.string.images), (Serializable) listOfProductImagesDetails);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        tvCount.setText(String.format("%d+", count));
        llImagePreview.addView(view);

    }

    private void initializeImageView(int index, String image) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_container_item, null);
        ImageView ivImage = view.findViewById(R.id.ivImage);
        Glide.with(this)
                .load(image)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .into(ivImage);
        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Glide.with(ProductDescriptionActivity.this)
                        .load(listOfProductImagesDetails.get(index).getImage())
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .into(ivProductImage);
            }
        });
        llImagePreview.addView(view);
    }

    private void prepareOptions() {
        for (int index = 0; index < listOfOptionDetails.size(); index++) {
            OptionDetails optionDetails = listOfOptionDetails.get(index);
            String optionID = optionDetails.getLabelDetails().getId();
            String optionName = optionDetails.getLabelDetails().getName();
            List<Values> listOfOptionValues = optionDetails.getListOfValuesDetails();
            initializeView(index, optionID, optionName, listOfOptionValues);
        }
        if (listOfVariantDetails != null && listOfVariantDetails.size() != 0) {
            for (int variantIndex = 0; variantIndex < listOfVariantDetails.size(); variantIndex++) {
                String variantId = listOfVariantDetails.get(variantIndex).getId();
                mapOfVariantIDVariantDetails.put(variantId, listOfVariantDetails.get(variantIndex));
                List<VariantValues> listOfVariantValues = listOfVariantDetails.get(variantIndex).getListOfVariantValues();
                mapOfVariantIDValues.put(variantId, listOfVariantValues);
                List<String> listOfOptionItemIDs = new LinkedList<>();
                if (listOfVariantValues != null && listOfVariantValues.size() != 0) {
                    for (int variantValueIndex = 0; variantValueIndex < listOfVariantValues.size(); variantValueIndex++) {
                        String optionItemID = listOfVariantValues.get(variantValueIndex).getOptionItemId();
                        listOfOptionItemIDs.add(optionItemID);
                    }
                    mapOfVariantIDItemIdCombinations.put(variantId, listOfOptionItemIDs);

                }
            }
        }

        // mapOfTempVariantIDItemIdCombinations.putAll(mapOfVariantIDItemIdCombinations);
    }

    private void initializeView(int position, String optionID, String optionName, List<Values> listOfOptionValues) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_product_options_container_item, null);
        TextView tvName = view.findViewById(R.id.tvName);
        RecyclerView rvProductOptions = view.findViewById(R.id.rvProductOptions);
        mapOfOptionLabelAndViews.put(optionID, rvProductOptions);
        llProductOptions.addView(view);
        if (listOfOptionValues != null && listOfOptionValues.size() != 0) {
            tvName.setText(optionName);
            initializeOptionsAdapter(position, rvProductOptions, listOfOptionValues);
        }
    }

    private void initializeOptionsAdapter(int optionPosition, RecyclerView rvProductOptions, List<Values> listOfOptionValues) {
        ProductOptionValueChipsAdapter productOptionValueChipsAdapter = new ProductOptionValueChipsAdapter(this, listOfOptionValues, listOfOptionDetails.get(optionPosition).getLabelDetails().getId());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvProductOptions.setLayoutManager(layoutManager);
        rvProductOptions.setItemAnimator(new DefaultItemAnimator());
        rvProductOptions.setNestedScrollingEnabled(true);
        rvProductOptions.setAdapter(productOptionValueChipsAdapter);

        productOptionValueChipsAdapter.setClickListener(new ProductOptionValueChipsAdapter.ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (mapOfVariantIDItemIdCombinations != null && mapOfVariantIDItemIdCombinations.size() != 0) {
                    productOptionValueChipsAdapter.updateSelectedItem(position);
                }

                String labelIDOptionID = view.getTag().toString();
                String[] labelIDAndOptionID = labelIDOptionID.split("&&");
                String labelID = labelIDAndOptionID[0];
                String optionItemID = labelIDAndOptionID[1];
                mapOfLabelIDSelectedOptionID.put(labelID, optionItemID);
                mapOfTempVariantIDItemIdCombinations = new LinkedHashMap<>();
                if (mapOfLabelIDSelectedOptionID.size() == listOfOptionDetails.size()) {
                    if (mapOfVariantIDItemIdCombinations != null && mapOfVariantIDItemIdCombinations.size() != 0) {
                        List<String> listOfVariantIds = new LinkedList<>(mapOfVariantIDItemIdCombinations.keySet());
                        for (int index = 0; index < mapOfVariantIDItemIdCombinations.size(); index++) {
                            String variantID = listOfVariantIds.get(index);
                            List<String> listOfOptionItemIDsCombination = mapOfVariantIDItemIdCombinations.get(variantID);
                            if (listOfOptionItemIDsCombination != null && listOfOptionItemIDsCombination.size() != 0) {
                                int count = 0;
                                List<String> listOfSelectedOptionIDs = new LinkedList<>(mapOfLabelIDSelectedOptionID.values());
                                for (int labelIndex = 0; labelIndex < mapOfLabelIDSelectedOptionID.size(); labelIndex++) {
                                    if (listOfOptionItemIDsCombination.contains(listOfSelectedOptionIDs.get(labelIndex))) {
                                        count++;
                                    }
                                }
                                if (count == listOfSelectedOptionIDs.size()) {
                                    selectedVariantID = variantID;
                                    mapOfTempVariantIDItemIdCombinations.put(variantID, listOfOptionItemIDsCombination);
                                }
                            }
                        }
                    }
                }
                preparePriceDetails();
            }
        });
        rvProductOptions.setAdapter(productOptionValueChipsAdapter);
        rvProductOptions.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (rvProductOptions.findViewHolderForAdapterPosition(0) != null) {
                    Objects.requireNonNull(rvProductOptions.findViewHolderForAdapterPosition(0)).itemView.performClick();
                }
            }
        }, 50);
    }

    private void preparePriceDetails() {
        selectedVariantDetails = mapOfVariantIDVariantDetails.get(selectedVariantID);
        if (selectedVariantDetails != null) {
            String price = selectedVariantDetails.getMrp();
            String discount = selectedVariantDetails.getDiscount();
            productQuantity = selectedVariantDetails.getQuantity();
            Double salePrice = null;
            if (price != null && !price.isEmpty() && discount != null && !discount.isEmpty()) {
                salePrice = CommonMethods.getSalePrice(discount, price);
            }

            setPriceValues(price, discount, salePrice);

        } else {
            Double salePrice = null;
            if (productPrice != null && !productPrice.isEmpty() && productDiscount != null && !productDiscount.isEmpty()) {
                salePrice = CommonMethods.getSalePrice(productDiscount, productPrice);
            }
            setPriceValues(productPrice, productDiscount, salePrice);
        }
    }

    private void setPriceValues(String price, String discount, Double salePrice) {
        if (price != null && !price.isEmpty()) {
            if (!price.equalsIgnoreCase("0")) {
                tvOriginalPrice.setVisibility(View.VISIBLE);
                tvOriginalPrice.setText(String.format("%s%s", getString(R.string.rs), price));
            } else {
                tvOriginalPrice.setVisibility(View.GONE);
            }
        } else {
            tvOriginalPrice.setVisibility(View.GONE);
            tvOriginalPrice.setText("N/A");
        }

        if (discount != null && !discount.isEmpty()) {
            if (!discount.equalsIgnoreCase("0")) {
                tvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(String.format("%s%% off", discount));
            } else {
                tvDiscount.setVisibility(View.GONE);
            }
        } else {
            tvDiscount.setVisibility(View.GONE);
            tvDiscount.setText("N/A");
        }

        if (salePrice != null) {
            tvPriceAfterDiscount.setText(String.format("%s%s", getString(R.string.rs), String.valueOf(salePrice)));
        } else {
            tvPriceAfterDiscount.setText("N/A");
        }
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.tvQuantity:
                Intent intent = new Intent(this, QuantityPopupActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.quantity), productQuantity);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.llAddCart:
                addCart();
                break;
            case R.id.llBuyNow:
                buyNow();
                break;
            case R.id.ivCart:
                Intent cartIntent = new Intent(this, CartActivity.class);
                startActivity(cartIntent);
                break;
            default:
                break;
        }
    }

    private void addCart() {
        showProgressBar(this);
        AddCartApiCall.serviceCallToAddProductToCart(this, null, null, productID, selectedVariantID, selectedQuantity, token);
    }

    private void buyNow() {
        int quantity = 1;
        double totalPrice = 0.0;

        JsonArray productsJsonArray = new JsonArray();
        String variantID = listOfVariantDetails.get(0).getId();
        String productID = productDescriptionResponse.getData().getId();
        String price = listOfVariantDetails.get(0).getMrp();
        String discount = listOfVariantDetails.get(0).getDiscount();
        Double salePrice = null;
        if (price != null && !price.isEmpty() && discount != null && !discount.isEmpty()) {
            salePrice = CommonMethods.getSalePrice(discount, price);
            salePrice = quantity * salePrice;
            totalPrice = totalPrice + salePrice;
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("product_id", productID);
        jsonObject.addProperty("variant_id", variantID);
        jsonObject.addProperty("qty", quantity);
        productsJsonArray.add(jsonObject);
        prepareCheckout(productsJsonArray);
    }


    private void prepareCheckout(JsonArray productsJsonArray) {
        checkoutRequestObject = new JsonObject();
        checkoutRequestObject.addProperty("address_id", "");
        checkoutRequestObject.addProperty("applied_promo_code", "");
        checkoutRequestObject.add("products", productsJsonArray);
        UserData.getInstance().setCheckoutRequestObject(checkoutRequestObject);
        showProgressBar(this);
        CheckoutProductsApiCall.serviceCallToCheckoutProducts(this, null, null, token, checkoutRequestObject);
    }
}
