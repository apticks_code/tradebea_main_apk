package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.SelectedOrderDetailsApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.SelectedOrderProductsAdapter;
import com.tradebeauser.adapters.SelectedOrderProductsStatusAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.CustomerAppStatus;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.DefaultReturnPolicy;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.DefaultReturnReason;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.Invoice;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.OrderDetail;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.ReturnReasonId;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.ReturnRequest;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.Rules;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.SelectedOrderData;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.SelectedOrderDetailsResponse;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import java.util.LinkedList;

public class SelectedOrderDetailsActivity extends BaseActivity implements HttpReqResCallBack, View.OnClickListener {

    private Rules rules;
    private ImageView ivBackArrow;
    private LinearLayout llDataContainer;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvProductDetails, rvProductStatus;
    private TextView tvSubTotal, tvTax, tvDelivery, tvTotalBill, tvReturn, tvCancel;

    private LinkedList<DefaultReturnReason> listOfDefaultReturnReasons;

    private int orderId = 0;
    private int returnId = 0;
    private int orderPickupOTP;
    private int orderReturnOTP;
    private int returnBeforeInHrs = 0;

    private String deliveryDate = "";

    private ReturnRequest returnRequest;
    private ReturnReasonId returnReasonId;
    private String reason = "";
    private Invoice invoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_selected_order_details);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
        refreshContent();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id)))
                orderId = bundle.getInt(getString(R.string.order_id));
        }
    }

    private void initializeUi() {
        tvTax = findViewById(R.id.tvTax);
        tvReturn = findViewById(R.id.tvReturn);
        tvCancel = findViewById(R.id.tvCancel);
        tvSubTotal = findViewById(R.id.tvSubTotal);
        tvDelivery = findViewById(R.id.tvDelivery);
        tvTotalBill = findViewById(R.id.tvTotalBill);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        llDataContainer = findViewById(R.id.llDataContainer);
        rvProductStatus = findViewById(R.id.rvProductStatus);
        rvProductDetails = findViewById(R.id.rvProductDetails);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }

    private void initializeListeners() {
        tvCancel.setOnClickListener(this);
        tvReturn.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        SelectedOrderDetailsApiCall.serviceCallForSelectedOrderDetails(this, null, null, orderId, token);
    }

    private void refreshContent() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                prepareDetails();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_SELECTED_ORDER_DETAILS) {
            if (jsonResponse != null) {
                tvReturn.setVisibility(View.GONE);
                SelectedOrderDetailsResponse selectedOrderDetailsResponse = new Gson().fromJson(jsonResponse, SelectedOrderDetailsResponse.class);
                if (selectedOrderDetailsResponse != null) {
                    boolean status = selectedOrderDetailsResponse.getStatus();
                    if (status) {
                        SelectedOrderData selectedOrderData = selectedOrderDetailsResponse.getSelectedOrderData();
                        if (selectedOrderData != null) {
                            LinkedList<CustomerAppStatus> listOfCustomerAppStatus = selectedOrderData.getListOfCustomerAppStatus();
                            LinkedList<OrderDetail> listOfOrderDetails = selectedOrderData.getListOfOrderDetails();
                            if (listOfOrderDetails != null) {
                                if (listOfOrderDetails.size() != 0) {
                                    Double subTotal = 0.0;
                                    Double tax = 0.0;
                                    Double total = 0.0;
                                    Double deliveryFee = 0.0;
                                    for (int index = 0; index < listOfOrderDetails.size(); index++) {
                                        OrderDetail orderDetail = listOfOrderDetails.get(index);
                                        Double tempSubTotal = 0.0;
                                        Double tempTax = 0.0;
                                        Double tempTotal = 0.0;
                                        Double tempDeliveryFee = 0.0;

                                        if (orderDetail.getSubTotal() != null)
                                            tempSubTotal = orderDetail.getSubTotal();
                                        if (orderDetail.getTax() != null)
                                            tempTax = orderDetail.getTax();
                                        if (orderDetail.getTotal() != null)
                                            tempTotal = orderDetail.getTotal();
                                        if (orderDetail.getDeliveryFee() != null)
                                            tempDeliveryFee = orderDetail.getDeliveryFee();
                                        subTotal = tempSubTotal + subTotal;
                                        tax = tempTax + tax;
                                        deliveryFee = tempDeliveryFee + deliveryFee;
                                        total = tempTotal + total;

                                        prepareReturnOrderDetails(listOfOrderDetails.get(0), listOfCustomerAppStatus);
                                    }
                                    llDataContainer.setVisibility(View.VISIBLE);
                                    returnRequest = selectedOrderData.getReturnRequest();
                                    invoice = selectedOrderData.getInvoice();
                                    String trackId = selectedOrderData.getTrackId();
                                    prepareReturnOtpDetails();

                                    initializeOrderDetails(listOfOrderDetails, returnRequest, invoice, trackId);
                                    initializeStatus(listOfCustomerAppStatus);


                                    tvSubTotal.setText(getString(R.string.rs) + " " + subTotal);
                                    tvTax.setText(getString(R.string.rs) + " " + tax);
                                    tvTotalBill.setText(getString(R.string.rs) + " " + total);
                                    tvDelivery.setText(getString(R.string.rs) + " " + deliveryFee);
                                } else {
                                    llDataContainer.setVisibility(View.GONE);
                                }
                            } else {
                                llDataContainer.setVisibility(View.GONE);
                            }
                        }
                    }
                }
                closeProgressbar();
            }
        }
    }

    private void prepareReturnOtpDetails() {
        if (returnRequest != null) {
            if (returnRequest.getOrderPickupOtp() != null)
                orderPickupOTP = returnRequest.getOrderPickupOtp();
            if (returnRequest.getOrderReturnOtp() != null)
                orderReturnOTP = returnRequest.getOrderReturnOtp();

            if (returnRequest.getReturnReasonId() != null)
                returnReasonId = returnRequest.getReturnReasonId();
            if (returnReasonId != null) {
                if (returnReasonId.getReason() != null) {
                    reason = returnReasonId.getReason();
                    tvReturn.setVisibility(View.GONE);
                }
            }
        }
    }

    private void prepareReturnOrderDetails(OrderDetail orderDetail, LinkedList<CustomerAppStatus> listOfCustomerAppStatus) {
        rules = orderDetail.getRules();
        tvReturn.setVisibility(View.GONE);
        if (listOfCustomerAppStatus != null) {
            if (listOfCustomerAppStatus.size() != 0) {
                for (int index = 0; index < listOfCustomerAppStatus.size(); index++) {
                    CustomerAppStatus customerAppStatus = listOfCustomerAppStatus.get(index);
                    Integer id = customerAppStatus.getId();
                    String createdAt = customerAppStatus.getCreatedAt();

                    if (id == 502) {
                        if (createdAt != null) {
                            tvCancel.setVisibility(View.GONE);
                        } else {
                            tvCancel.setVisibility(View.VISIBLE);
                        }
                    }

                    if (createdAt != null) {
                        if (id == 508) {
                            deliveryDate = customerAppStatus.getCreatedAt();
                            if (rules != null) {
                                DefaultReturnPolicy defaultReturnPolicy = rules.getDefaultReturnPolicy();
                                if (defaultReturnPolicy != null) {
                                    returnId = defaultReturnPolicy.getId();
                                    listOfDefaultReturnReasons = defaultReturnPolicy.getListOfDefaultReturnReasons();
                                    if (defaultReturnPolicy.getReturnBeforeInHrs() != null) {
                                        returnBeforeInHrs = defaultReturnPolicy.getReturnBeforeInHrs();
                                    } else {
                                        returnBeforeInHrs = 0;
                                    }
                                    //deliveryDate = "2022-10-27 21:00:00";
                                    String timeAgo = CommonMethods.getTimeAgo(deliveryDate);
                                    int timeAgoInt = Integer.parseInt(timeAgo);
                                    if (returnBeforeInHrs >= timeAgoInt) {
                                        tvReturn.setVisibility(View.VISIBLE);
                                    } else {
                                        tvReturn.setVisibility(View.GONE);
                                    }
                                }
                            }
                        } else {
                            tvReturn.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }
    }

    private void initializeStatus(LinkedList<CustomerAppStatus> listOfCustomerAppStatus) {
        SelectedOrderProductsStatusAdapter selectedOrderProductsStatusAdapter = new SelectedOrderProductsStatusAdapter(this, listOfCustomerAppStatus);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvProductStatus.setLayoutManager(layoutManager);
        rvProductStatus.setItemAnimator(new DefaultItemAnimator());
        rvProductStatus.setAdapter(selectedOrderProductsStatusAdapter);
    }

    private void initializeOrderDetails(LinkedList<OrderDetail> listOfOrderDetails, ReturnRequest returnRequest, Invoice invoice, String trackId) {
        SelectedOrderProductsAdapter selectedOrderProductsAdapter = new SelectedOrderProductsAdapter(this, listOfOrderDetails, returnRequest, invoice, trackId);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvProductDetails.setLayoutManager(layoutManager);
        rvProductDetails.setItemAnimator(new DefaultItemAnimator());
        rvProductDetails.setAdapter(selectedOrderProductsAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.tvReturn) {
            prepareReturnDetails();
        } else if (id == R.id.tvCancel) {
            prepareCancelOrderDetails();
        }
    }

    private void prepareCancelOrderDetails() {
        Intent cancelIntent = new Intent(this, CancelOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.order_id), orderId);
        cancelIntent.putExtras(bundle);
        startActivity(cancelIntent);
    }

    private void prepareReturnDetails() {
        UserData.getInstance().setListOfDefaultReturnReasons(listOfDefaultReturnReasons);
        Intent returnOrderIntent = new Intent(this, ReturnOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.return_id), returnId);
        bundle.putInt(getString(R.string.order_id), orderId);
        returnOrderIntent.putExtras(bundle);
        startActivity(returnOrderIntent);
    }
}