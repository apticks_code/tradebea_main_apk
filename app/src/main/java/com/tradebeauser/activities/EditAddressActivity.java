package com.tradebeauser.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.UpdateAddressApiCall;
import com.tradebeauser.R;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.AddOrUpdateOrDeleteResponse.AddOrUpdateOrDeleteResponse;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.models.responseModels.profileResponse.LocationInfo;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class EditAddressActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, RadioGroup.OnCheckedChangeListener {
    private ImageView ivBack;
    private RadioGroup radioGroup;
    private RadioButton rbHome, rbWork, rbOthers;
    private TextView tvEdit, tvGeoLocation, tvSave;
    private EditText etCompleteAddress, etLandmark, etPinCode, etOther;
    private List<AddressDetails> listOfActiveAddress;
    private AddressDetails addressDetails;
    private LocationInfo locationInfo;

    private String latitude = "";
    private String longitude = "";
    private String geoLocation = "";
    private String token = "";
    private String tag = "";
    private String geoAddress = "";
    private String recId = "";
    private String landmark = "";
    private String pinCode = "";
    private String other = "";
    private String completeAddress = "";
    private int AUTOCOMPLETE_REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_address_activity);
        getIntentData();
        initializeUI();
        initializeListeners();
        prepareDetails();
    }

    private void getIntentData() {
        /*Bundle bundle = getIntent().getExtras();
        int position = -1;
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.position))) {
                position = bundle.getInt(getString(R.string.position));
            }
            if (bundle.containsKey(getString(R.string.address_details))) {
                listOfActiveAddress = (List<AddressDetails>) bundle.getSerializable(getString(R.string.address_details));
                if(position!=-1) {
                    addressDetails = listOfActiveAddress.get(position);
                }
            }
        }*/
        addressDetails = UserData.getInstance().getSelectedAddress();
    }

    private void initializeUI() {
        ivBack = findViewById(R.id.ivBack);
        tvEdit = findViewById(R.id.tvEdit);
        tvSave = findViewById(R.id.tvSave);
        rbHome = findViewById(R.id.rbHome);
        rbWork = findViewById(R.id.rbWork);
        etOther = findViewById(R.id.etOther);
        rbOthers = findViewById(R.id.rbOthers);
        etPinCode = findViewById(R.id.etPinCode);
        radioGroup = findViewById(R.id.radioGroup);
        etLandmark = findViewById(R.id.etLandmark);
        tvGeoLocation = findViewById(R.id.tvGeoLocation);
        etCompleteAddress = findViewById(R.id.etCompleteAddress);

        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
    }

    private void prepareDetails() {
        if (addressDetails != null) {
            locationInfo = addressDetails.getLocation();
            latitude = String.valueOf(locationInfo.getLatitude());
            longitude = String.valueOf(locationInfo.getLongitude());
            recId = String.valueOf(addressDetails.getId());
            tag = addressDetails.getTag();
            other = addressDetails.getTag();
            landmark = addressDetails.getLandmark();
            pinCode = String.valueOf(addressDetails.getPincode());
            completeAddress = addressDetails.getAddress();
            geoLocation = addressDetails.getLocation().getGeoLocation();
            if(tag!= null){
                if(tag.equalsIgnoreCase("home")){
                    rbHome.setChecked(true);
                    etOther.setVisibility(View.GONE);
                }else if(tag.equalsIgnoreCase("work")){
                    rbWork.setChecked(true);
                    etOther.setVisibility(View.GONE);
                }else{
                    rbOthers.setChecked(true);
                    etOther.setVisibility(View.VISIBLE);
                    etOther.setText(tag);
                }
            }
            if (geoLocation != null) {
                tvGeoLocation.setText(String.format("%s", geoLocation));
            }
            if (completeAddress != null) {
                etCompleteAddress.setText(String.format("%s", completeAddress));
            }
            if (landmark != null) {
                etLandmark.setText(String.format("%s", landmark));
            }
            if (pinCode != null) {
                etPinCode.setText(String.format("%s", pinCode));
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.tvEdit:
                showPlacesAutoComplete();
                break;
            case R.id.tvSave:
                geoAddress = tvGeoLocation.getText().toString();
                completeAddress = etCompleteAddress.getText().toString();
                landmark = etLandmark.getText().toString();
                pinCode = etPinCode.getText().toString();
                other = etOther.getText().toString();

                if (!geoAddress.isEmpty()) {
                    if (!completeAddress.isEmpty()) {
                        if (!tag.isEmpty()) {
                            showProgressBar(this);
                            UpdateAddressApiCall.serviceCallToUpdateNewAddresses(this, null, null, token, geoAddress, completeAddress, landmark, pinCode, tag, latitude, longitude,recId);
                        } else if (!other.isEmpty()) {
                            showProgressBar(this);
                            UpdateAddressApiCall.serviceCallToUpdateNewAddresses(this, null, null, token, geoAddress, completeAddress, landmark, pinCode, other, latitude, longitude,recId);
                        } else {
                            Toast.makeText(this, "Select tag", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Enter complete Address", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Enter Geo Address", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
    }

    private void showPlacesAutoComplete() {
        Places.initialize(getApplicationContext(), getString(R.string.places_api_key));

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = Autocomplete.getPlaceFromIntent(data);
                latitude = String.valueOf(Objects.requireNonNull(place.getLatLng()).latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
                tvGeoLocation.setText(place.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                String status = Autocomplete.getStatusFromIntent(data).getStatusMessage();
                Log.d("asd", status);
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("asd", "asd");
            }
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
            case R.id.rbHome:
                etOther.setVisibility(View.GONE);
                tag = getString(R.string.home);
                etOther.setText("");
                break;
            case R.id.rbWork:
                tag = getString(R.string.work);
                etOther.setVisibility(View.GONE);
                etOther.setText("");
                break;
            case R.id.rbOthers:
                etOther.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_UPDATE_ADDRESS:
                if (jsonResponse != null) {
                    AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                    if (addOrUpdateOrDeleteResponse != null) {
                        boolean status = addOrUpdateOrDeleteResponse.getStatus();
                        String message = addOrUpdateOrDeleteResponse.getMessage();
                        if (status) {
                            PreferenceConnector.writeBoolean(this, getString(R.string.refresh_saved_address), true);
                            finish();
                        }
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
        }
    }


}
