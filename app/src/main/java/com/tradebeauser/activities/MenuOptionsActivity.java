package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.GetMenuCategoriesApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.MenuBannerImagesAdapter;
import com.tradebeauser.adapters.MenuBrandsAdapter;
import com.tradebeauser.adapters.MenuCategoriesAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.CategoriesInfoDetails;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.Data;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.MenuBrands;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.MenuCategoryDetails;
import com.tradebeauser.models.responseModels.MenuCategoryDetails.MenuCategoryDetailsResponse;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.HeightWrappingViewPager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MenuOptionsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private Timer timer;
    private ImageView ivBack, ivCart;
    private TextView tvTitle, tvError;
    private LinearLayout llBanners, llDots;
    private HeightWrappingViewPager viewPager;
    private NestedScrollView nestedScrollView;
    private RecyclerView rvCategories, rvBrands;

    private String menuID = "";
    private MenuCategoryDetails menuCategoryDetails;
    private List<String> listOfBannerImages;
    private List<MenuBrands> listOfBrandDetails;
    private List<CategoriesInfoDetails> listOfCategoryDetails;

    private int currentPage = 0;
    private final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu_options);
        getIntentData();
        initializeUi();
        initializeListeners();
        prepareMenuOptions();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.menu_id))) {
                menuID = bundle.getString(getString(R.string.menu_id));
            }
        }
    }

    private void initializeUi() {
        llDots = findViewById(R.id.llDots);
        ivCart = findViewById(R.id.ivCart);
        ivBack = findViewById(R.id.ivBack);
        tvError = findViewById(R.id.tvError);
        tvTitle = findViewById(R.id.tvTitle);
        rvBrands = findViewById(R.id.rvBrands);
        llBanners = findViewById(R.id.llBanners);
        viewPager = findViewById(R.id.viewPager);
        rvCategories = findViewById(R.id.rvCategories);
        nestedScrollView = findViewById(R.id.nestedScrollView);

        listOfBannerImages = new LinkedList<>();
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
    }

    private void prepareMenuOptions() {
        showProgressBar(this);
        GetMenuCategoriesApiCall.serviceCallToGetMenuCategories(this, null, null, menuID);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBack:
                onBackPressed();
                break;
            default:
                break;

        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_MENU_CATEGORIES:
                if (jsonResponse != null) {
                    try {
                        MenuCategoryDetailsResponse menuDetailsResponse = new Gson().fromJson(jsonResponse, MenuCategoryDetailsResponse.class);
                        if (menuDetailsResponse != null) {
                            boolean status = menuDetailsResponse.getStatus();
                            String message = menuDetailsResponse.getMessage();
                            Data menuData = menuDetailsResponse.getMenuData();
                            if (status) {
                                menuCategoryDetails = menuData.getMenuDetails();
                                listOfBrandDetails = menuData.getListOfMenuBrands();
                                listOfBannerImages = menuData.getListOfMenuBanners();
                                listOfCategoryDetails = menuCategoryDetails.getListOfCategoriesDetails();
                                if (listOfBannerImages != null) {
                                    if (listOfBannerImages.size() != 0) {
                                        llBanners.setVisibility(View.VISIBLE);
                                        prepareBanners();
                                    } else {
                                        llBanners.setVisibility(View.GONE);
                                    }
                                } else {
                                    llBanners.setVisibility(View.GONE);
                                }

                                if(listOfCategoryDetails == null){
                                    listOfCategoryDetails = new ArrayList<>();
                                }

                                if(listOfBrandDetails == null){
                                    listOfBrandDetails = new ArrayList<>();
                                }

                                if (listOfCategoryDetails.size() == 0 && listOfBrandDetails.size() == 0) {
                                    nestedScrollView.setVisibility(View.GONE);
                                    tvError.setVisibility(View.VISIBLE);
                                }
                                initializeBrandsAdapter();
                                initializeAdapter();
                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Toast.makeText(this, getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
                closeProgressbar();
                break;
        }
    }

    private void initializeBrandsAdapter() {
        if (listOfBrandDetails != null && listOfBrandDetails.size() != 0) {
            rvBrands.setVisibility(View.VISIBLE);
            MenuBrandsAdapter menuDetailsAdapter = new MenuBrandsAdapter(this, listOfBrandDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
            rvBrands.setLayoutManager(layoutManager);
            rvBrands.setItemAnimator(new DefaultItemAnimator());
            rvBrands.setAdapter(menuDetailsAdapter);
        }
    }

    private void prepareBanners() {
        MenuBannerImagesAdapter welcomeImagesAdapter = new MenuBannerImagesAdapter(this, listOfBannerImages);
        viewPager.setAdapter(welcomeImagesAdapter);
        for (int index = 0; index < welcomeImagesAdapter.getCount(); index++) {
            ImageButton ibDot = new ImageButton(this);
            ibDot.setTag(index);
            ibDot.setImageResource(R.drawable.dot_selector);
            ibDot.setBackgroundResource(0);
            ibDot.setPadding(0, 0, 5, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(60, 60);
            ibDot.setLayoutParams(params);
            if (index == 0)
                ibDot.setSelected(true);
            llDots.addView(ibDot);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int index = 0; index < welcomeImagesAdapter.getCount(); index++) {
                    if (index != position) {
                        (llDots.findViewWithTag(index)).setSelected(false);
                    }
                }
                (llDots.findViewWithTag(position)).setSelected(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        /*After setting the adapter use the timer */
        int NUM_PAGES = listOfBannerImages.size();
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                viewPager.setCurrentItem(currentPage++, true);
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    private void initializeAdapter() {
        String id = menuCategoryDetails.getId();
        String name = menuCategoryDetails.getName();
        tvTitle.setText(name);
        if (listOfCategoryDetails != null && listOfCategoryDetails.size() != 0) {
            rvCategories.setVisibility(View.VISIBLE);
            MenuCategoriesAdapter shopCategoriesAdapter = new MenuCategoriesAdapter(this, listOfCategoryDetails);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvCategories.setLayoutManager(layoutManager);
            rvCategories.setItemAnimator(new DefaultItemAnimator());
            rvCategories.setAdapter(shopCategoriesAdapter);
        } else {

        }


        /*else {
            gotToProductsList(id);
        }*/
    }

    private void gotToProductsList(String value) {
        Intent productsIntent = new Intent(this, ProductsListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.coming_from), getString(R.string.shop_by_category));
        bundle.putString(getString(R.string.filter_key), "menu_id");
        bundle.putString(getString(R.string.filter_value), value);
        productsIntent.putExtras(bundle);
        startActivity(productsIntent);
    }
}
