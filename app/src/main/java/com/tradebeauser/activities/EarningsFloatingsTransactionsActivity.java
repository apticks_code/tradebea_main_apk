package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.GetTransactionDetails;
import com.tradebeauser.R;
import com.tradebeauser.adapters.TransactionDetailsAdapter;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.getTransactionDetailsResponse.GetTransactionDetailsResponse;
import com.tradebeauser.models.responseModels.getTransactionDetailsResponse.TransactionDetails;
import com.tradebeauser.models.responseModels.getTransactionDetailsResponse.TransactionDetailsData;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;

import java.util.LinkedList;

public class EarningsFloatingsTransactionsActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;
    private RecyclerView rvTransactionDetails;
    private TextView tvError, tvActionBarTitle;

    private LinkedList<TransactionDetails> listOfTransactionDetails;
    private String comingFrom = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_earnings_floating_transactions);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.coming_from))) {
                comingFrom = bundle.getString(getString(R.string.coming_from), "");
            }
        }
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvActionBarTitle = findViewById(R.id.tvActionBarTitle);
        rvTransactionDetails = findViewById(R.id.rvTransactionDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        tvActionBarTitle.setText(comingFrom);
        String status = "";
        if (comingFrom.equalsIgnoreCase(getString(R.string.earnings))) {
            status = "1";
        } else if (comingFrom.equalsIgnoreCase(getString(R.string.floatings))) {
            status = "2";
        }

        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetTransactionDetails.serviceCallToGetTransactionDetails(this, null, null, token, "", "", "30", status);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_TRANSACTION_DETAILS) {
            if (jsonResponse != null) {
                GetTransactionDetailsResponse getTransactionDetailsResponse = new Gson().fromJson(jsonResponse, GetTransactionDetailsResponse.class);
                if (getTransactionDetailsResponse != null) {
                    boolean status = getTransactionDetailsResponse.getStatus();
                    if (status) {
                        TransactionDetailsData transactionDetailsData = getTransactionDetailsResponse.getTransactionDetailsData();
                        if (transactionDetailsData != null) {
                            listOfTransactionDetails = transactionDetailsData.getListOfTransactionDetails();
                            if (listOfTransactionDetails != null) {
                                if (listOfTransactionDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsEmpty();
                            }
                        }
                    } else {
                        listIsEmpty();
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void initializeAdapter() {
        TransactionDetailsAdapter transactionDetailsAdapter = new TransactionDetailsAdapter(this, listOfTransactionDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvTransactionDetails.setLayoutManager(layoutManager);
        rvTransactionDetails.setItemAnimator(new DefaultItemAnimator());
        rvTransactionDetails.setAdapter(transactionDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvTransactionDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvTransactionDetails.setVisibility(View.GONE);
    }
}
