package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tradebeauser.R;
import com.tradebeauser.adapters.ProductsListAdapter;
import com.tradebeauser.adapters.QuantityAdapter;
import com.tradebeauser.interfaces.SelectedQuantityCallBack;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import java.util.Objects;

public class QuantityPopupActivity extends BaseActivity implements SelectedQuantityCallBack, View.OnClickListener {
    private ImageView ivClose;
    private RecyclerView rvQuantity;

    private int quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_quantity_popup);
        getIntentData();
        initializeUI();
        initializeListeners();
        initializeAdapter();
    }

    private void initializeListeners() {
        ivClose.setOnClickListener(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.quantity))) {
                String  quantity = bundle.getString(getString(R.string.quantity));
                if(quantity!=null && !quantity.isEmpty())
                this.quantity = Integer.parseInt(quantity);
            }
        }
    }

    private void initializeUI() {
        ivClose = findViewById(R.id.ivClose);
        rvQuantity = findViewById(R.id.rvQuantity);
    }

    private void initializeAdapter() {
        QuantityAdapter quantityAdapter = new QuantityAdapter(this, quantity);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvQuantity.setLayoutManager(layoutManager);
        rvQuantity.setItemAnimator(new DefaultItemAnimator());
        rvQuantity.setAdapter(quantityAdapter);
    }

    @Override
    public void OnQuantitySelected(int quantity) {
        UserData.getInstance().setQuantity(quantity);
        PreferenceConnector.writeBoolean(this,getString(R.string.isQuantityUpdated),true);
        finish();
    }

    @Override
    public void onClick(View view) {
        onBackPressed();
    }
}
