package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.tradebeauser.R;
import com.tradebeauser.adapters.FilterLabelsAdapter;
import com.tradebeauser.adapters.BasicFilterOptionsAdapter;
import com.tradebeauser.adapters.OptionalFilterOptionsAdapter;
import com.tradebeauser.models.responseModels.productDetailsResponse.BasicFilterDetails;
import com.tradebeauser.models.responseModels.productDetailsResponse.FilterLabelDetails;
import com.tradebeauser.models.responseModels.productDetailsResponse.OptionalFilterDetails;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ProductsFilterActivity extends BaseActivity implements View.OnClickListener , FilterLabelsAdapter.ItemClickListener {

    private ImageView ivBack;
    private TextView tvClearFilter, tvCancel, tvApply;
    private RecyclerView rvFilterType, rvFilterOptions;
    private JSONObject filters;
    private List<FilterLabelDetails> listOfFilterLabels;
    private List<List<OptionalFilterDetails>> listOfOptionalFilters;
    private List<OptionalFilterDetails> listOfOptionalFiltersValues;
    private LinkedHashMap<String, List<BasicFilterDetails>> mapOfBasicFilterDetails;
    private LinkedHashMap<String, List<OptionalFilterDetails>> mapOfOptionalFilterDetails;

    private String filtersObject = "";
    private JSONObject basicFilters;
    private JSONArray filterLabels;
    private JSONObject optionalFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_filter_activity);
        getIntentData();
        initializeUI();
        initializeListeners();
        prepareDetails();

    }


    private void getIntentData() {
       /* Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.filter))) {
                filtersObject = bundle.getString(getString(R.string.filter));
            }
        }*/
       filters = UserData.getInstance().getFiltersDetails();
       mapOfBasicFilterDetails = new LinkedHashMap<>();
       mapOfOptionalFilterDetails = new LinkedHashMap<>();
    }

    private void initializeUI() {
        ivBack = findViewById(R.id.ivBack);
        tvApply = findViewById(R.id.tvApply);
        tvCancel = findViewById(R.id.tvCancel);
        rvFilterType = findViewById(R.id.rvFilterType);
        tvClearFilter = findViewById(R.id.tvClearFilter);
        rvFilterOptions = findViewById(R.id.rvFilterOptions);
        listOfFilterLabels = new LinkedList<>();
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvApply.setOnClickListener(this);
        tvClearFilter.setOnClickListener(this);
    }

    private void prepareDetails() {
        if (filters != null) {
            try {
                Iterator<String> listOfKeys = filters.keys();

                for (; listOfKeys.hasNext(); ) {
                    String key = listOfKeys.next();

                    if (key.equalsIgnoreCase("basic_filters"))
                        basicFilters = filters.getJSONObject("basic_filters");
                    else if (key.equalsIgnoreCase("filter_labels")) {
                        filterLabels = filters.getJSONArray("filter_labels");
                        for (int index = 0; index < filterLabels.length(); index++) {
                            FilterLabelDetails filterLabelDetails = new Gson().fromJson(String.valueOf(filterLabels.get(index)), FilterLabelDetails.class);
                            listOfFilterLabels.add(filterLabelDetails);
                        }
                    } else if (key.equalsIgnoreCase("option_filters")) {
                        optionalFilters = filters.getJSONObject("option_filters");
                    }

                }

                for (int labelIndex = 0; labelIndex < listOfFilterLabels.size(); labelIndex++) {
                    List<BasicFilterDetails> listOfBasicFilters = new LinkedList<>();
                    String key = listOfFilterLabels.get(labelIndex).getId();
                    if (basicFilters.has(key)) {

                        JSONArray basicFiltersJsonArray = basicFilters.getJSONArray(key);
                        for (int basicIndex = 0; basicIndex < basicFiltersJsonArray.length(); basicIndex++) {
                            JSONObject basicFilter = basicFiltersJsonArray.getJSONObject(basicIndex);
                            BasicFilterDetails basicFilterDetails = new Gson().fromJson(String.valueOf(basicFilter), BasicFilterDetails.class);
                            listOfBasicFilters.add(basicFilterDetails);
                        }
                        mapOfBasicFilterDetails.put(key, listOfBasicFilters);

                    } else if (optionalFilters.has(key)) {
                        List<OptionalFilterDetails> listOfOptionalFilters = new LinkedList<>();
                            JSONArray optionalFiltersJsonArray = optionalFilters.getJSONArray(key);

                            for (int optionalIndex = 0; optionalIndex < optionalFiltersJsonArray.length(); optionalIndex++) {

                                JSONArray optionalFilter = optionalFiltersJsonArray.optJSONArray(optionalIndex);
                                for (int optIndex = 0; optIndex < optionalFilter.length(); optIndex++) {
                                    JSONObject optFilter = optionalFilter.getJSONObject(optIndex);
                                    OptionalFilterDetails optFilterDetails = new Gson().fromJson(String.valueOf(optFilter), OptionalFilterDetails.class);
                                    listOfOptionalFilters.add(optFilterDetails);
                            }
                            mapOfOptionalFilterDetails.put(key, listOfOptionalFilters);
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            initializeAdapter();
        }
    }

    private void initializeAdapter() {
        FilterLabelsAdapter filterLabelsAdapter = new FilterLabelsAdapter(this, listOfFilterLabels);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvFilterType.setLayoutManager(layoutManager);
        rvFilterType.setItemAnimator(new DefaultItemAnimator());
        rvFilterType.setAdapter(filterLabelsAdapter);
        filterLabelsAdapter.setClickListener(this);
        rvFilterType.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if(rvFilterType.findViewHolderForAdapterPosition(0)!=null )
                {

                    rvFilterType.findViewHolderForAdapterPosition(0).itemView.performClick();
                }
            }
        },50);
      //  Objects.requireNonNull(rvFilterType.findViewHolderForAdapterPosition(0)).itemView.performClick();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivBack:
            case R.id.tvCancel:
                LinkedHashMap<String, List<String>> mapOfFinalFilterLabelKeyAndSelectedOptions = UserData.getInstance().getMapOfFinalFilterLabelKeyAndSelectedOptions();
                UserData.getInstance().setMapOfFilterLabelKeyAndSelectedOptions(mapOfFinalFilterLabelKeyAndSelectedOptions);
                onBackPressed();
                break;

            case R.id.tvApply:
                LinkedHashMap<String, List<String>> mapOfFilterLabelKeyAndSelectedOptions = UserData.getInstance().getMapOfFilterLabelKeyAndSelectedOptions();
                UserData.getInstance().setMapOfFinalFilterLabelKeyAndSelectedOptions(mapOfFilterLabelKeyAndSelectedOptions);
                PreferenceConnector.writeBoolean(this,getString(R.string.filter_applied),true);
                finish();
                break;

            case R.id.tvClearFilter:
                UserData.getInstance().setMapOfFilterLabelKeyAndSelectedOptions(new LinkedHashMap<>());
                UserData.getInstance().setMapOfFinalFilterLabelKeyAndSelectedOptions(new LinkedHashMap<>());
                PreferenceConnector.writeBoolean(this,getString(R.string.filter_cleared),true);
                finish();
                break;
        }
    }

    @Override
    public void onClick(View view, int position) {
        String labelKey = listOfFilterLabels.get(position).getId();
        if(mapOfBasicFilterDetails.containsKey(labelKey)) {
            BasicFilterOptionsAdapter basicFilterOptionsAdapter = new BasicFilterOptionsAdapter(this,mapOfBasicFilterDetails.get(labelKey),labelKey);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rvFilterOptions.setLayoutManager(layoutManager);
            rvFilterOptions.setItemAnimator(new DefaultItemAnimator());
            rvFilterOptions.setAdapter(basicFilterOptionsAdapter);
        }else if(mapOfOptionalFilterDetails.containsKey(labelKey)){
            OptionalFilterOptionsAdapter optionalFilterOptionsAdapter = new OptionalFilterOptionsAdapter(this,mapOfOptionalFilterDetails.get(labelKey),labelKey);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rvFilterOptions.setLayoutManager(layoutManager);
            rvFilterOptions.setItemAnimator(new DefaultItemAnimator());
            rvFilterOptions.setAdapter(optionalFilterOptionsAdapter);
        }

    }
}
