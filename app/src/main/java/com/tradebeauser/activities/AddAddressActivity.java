package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.AddAddressApiCall;
import com.tradebeauser.R;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.AddOrUpdateOrDeleteResponse.AddOrUpdateOrDeleteResponse;
import com.tradebeauser.models.responseModels.profileResponse.AddressDetails;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;


public class AddAddressActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack, RadioGroup.OnCheckedChangeListener {

    private ImageView ivBack;
    private RadioGroup radioGroup;
    private RadioButton rbHome, rbWork, rbOthers;
    private TextView tvEdit, tvGeoLocation, tvSave;
    private EditText etCompleteAddress, etLandmark, etPinCode, etOther;
    private AddressDetails addressDetails;

    private String tag = "";
    private String token = "";
    private String latitude = "";
    private String longitude = "";
    private String geoLocation = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_address_activity);
        getIntentData();
        initializeUI();
        initializeListeners();
        prepareDetails();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.latitude))) {
                latitude = bundle.getString(getString(R.string.latitude));
            }
            if (bundle.containsKey(getString(R.string.longitude))) {
                longitude = bundle.getString(getString(R.string.longitude));
            }
            if (bundle.containsKey(getString(R.string.address))) {
                geoLocation = bundle.getString(getString(R.string.address));
            }
        }
    }

    private void initializeUI() {
        ivBack = findViewById(R.id.ivBack);
        tvEdit = findViewById(R.id.tvEdit);
        tvSave = findViewById(R.id.tvSave);
        rbHome = findViewById(R.id.rbHome);
        rbWork = findViewById(R.id.rbWork);
        etOther = findViewById(R.id.etOther);
        rbOthers = findViewById(R.id.rbOthers);
        etPinCode = findViewById(R.id.etPinCode);
        radioGroup = findViewById(R.id.radioGroup);
        etLandmark = findViewById(R.id.etLandmark);
        tvGeoLocation = findViewById(R.id.tvGeoLocation);
        etCompleteAddress = findViewById(R.id.etCompleteAddress);

        token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        radioGroup.setOnCheckedChangeListener(this);
    }

    private void prepareDetails() {
        tvGeoLocation.setText(geoLocation);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.tvEdit:
                onBackPressed();
                break;
            case R.id.tvSave:
                String geoAddress = tvGeoLocation.getText().toString();
                String completeAddress = etCompleteAddress.getText().toString();
                String landmark = etLandmark.getText().toString();
                String pinCode = etPinCode.getText().toString();
                String other = etOther.getText().toString();

                if (!geoAddress.isEmpty()) {
                    if (!completeAddress.isEmpty()) {
                        if (!tag.isEmpty()) {
                            showProgressBar(this);
                            AddAddressApiCall.serviceCallToAddNewAddresses(this, null, null, token, geoAddress, completeAddress, landmark, pinCode, tag,latitude,longitude);
                        } else if (!other.isEmpty()) {
                            showProgressBar(this);
                            AddAddressApiCall.serviceCallToAddNewAddresses(this, null, null, token, geoAddress, completeAddress, landmark, pinCode, other,latitude,longitude);
                        } else {
                            Toast.makeText(this, "Select tag", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Enter complete Address", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Enter Geo Address", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
            case R.id.rbHome:
                etOther.setVisibility(View.GONE);
                tag = getString(R.string.home);
                etOther.setText("");
                break;
            case R.id.rbWork:
                tag = getString(R.string.work);
                etOther.setVisibility(View.GONE);
                etOther.setText("");
                break;
            case R.id.rbOthers:
                etOther.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_ADD_ADDRESS:
                if (jsonResponse != null) {
                        AddOrUpdateOrDeleteResponse addOrUpdateOrDeleteResponse = new Gson().fromJson(jsonResponse, AddOrUpdateOrDeleteResponse.class);
                        if (addOrUpdateOrDeleteResponse != null) {
                            boolean status = addOrUpdateOrDeleteResponse.getStatus();
                            String message = addOrUpdateOrDeleteResponse.getMessage();
                            if (status) {
                                PreferenceConnector.writeBoolean(this, getString(R.string.refresh_saved_address), true);
                                finish();
                            }
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                    closeProgressbar();
                break;
        }
    }


}
