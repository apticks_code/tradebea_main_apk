package com.tradebeauser.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.hbb20.CountryCodePicker;
import com.tradebeauser.ApiCalls.LoginDetailsApiCall;
import com.tradebeauser.ApiCalls.SocialLoginApiCall;
import com.tradebeauser.R;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.responseModels.loginResponse.LoginData;
import com.tradebeauser.models.responseModels.loginResponse.LoginResponse;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;

import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private CallbackManager callbackManager;
    private EditText etPhoneNumber, etPassword;
    private CountryCodePicker countryCodePicker;
    private GoogleSignInClient mGoogleSignInClient;
    private LinearLayout llLogin, llLoginByOtp, llSignUp;
    private TextView tvLogin, tvLoginByOTP, tvSignUp, tvForgotPassword;
    private ImageView ivBackArrow, ivShowHidePassword, ivGmail, ivFacebook;

    private boolean isShowPassword;
    private boolean isValidMobileNumber;

    private static final int RC_SIGN_IN = 1;

    private String id = "";
    private String facebookName = "";
    private String facebookEmailId = "";
    private String facebookProfilePic = "";
    private String facebookDateOfBirth = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);
        setStatusBarColor();
        initializeUi();
        initializeListeners();
        prepareSocialLoginDetails();
        prepareCountryCodePickerDetails();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    private void initializeUi() {
        tvLogin = findViewById(R.id.tvLogin);
        llLogin = findViewById(R.id.llLogin);
        ivGmail = findViewById(R.id.ivGmail);
        tvSignUp = findViewById(R.id.tvSignUp);
        llSignUp = findViewById(R.id.llSignUp);
        ivFacebook = findViewById(R.id.ivFacebook);
        etPassword = findViewById(R.id.etPassword);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        llLoginByOtp = findViewById(R.id.llLoginByOtp);
        tvLoginByOTP = findViewById(R.id.tvLoginByOTP);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        countryCodePicker = findViewById(R.id.countryCodePicker);
        ivShowHidePassword = findViewById(R.id.ivShowHidePassword);
    }

    private void initializeListeners() {
        llLogin.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        ivGmail.setOnClickListener(this);
        llSignUp.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        llLoginByOtp.setOnClickListener(this);
        tvLoginByOTP.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        ivShowHidePassword.setOnClickListener(this);
    }

    private void prepareCountryCodePickerDetails() {
        countryCodePicker.registerCarrierNumberEditText(etPhoneNumber);
        countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                isValidMobileNumber = isValidNumber;
                if (isValidNumber) {
                    etPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_valid, 0);
                } else {
                    etPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        });
    }

    private void prepareSocialLoginDetails() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest graphRequest = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject jsonObject, GraphResponse response) {
                                        if (response.getError() != null) {
                                            Toast.makeText(LoginActivity.this, getString(R.string.status_error), Toast.LENGTH_SHORT).show();
                                        } else {
                                            try {
                                                id = jsonObject.getString("id");
                                                facebookName = jsonObject.optString("name");
                                                facebookEmailId = jsonObject.optString("email");
                                                facebookDateOfBirth = jsonObject.optString("birthday");
                                                if (jsonObject.has("picture")) {
                                                    facebookProfilePic = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
                                                }
                                                facebookProfilePic = "https://graph.facebook.com/" + id + "/picture?type=large";
                                                showProgressBar(LoginActivity.this);
                                                SocialLoginApiCall.serviceCallForSocialLoginDetails(LoginActivity.this, facebookName, facebookProfilePic, facebookEmailId, id, "");
                                            } catch (Exception exception) {
                                                exception.printStackTrace();
                                            }
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,link,email,first_name,last_name,gender,location,birthday,picture");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        LoginManager.getInstance().logOut();
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.ivGmail:
                prepareGmailDetails();
                break;
            case R.id.ivFacebook:
                prepareFacebookDetails();
                break;
            case R.id.llLogin:
            case R.id.tvLogin:
                prepareLoginDetails();
                break;
            case R.id.llLoginByOtp:
            case R.id.tvLoginByOTP:
                prepareLoginByOTPDetails();
                break;
            case R.id.llSignUp:
            case R.id.tvSignUp:
                prepareSignUpDetails();
                break;
            case R.id.ivShowHidePassword:
                prepareShowHidePasswordDetails();
                break;
            case R.id.tvForgotPassword:
                prepareForgotPasswordDetails();
                break;
            default:
                break;
        }
    }

    private void prepareGmailDetails() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void prepareFacebookDetails() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));
    }

    private void prepareLoginDetails() {
        String phoneNumber = etPhoneNumber.getText().toString();
        String password = etPassword.getText().toString();
        if (!phoneNumber.isEmpty()) {
            if (isValidMobileNumber) {
                if (!password.isEmpty()) {
                    showProgressBar(this);
                    LoginDetailsApiCall.serviceCallForLoginDetails(this, phoneNumber, password);
                } else {
                    Toast.makeText(this, getString(R.string.please_enter_password), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.please_select_valid_mobile_number), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_phone_number), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareLoginByOTPDetails() {
        Intent loginByOTPIntent = new Intent(this, LoginByOTPActivity.class);
        startActivity(loginByOTPIntent);
    }

    private void prepareSignUpDetails() {
        Intent signUpIntent = new Intent(this, SignUpActivity.class);
        startActivity(signUpIntent);
    }

    private void prepareShowHidePasswordDetails() {
        if (!isShowPassword) {
            isShowPassword = true;
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            ivShowHidePassword.setImageResource(R.drawable.ic_eye_open);
            String password = etPassword.getText().toString();
            if (!password.isEmpty()) {
                etPassword.setSelection(password.length());
            }
        } else {
            isShowPassword = false;
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            ivShowHidePassword.setImageResource(R.drawable.ic_eye_close);
            String password = etPassword.getText().toString();
            if (!password.isEmpty()) {
                etPassword.setSelection(password.length());
            }
        }
    }

    private void prepareForgotPasswordDetails() {
        Intent forgotPasswordDetailsIntent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(forgotPasswordDetailsIntent);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_FOR_LOGIN:
                if (jsonResponse != null) {
                    LoginResponse loginResponse = new Gson().fromJson(jsonResponse, LoginResponse.class);
                    if (loginResponse != null) {
                        boolean status = loginResponse.getStatus();
                        String message = loginResponse.getMessage();
                        if (status) {
                            LoginData loginData = loginResponse.getLoginData();
                            if (loginData != null) {
                                String token = loginData.getToken();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                                PreferenceConnector.writeString(this, getString(R.string.user_token), token);
                                goToHome();
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                closeProgressbar();
                break;
            case Constants.SERVICE_CALL_FOR_SOCIAL_LOGIN:
                if (jsonResponse != null) {
                    try {
                        LoginResponse loginResponse = new Gson().fromJson(jsonResponse, LoginResponse.class);
                        if (loginResponse != null) {
                            boolean status = loginResponse.getStatus();
                            String message = loginResponse.getMessage();
                            if (status) {
                                LoginData loginData = loginResponse.getLoginData();
                                if (loginData != null) {
                                    String token = loginData.getToken();
                                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                                    PreferenceConnector.writeString(this, getString(R.string.user_token), token);
                                    goToHome();
                                }
                            } else {
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                closeProgressbar();
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            updateUI(account);
        } catch (ApiException e) {
            Log.d("abc", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            String name = account.getDisplayName();
            String emailId = account.getEmail();
            String id = account.getId();
            String token = account.getIdToken();
            showProgressBar(this);
            SocialLoginApiCall.serviceCallForSocialLoginDetails(LoginActivity.this, name, "", emailId, id, token);
        }
    }

    private void goToHome() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }
}
