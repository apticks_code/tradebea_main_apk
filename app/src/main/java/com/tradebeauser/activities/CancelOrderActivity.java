package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tradebeauser.ApiCalls.CancelOrderApiCall;
import com.tradebeauser.R;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.requestModels.cancelOrderRequest.CancelOrderRequest;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;

public class CancelOrderActivity extends BaseActivity implements View.OnClickListener, HttpReqResCallBack {

    private TextView tvSubmit;
    private EditText etMessage;
    private ImageView ivBackArrow;
    private LinearLayout llSubmit;

    private int orderId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cancel_order);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.order_id))) {
                orderId = bundle.getInt(getString(R.string.order_id));
            }
        }
    }

    private void initializeUi() {
        tvSubmit = findViewById(R.id.tvSubmit);
        llSubmit = findViewById(R.id.llSubmit);
        etMessage = findViewById(R.id.etMessage);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        tvSubmit.setOnClickListener(this);
        llSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        } else if (id == R.id.llSubmit) {
            prepareSubmitDetails();
        } else if (id == R.id.tvSubmit) {
            prepareSubmitDetails();
        }
    }

    private void prepareSubmitDetails() {
        String cancelReason = etMessage.getText().toString();
        if (!cancelReason.isEmpty()) {
            showProgressBar(this);
            CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
            cancelOrderRequest.setId(orderId);
            cancelOrderRequest.setMessage(cancelReason);
            String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
            CancelOrderApiCall.serviceCallForCancelOrder(this, null, null, token, cancelOrderRequest);
        } else {
            Toast.makeText(this, getString(R.string.please_enter_cancel_reason), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_CANCEL_ORDER) {
            if (jsonResponse != null) {

            }
            closeProgressbar();
        }
    }
}
