package com.tradebeauser.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.tradebeauser.ApiCalls.GetWalletBalance;
import com.tradebeauser.ApiCalls.OrderProductsDetailsApiCall;
import com.tradebeauser.ApiCalls.PaymentDetailsApiCall;
import com.tradebeauser.R;
import com.tradebeauser.fragments.WalletFragment;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.requestModels.orderProductsRequest.OrderProductsRequest;
import com.tradebeauser.models.requestModels.orderProductsRequest.ProductDetailsRequest;
import com.tradebeauser.models.requestModels.paymentRequest.PaymentRequest;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.AppliedPromoDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.Data;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ExtraCharges;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.ProductDetails;
import com.tradebeauser.models.responseModels.ProductsCheckoutResponse.VariantDetails;
import com.tradebeauser.models.responseModels.orderProductsDetailsResponse.OrderProductsDetailsResponse;
import com.tradebeauser.models.responseModels.paymentDetailsResponse.PaymentDetailsData;
import com.tradebeauser.models.responseModels.paymentDetailsResponse.PaymentDetailsResponse;
import com.tradebeauser.models.responseModels.walletBalanceResponse.AccountDetails;
import com.tradebeauser.models.responseModels.walletBalanceResponse.WalletBalanceResponse;
import com.tradebeauser.models.responseModels.walletBalanceResponse.WalletData;
import com.tradebeauser.utils.CommonMethods;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class PaymentModesActivity extends BaseActivity implements View.OnClickListener, PaymentResultWithDataListener, HttpReqResCallBack, CompoundButton.OnCheckedChangeListener, TextWatcher {

    private ImageView ivBack;
    private Data checkoutData;
    private EditText etEnteredWalletAmount;
    private CheckBox cbWalletAmount;
    private TextView tvTitle, tvMyWalletPay, tvAvailableBalance, tvCashOnDeliveryPay, tvOnlinePaymentPay, tvTotalPayableAmount;

    private String emailId = "";
    private String firstName = "";
    private String phoneNumber = "";
    private String razorPayKey = "";
    private String tempTaxAmount = "0";
    private String tempGrandTotal = "0";
    private String appliedPromoCode = "";
    private String tempDeliveryFeeDouble = "0";

    private int activeLocationId;
    private int selectedPaymentMethod;

    private double totalAmount;
    private double totalWalletAmount;
    private double totalRemainingAmount = 0.0;
    private double enteredWalletAmountDouble = 0.0;

    private boolean isWalletAmountChecked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_payment_mode);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.total_amount)))
                totalAmount = bundle.getDouble(getString(R.string.total_amount), 0.0);
            if (bundle.containsKey(getString(R.string.first_name)))
                firstName = bundle.getString(getString(R.string.first_name), "");
            if (bundle.containsKey(getString(R.string.email)))
                emailId = bundle.getString(getString(R.string.email), "");
            if (bundle.containsKey(getString(R.string.phone_number)))
                phoneNumber = bundle.getString(getString(R.string.phone_number), "");
            if (bundle.containsKey(getString(R.string.razor_pay_key)))
                razorPayKey = bundle.getString(getString(R.string.razor_pay_key), "");
            if (bundle.containsKey("tempGrandTotal"))
                tempGrandTotal = bundle.getString("tempGrandTotal", "0");
            if (bundle.containsKey("tempDeliveryFeeDouble"))
                tempDeliveryFeeDouble = bundle.getString("tempDeliveryFeeDouble", "0");
            if (bundle.containsKey("tempTaxAmount"))
                tempTaxAmount = bundle.getString("tempTaxAmount", "0");
            if (bundle.containsKey("activeLocationId"))
                activeLocationId = bundle.getInt("activeLocationId");
            if (bundle.containsKey(getString(R.string.promo_code)))
                appliedPromoCode = bundle.getString(getString(R.string.promo_code), "");
        }

        checkoutData = UserData.getInstance().getCheckoutData();
    }

    private void initializeUi() {
        ivBack = findViewById(R.id.ivBack);
        tvTitle = findViewById(R.id.tvTitle);
        tvMyWalletPay = findViewById(R.id.tvMyWalletPay);
        cbWalletAmount = findViewById(R.id.cbWalletAmount);
        tvAvailableBalance = findViewById(R.id.tvAvailableBalance);
        tvOnlinePaymentPay = findViewById(R.id.tvOnlinePaymentPay);
        tvCashOnDeliveryPay = findViewById(R.id.tvCashOnDeliveryPay);
        tvTotalPayableAmount = findViewById(R.id.tvTotalPayableAmount);
        etEnteredWalletAmount = findViewById(R.id.etEnteredWalletAmount);
    }

    private void initializeListeners() {
        ivBack.setOnClickListener(this);
        tvMyWalletPay.setOnClickListener(this);
        tvOnlinePaymentPay.setOnClickListener(this);
        tvCashOnDeliveryPay.setOnClickListener(this);
        cbWalletAmount.setOnCheckedChangeListener(this);
        etEnteredWalletAmount.addTextChangedListener(this);
        tvTitle.setText(getString(R.string.payments));
    }

    private void prepareDetails() {
        tvTotalPayableAmount.setText(String.valueOf(tempGrandTotal));
        showProgressBar(this);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        GetWalletBalance.serviceCallToGetWalletBalance(this, null, null, token);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.tvMyWalletPay:
                selectedPaymentMethod = 3;
                //startPayment();
                showProgressBar(this);
                PaymentRequest walletPayPaymentRequest = new PaymentRequest();
                walletPayPaymentRequest.setId("WALLET_" + System.currentTimeMillis());
                //paymentRequest.setEmail(emailId);
                //paymentRequest.setAmount(Double.parseDouble(tempGrandTotal));
                if (totalRemainingAmount != 0.0) {
                    walletPayPaymentRequest.setAmount(totalRemainingAmount);
                } else {
                    walletPayPaymentRequest.setAmount(Double.parseDouble(String.valueOf(tempGrandTotal)));
                }
                //paymentRequest.setFee(Double.parseDouble(tempDeliveryFeeDouble));
                //paymentRequest.setTax(Double.parseDouble(tempTaxAmount));
                //paymentRequest.setContact(String.valueOf(phoneNumber));
                //paymentRequest.setDescription("Product Checkout");
                //paymentRequest.setStatus("2");
                walletPayPaymentRequest.setUsedWalletAmount(enteredWalletAmountDouble);
                walletPayPaymentRequest.setPaymentMethodId(selectedPaymentMethod);
                String walletToken = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                PaymentDetailsApiCall.serviceCallForPaymentDetails(this, null, null, walletPayPaymentRequest, walletToken);
                break;
            case R.id.tvCashOnDeliveryPay:
                selectedPaymentMethod = 1;
                showProgressBar(this);
                PaymentRequest paymentRequest = new PaymentRequest();
                paymentRequest.setId("COD_" + System.currentTimeMillis());
                //paymentRequest.setEmail(emailId);
                //paymentRequest.setAmount(Double.parseDouble(tempGrandTotal));
                if (totalRemainingAmount != 0.0) {
                    paymentRequest.setAmount(totalRemainingAmount);
                } else {
                    paymentRequest.setAmount(Double.parseDouble(String.valueOf(tempGrandTotal)));
                }
                //paymentRequest.setFee(Double.parseDouble(tempDeliveryFeeDouble));
                //paymentRequest.setTax(Double.parseDouble(tempTaxAmount));
                //paymentRequest.setContact(String.valueOf(phoneNumber));
                //paymentRequest.setDescription("Product Checkout");
                //paymentRequest.setStatus("2");
                paymentRequest.setUsedWalletAmount(enteredWalletAmountDouble);
                paymentRequest.setPaymentMethodId(selectedPaymentMethod);
                String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                PaymentDetailsApiCall.serviceCallForPaymentDetails(this, null, null, paymentRequest, token);
                break;
            case R.id.tvOnlinePaymentPay:
                selectedPaymentMethod = 2;
                startPayment();
                break;
            default:
                break;
        }
    }

    private void startPayment() {
        final Activity activity = this;
        Checkout checkout = new Checkout();
        checkout.setKeyID(razorPayKey);
        checkout.setImage(R.mipmap.ic_launcher);
        try {
            JSONObject options = new JSONObject();
            options.put("name", firstName);
            options.put("description", "Product Checkout");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("theme.color", "#3F51B5");
            options.put("currency", "INR");
            if (totalRemainingAmount != 0) {
                double remainingAmount = totalRemainingAmount * 100;
                options.put("amount", remainingAmount);//pass amount in currency subunits
            } else {
                options.put("amount", totalAmount);//pass amount in currency subunits
            }
            JSONObject preFill = new JSONObject();
            preFill.put("email", emailId);
            preFill.put("contact", String.valueOf(phoneNumber));
            options.put("prefill", preFill);
            checkout.open(activity, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorPayPaymentId, PaymentData paymentData) {
        showProgressBar(this);
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setId(razorPayPaymentId);
        //paymentRequest.setEmail(emailId);
        //paymentRequest.setAmount(Double.parseDouble(String.valueOf(tempGrandTotal)));
        if (totalRemainingAmount != 0.0) {
            paymentRequest.setAmount(totalRemainingAmount);
        } else {
            paymentRequest.setAmount(Double.parseDouble(String.valueOf(tempGrandTotal)));
        }
        //paymentRequest.setFee(Double.parseDouble(String.valueOf(tempDeliveryFeeDouble)));
        //paymentRequest.setTax(Double.parseDouble(String.valueOf(tempTaxAmount)));
        //paymentRequest.setContact(String.valueOf(phoneNumber));
        //paymentRequest.setDescription("Product Checkout");
        //paymentRequest.setStatus("1");
        paymentRequest.setUsedWalletAmount(enteredWalletAmountDouble);
        paymentRequest.setPaymentMethodId(selectedPaymentMethod);
        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
        PaymentDetailsApiCall.serviceCallForPaymentDetails(this, null, null, paymentRequest, token);
    }

    @Override
    public void onPaymentError(int statusCode, String errorMessage, PaymentData paymentData) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_PAYMENT_DETAILS) {
            if (jsonResponse != null) {
                PaymentDetailsResponse paymentDetailsResponse = new Gson().fromJson(jsonResponse, PaymentDetailsResponse.class);
                if (paymentDetailsResponse != null) {
                    boolean status = paymentDetailsResponse.getStatus();
                    if (status) {
                        PaymentDetailsData paymentDetailsData = paymentDetailsResponse.getPaymentDetailsData();
                        if (paymentDetailsData != null) {
                            Integer paymentId = paymentDetailsData.getPaymentId();
                            closeProgressbar();
                            if (paymentId != -1) {
                                try {
                                    OrderProductsRequest orderProductsRequest = new OrderProductsRequest();
                                    orderProductsRequest.setDeliveryAddressId(activeLocationId);
                                    orderProductsRequest.setPaymentMethodId(selectedPaymentMethod);
                                    orderProductsRequest.setPaymentId(paymentId);
                                    orderProductsRequest.setGrandTotal(totalRemainingAmount);
                                    orderProductsRequest.setUsedWalletAmount(enteredWalletAmountDouble);
                                    orderProductsRequest.setPromoId(appliedPromoCode);
                                    LinkedList<ProductDetailsRequest> listOfProductDetails = new LinkedList<>();
                                    if (checkoutData != null) {
                                        List<ProductDetails> listOfCheckoutProductDetails = checkoutData.getListOfProductDetails();
                                        if (listOfCheckoutProductDetails != null) {
                                            if (listOfCheckoutProductDetails.size() != 0) {
                                                for (int index = 0; index < listOfCheckoutProductDetails.size(); index++) {
                                                    ProductDetails productDetails = listOfCheckoutProductDetails.get(index);
                                                    int quantity = productDetails.getQuantity();
                                                    ExtraCharges extraCharges = productDetails.getExtraCharges();
                                                    AppliedPromoDetails appliedPromoDetails = productDetails.getAppliedPromoDetails();
                                                    LinkedList<VariantDetails> listOfVariantDetails = productDetails.getListOfVariantDetails();
                                                    if (listOfVariantDetails != null) {
                                                        if (listOfVariantDetails.size() != 0) {
                                                            VariantDetails variantDetails = listOfVariantDetails.get(0);
                                                            String id = variantDetails.getId();
                                                            String price = variantDetails.getMrp();
                                                            String discount = variantDetails.getDiscount();
                                                            String appliedPromoDiscount = variantDetails.getDiscount();
                                                            String deliveryCharges = extraCharges.getDeliveryFee();
                                                            String tax = extraCharges.getTax();
                                                            int promoId = 0;

                                                            if (appliedPromoDetails != null) {
                                                                promoId = appliedPromoDetails.getId();
                                                            }
                                                            double priceDouble = Double.parseDouble(price);
                                                            double subTotal = priceDouble * quantity;
                                                            double deliveryChargesDouble = Double.parseDouble(deliveryCharges);
                                                            double discountAmount = CommonMethods.convertDiscountPercentageIntoAmount(discount, String.valueOf(subTotal));
                                                            double promoDiscountAmount = CommonMethods.convertDiscountPercentageIntoAmount(appliedPromoDiscount, String.valueOf(subTotal));
                                                            double taxAmount = CommonMethods.convertTaxPercentageIntoAmount(tax, String.valueOf(subTotal));

                                                            double totalAmount = subTotal - discountAmount - promoDiscountAmount + taxAmount + deliveryChargesDouble;

                                                            ProductDetailsRequest productDetailsRequest = new ProductDetailsRequest();

                                                            productDetailsRequest.setSellerUserId(productDetails.getSellerUserId());
                                                            productDetailsRequest.setProductId(productDetails.getProductId());
                                                            productDetailsRequest.setVariantId(id);
                                                            productDetailsRequest.setSellerVariantId(productDetails.getProductSellerVariantId());
                                                            productDetailsRequest.setQty(productDetails.getQuantity());
                                                            productDetailsRequest.setPrice(price);
                                                            productDetailsRequest.setSubTotal(String.valueOf(subTotal));
                                                            productDetailsRequest.setDiscount(String.valueOf(discountAmount));
                                                            productDetailsRequest.setPromoDiscount(String.valueOf(promoDiscountAmount));
                                                            productDetailsRequest.setDeliveryFee(deliveryCharges);
                                                            productDetailsRequest.setTax(String.valueOf(taxAmount));
                                                            productDetailsRequest.setTotal(String.valueOf(totalAmount));
                                                            productDetailsRequest.setPromoId(promoId);
                                                            listOfProductDetails.add(productDetailsRequest);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        orderProductsRequest.setListOfProductDetails(listOfProductDetails);
                                        showProgressBar(this);
                                        String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
                                        OrderProductsDetailsApiCall.serviceCallForOrderProductsDetails(this, null, null, orderProductsRequest, token);
                                    }
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            }
                        } else {
                            closeProgressbar();
                        }
                    } else {
                        closeProgressbar();
                    }
                } else {
                    closeProgressbar();
                }
            } else {
                closeProgressbar();
            }
        } else if (requestType == Constants.SERVICE_CALL_FOR_ORDER_PRODUCTS_DETAILS) {
            if (jsonResponse != null) {
                OrderProductsDetailsResponse orderProductsDetailsResponse = new Gson().fromJson(jsonResponse, OrderProductsDetailsResponse.class);
                if (orderProductsDetailsResponse != null) {
                    boolean status = orderProductsDetailsResponse.getStatus();
                    String message = orderProductsDetailsResponse.getMessage();
                    if (status) {
                        goToHome();
                        Toast.makeText(this, getString(R.string.order_placed_successfully), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            closeProgressbar();
        } else if (requestType == Constants.SERVICE_CALL_TO_GET_WALLET_BALANCE) {
            if (jsonResponse != null) {
                WalletBalanceResponse walletBalanceResponse = new Gson().fromJson(jsonResponse, WalletBalanceResponse.class);
                if (walletBalanceResponse != null) {
                    boolean status = walletBalanceResponse.getStatus();
                    if (status) {
                        WalletData walletData = walletBalanceResponse.getWalletData();
                        if (walletData != null) {
                            AccountDetails accountDetails = walletData.getAccountDetails();
                            if (accountDetails != null) {
                                double wallet = accountDetails.getWallet();
                                //wallet = 4000;
                                totalWalletAmount = wallet;
                                tvAvailableBalance.setText("Available wallet balance " + "₹" + wallet);

                                double totalAmount = Double.parseDouble(tempGrandTotal);
                                if (totalWalletAmount >= totalAmount) {
                                    etEnteredWalletAmount.setText(String.valueOf(totalAmount));
                                }
                            }
                        }
                    }
                }
            }
            closeProgressbar();
        }
    }

    private void goToHome() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        tvMyWalletPay.setVisibility(View.GONE);
        String enteredWalletAmount = etEnteredWalletAmount.getText().toString();
        isWalletAmountChecked = isChecked;
        if (!enteredWalletAmount.isEmpty()) {
            double enteredWalletAmountDouble = Double.parseDouble(enteredWalletAmount);
            totalRemainingAmount = 0.0;
            double totalAmount = Double.parseDouble(tempGrandTotal);
            if (isChecked) {
                if (totalAmount <= totalWalletAmount) {
                    etEnteredWalletAmount.setText(String.valueOf(totalAmount));
                } else {
                    if (Math.round(totalWalletAmount) != 0) {
                        etEnteredWalletAmount.setText(String.valueOf(totalWalletAmount));
                    }
                }
                if (enteredWalletAmountDouble < totalWalletAmount) {
                    this.enteredWalletAmountDouble = enteredWalletAmountDouble;
                    if (enteredWalletAmountDouble < totalAmount) {
                        totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                    } else if (enteredWalletAmountDouble == totalAmount) {
                        totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                        tvMyWalletPay.setVisibility(View.VISIBLE);
                    }
                } else if (enteredWalletAmountDouble == totalWalletAmount) {
                    this.enteredWalletAmountDouble = enteredWalletAmountDouble;
                    if (enteredWalletAmountDouble < totalAmount) {
                        totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                    } else if (enteredWalletAmountDouble == totalAmount) {
                        totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                        tvMyWalletPay.setVisibility(View.VISIBLE);
                    }
                } else {
                    this.enteredWalletAmountDouble = 0;
                    this.totalRemainingAmount = totalAmount;
                    Toast.makeText(this, getString(R.string.please_enter_valid_wallet_amount), Toast.LENGTH_SHORT).show();
                }
            } else {
                this.enteredWalletAmountDouble = 0;
                totalRemainingAmount = totalAmount;
                etEnteredWalletAmount.setText("");
            }
            tvTotalPayableAmount.setText(String.valueOf(totalRemainingAmount));
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        String enteredWalletAmount = charSequence.toString();
        tvMyWalletPay.setVisibility(View.GONE);
        double totalAmount = Double.parseDouble(tempGrandTotal);
        if (!enteredWalletAmount.isEmpty()) {
            double enteredWalletAmountDouble = Double.parseDouble(enteredWalletAmount);
            totalRemainingAmount = 0.0;
            if (isWalletAmountChecked) {
                if (enteredWalletAmountDouble <= totalAmount) {
                    if (enteredWalletAmountDouble < totalWalletAmount) {
                        this.enteredWalletAmountDouble = enteredWalletAmountDouble;
                        if (enteredWalletAmountDouble < totalAmount) {
                            totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                        } else if (enteredWalletAmountDouble == totalAmount) {
                            totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                            tvMyWalletPay.setVisibility(View.VISIBLE);
                        }
                    } else if (enteredWalletAmountDouble == totalWalletAmount) {
                        this.enteredWalletAmountDouble = enteredWalletAmountDouble;
                        if (enteredWalletAmountDouble < totalAmount) {
                            totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                        } else if (enteredWalletAmountDouble == totalAmount) {
                            totalRemainingAmount = totalAmount - enteredWalletAmountDouble;
                            tvMyWalletPay.setVisibility(View.VISIBLE);
                        }
                    } else {
                        this.enteredWalletAmountDouble = 0;
                        this.totalRemainingAmount = totalAmount;
                        etEnteredWalletAmount.setText("");
                        Toast.makeText(this, getString(R.string.please_enter_valid_wallet_amount), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    this.enteredWalletAmountDouble = 0;
                    totalRemainingAmount = totalAmount;
                    etEnteredWalletAmount.setText("");
                    Toast.makeText(this, "Please enter the amount not greater than total amount", Toast.LENGTH_SHORT).show();
                }
            } else {
                this.enteredWalletAmountDouble = 0;
                totalRemainingAmount = totalAmount;
            }
        } else {
            this.enteredWalletAmountDouble = 0;
            totalRemainingAmount = totalAmount;
        }
        tvTotalPayableAmount.setText(String.valueOf(totalRemainingAmount));
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
