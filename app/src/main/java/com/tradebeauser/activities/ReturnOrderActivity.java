package com.tradebeauser.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tradebeauser.ApiCalls.ReturnOrderApiCall;
import com.tradebeauser.R;
import com.tradebeauser.adapters.CustomArrayAdapterForSpinner;
import com.tradebeauser.customviews.CustomSpinner;
import com.tradebeauser.interfaces.HttpReqResCallBack;
import com.tradebeauser.models.requestModels.returnRequest.ReturnRequest;
import com.tradebeauser.models.responseModels.returnResponse.ReturnResponse;
import com.tradebeauser.models.responseModels.selectedOrderDetailsResponse.DefaultReturnReason;
import com.tradebeauser.utils.Constants;
import com.tradebeauser.utils.PreferenceConnector;
import com.tradebeauser.utils.UserData;

import java.util.LinkedList;

public class ReturnOrderActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, HttpReqResCallBack {

    private TextView tvSubmit;
    private ImageView ivBackArrow;
    private EditText etPersonalReason;
    private CustomSpinner customSpinner;
    private LinearLayout llCustomSpinner, llSubmit;

    private LinkedList<DefaultReturnReason> listOfDefaultReturnReasons;

    private LinkedList<Integer> listOfReturnReasonIds;
    private LinkedList<String> listOfReturnReasonNames;

    private int returnId = 0;
    private int orderId = 0;
    private int reasonId = -1;

    private String reasonName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_return_order);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareSpinnerDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.return_id)))
                returnId = bundle.getInt(getString(R.string.return_id));
            if (bundle.containsKey(getString(R.string.order_id)))
                orderId = bundle.getInt(getString(R.string.order_id));
        }
        listOfDefaultReturnReasons = UserData.getInstance().getListOfDefaultReturnReasons();
    }

    private void initializeUi() {
        tvSubmit = findViewById(R.id.tvSubmit);
        llSubmit = findViewById(R.id.llSubmit);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        customSpinner = findViewById(R.id.customSpinner);
        llCustomSpinner = findViewById(R.id.llCustomSpinner);
        etPersonalReason = findViewById(R.id.etPersonalReason);
    }

    private void initializeListeners() {
        tvSubmit.setOnClickListener(this);
        llSubmit.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        customSpinner.setOnItemSelectedListener(this);
    }

    private void prepareSpinnerDetails() {
        listOfReturnReasonIds = new LinkedList<>();
        listOfReturnReasonNames = new LinkedList<>();
        listOfReturnReasonIds.add(-1);
        listOfReturnReasonNames.add(getString(R.string.select_reason));
        if (listOfDefaultReturnReasons != null) {
            if (listOfDefaultReturnReasons.size() != 0) {
                for (int index = 0; index < listOfDefaultReturnReasons.size(); index++) {
                    DefaultReturnReason defaultReturnReason = listOfDefaultReturnReasons.get(index);
                    int reasonId = defaultReturnReason.getId();
                    String reasonName = defaultReturnReason.getReason();
                    listOfReturnReasonIds.add(reasonId);
                    listOfReturnReasonNames.add(reasonName);
                }
            }
        }

        CustomArrayAdapterForSpinner customArrayAdapterForSpinner = new CustomArrayAdapterForSpinner(this, R.layout.simple_spinner_dropdown_item, listOfReturnReasonNames);
        customSpinner.setAdapter(customArrayAdapterForSpinner);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvSubmit) {
            prepareSubmitDetails();
        } else if (id == R.id.llSubmit) {
            prepareSubmitDetails();
        } else if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    private void prepareSubmitDetails() {
        if (reasonId != -1) {
            String personalReason = etPersonalReason.getText().toString();
            showProgressBar(this);
            String token = PreferenceConnector.readString(this, getString(R.string.user_token), "");
            ReturnRequest returnRequest = new ReturnRequest();
            returnRequest.setOrderId(String.valueOf(orderId));
            returnRequest.setPersonalReason(personalReason);
            returnRequest.setReturnReasonId(reasonId);
            ReturnOrderApiCall.serviceCallForReturnOrder(this, null, null, returnRequest, token);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        reasonId = listOfReturnReasonIds.get(position);
        reasonName = listOfReturnReasonNames.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_FOR_RETURN_ORDER) {
            if (jsonResponse != null) {
                ReturnResponse returnResponse = new Gson().fromJson(jsonResponse, ReturnResponse.class);
                if (returnResponse != null) {
                    boolean status = returnResponse.getStatus();
                    String message = returnResponse.getMessage();
                    if (status) {
                        finish();
                    }
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }
            }
            closeProgressbar();
        }
    }
}
